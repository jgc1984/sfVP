function resetFormulario(form) {

    form.find('input, textarea, input:not([type="submit"])').removeAttr('value');
    form.find('input, textarea, input:not([type="submit"])').val('');
    form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

    form.find('select option').removeAttr('selected').find('option:first').attr('selected', 'selected');

}

function prevent(e) {
    if (e.preventDefault) {
        e.preventDefault();
    } else {
        e.returnValue = false;
    }
}

function bloquearPantalla(gif) {
    $.blockUI({
        message: '<h1> <img src="' + gif + '"/>Cargando Video...</h1>',
        css: {
            top: '100px',
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });

}

function desbloquearPantalla() {
    setTimeout($.unblockUI, 1000);
}