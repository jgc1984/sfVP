/*


$(document).on('click', '.track', function () {  
          $('.sliders').hide("slow");
            var htmlEmbebedPlayer  = "<embed type='application/x-vlc-plugin' name='videotest'  pluginspage='http://www.videolan.org' version='VideoLAN.VLCPlugin.' id='vlc' height='500' width='1075px' toolbar='true' windowless='true' bgcolor='blue'/>";            
            $('.player').html(htmlEmbebedPlayer);                               
            var path = $(this).parent().find('input[name="path"]').val();            
            var archivo = $(this).parent().find('input[name="archivo"]').val();
            link = "<a href='#' class='list-group-item active'><i class='fa fa-volume-up active'></i></a>";            
            track = path + archivo;
            
            vlc =  document.getElementById('vlc');  
            vlc.playlist.add("file:///"+track);
            vlc.playlist.play();

});




$(document).on('click', '.glyphicon-play-circle', function( event ) {   
          htmlEmbebed  = "<embed type='application/x-vlc-plugin' name='videotest'  pluginspage='http://www.videolan.org' version='VideoLAN.VLCPlugin.' id='vlc' class='playerEmbeded' toolbar='true' windowless='true' bgcolor='blue'/>"                
          $('.sliders').hide(); 

          $('.player').html(htmlEmbebed); 

          var datos = $('.tracksByArtist ul.list-group');

          var playlistSorted = sfBIMG.getGener();
                     
          vlc =  document.getElementById('vlc');  
                                       
          $.each( playlistSorted, function( key, track ) {
              $.each( track, function( clave, value ) {                    
                      if ( clave == 1 ){
                          console.log(value);
                          var file = "file://"+value;
                          console.log(file);            
                          vlc.playlist.add(file);    
                      }                                    
              });         

          });
            
          vlc.playlist.play();                
    }); 



/* TOOLS */
$(document).on( 'click', 'a.rename', function( event ){
            event.preventDefault();  
            var newFile = $(this).parent().find('#newNameFile').val();
            var oldFile = $(this).parent().find('#oldNameFile').val();
            var pathRoot = $(this).parent().find('#path').val();
            vlc = document.getElementById('vlc');
            alert("Se detendra la reproduccíon del video");
            vlc.playlist.stop();
            var container = $('#container');                      
            sfBIMG.tools.renameFile(oldFile, newFile, container, pathRoot);
});


$(document).on( 'click', 'a.download', function( event ){   
      event.preventDefault();
      
          $('.playlistNow').show();
          $('input[type="checkbox"]').show();                                   

          $(".playlistNow").droppable({
                
             drop: function( event, ui ) { 
              
                if (!ui.draggable.data("soltado")){ 
                   ui.draggable.data("soltado", true); 
                   $( this ).addClass( "ui-state-highlight" );

                } 
             }, 
             out: function( event, ui ) { 
                if (ui.draggable.data("soltado")){ 
                   ui.draggable.data("soltado", false); 
                   var elem = $(this); 
                   elem.data("numsoltar", elem.data("numsoltar") - 1); 
                   elem.html("Llevo " + elem.data("numsoltar") + " elementos soltados"); 
                } 
             } 
          });

});


$(document).on( 'click', '#save_playlist', function( event ) { 
         /*
        format                = "html",
        methodRequest         = "POST",        
        dataInfo              = new Object();
        dataInfo              = data;
        response              = new Object();//Simple | composite | integrated
        response.format       = "json";
        response.type         = "other"; 
        response.targetclass  = elementParent; 
        sfBIMG.callAjax( url, format, methodRequest, dataInfo, response );
        */  

            var data = JSON.stringify($('#playlist').serializeArray());
            event.preventDefault();
               $.ajax({
                   method: 'GET',
                   url: urlSavePlaylists,
                   dataType : 'html',
                   data: 'data='+data,
                   success : function( response ) {
                     
                     var json = JSON.parse(response);                                            
                       if ( json.status == "OK") {
                        alert("Lista creada correctamente");
                       }
                   }
                
                });
});


/* Obtiene todas las listas de reproduccion del usuario */
$(document).on( 'click', '.listas', function ( event ){
            event.preventDefault();
            
            $.ajax({
                  url: urlPlaylists,
                  dataType: 'html',
                  method: 'POST',          
                     success: function (response) {      

                        $('#contenedor').append(response);
                        $('#playlists').modal({
                           show: true,          
                        });
                     }
              });     
            
});




/*$(".usb").click(function( event ) {           
   playlistID = $(this).parent().parent().find('#playlistParent').val();
   generateM3U( playlistID );
            
});*/



/* Agregar a la lista de reproduccion el track encontrado en la busqueda */
$( document ).on( 'change', 'input[type="checkbox"]', function () {
         if( $(this).is(":checked") ) {              
              
              var track = $(this).parent();
              $(".playlistNow ul").append(track);
              track.append('<i class="glyphicon glyphicon-ok"></i>');
              $(".playlistNow .form-group").append("<p>Agregado</p>");
              $(".playlistNow .form-group i").removeClass("glyphicon glyphicon-floppy-disk");
              $(".playlistNow .form-group i").addClass("glyphicon glyphicon-floppy-remove");
              

          }
});

$(document).on( 'click', '.fa-star', function(){
     track = $(this).parent().parent().parent().find('input').val();
     setFavoriteByTrack(track);
});

/************************************* FUNCIONES GENERALES ***********************************************/
var sfBIMG = {
  /**/


  init: function init( opener ){
    this.opener = opener;

    sfBIMG.confirmOut(true);



  },

  
  /**/
  blockScreen: function (gif) {
    $.blockUI({
        message: '<h1> Cargando Video...</h1> <img src="' + gif + '"/>',
        css: {
            top: '100px',
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });

  },
  
  unBlockScreen: function () {
      setTimeout($.unblockUI, 1000);
  },

  confirmOut: function ( confirm ){
    if( confirm == true ){
      window.addEventListener("beforeunload", function (e) {
                    var confirmationMessage = "\o/";

                    (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                    vlc.playlist.stop();
                    return confirmationMessage;     
                                         //Webkit, Safari, Chrome
      });    
    }  
  },

  callAjax: function ( url, format, methodRequest, dataInfo, handleresponse ){
    sfBIMG.blockScreen(gifURL);
    $.ajax({
              url: url,
              dataType: format,
              method: methodRequest,
              async: false,
              data: dataInfo,
              success: function ( response ) {              
                switch( handleresponse.type ){
                  case 'simple':
                    alert(response.message);
                    $('.player').show(); 
                    break;
                  case 'composite':
                    var elementTarget = document.getElementsByClassName(handleresponse.targetclass);                                           
                    html  = "<div class='infobox'>";
                    html += response;
                    html += "</div>";
                
                    $( elementTarget ).html(html);
                    $('.player').show(); 
                    break; 

                  case 'other':
                    json = jQuery.parseJSON( response );
                    if ( json.status == "OK" ){
                      var decoded = $('<div/>').html(json.folder.html).text();
                      $( handleresponse.targetclass ).html(decoded);
                      root = $( handleresponse.targetclass );
                      alert(json.message);
                      //message = "<div class='col-md-2 alert alert-success'><strong>Creado !</strong></div>";
                      $( root ).html( decoded );                                                
                    } else {
                      console.log(json);
                      alert(response.message);
                    }

                    break;
                    

                  case 'integrated':
                    getfilelist( handleresponse.targetelement , handleresponse.param );
                    break;  

                default:
                    break;

                }
              }, 
              error:  function ( response ){
                alert( response );
              }
                
    });  
  
    sfBIMG.unBlockScreen();
    }
  ,

  messageAlert: function( message ){
      alert( message );
  },

  
  getGener: function( datos ){        
        var Geners = [];

        $.each( datos.find('li'), function( key, element ) {
                console.log(element);
                valor = $(element).find('input[name="genero"]').val();                 
                var Gener = [];                                  
                pathFile = $(element).find('input[name="path"]').val();
                
                if( $.isNumeric(valor) ){
                  Gener.push(valor);                
                  Gener.push(pathFile);                
                  Geners.push(Gener);
                }
                
                                            
        });                

        Geners.sort(function(a, b) {
            return a[0] - b[0];
        });                

        return Geners;    
    },
      
  getTracksByFuente: function (fuente, url) {  
    var format         = "html";
    var methodRequest  = "POST";
    var dataInfo       = new Object();
    
    dataInfo.fuente = fuente;
    var response = new Object();//Simple | composite | integrated
    response.format = "json";
    response.type   = "composite"; 
    response.targetclass  = "panels"; 
    sfBIMG.callAjax( url, format, methodRequest, dataInfo, response );
    
  },

  getTracks: function ( padre, url ) {

    var format         = "html";
    var methodRequest  = "POST";
    var dataInfo       = new Object();
    
    dataInfo.padre = padre;
    var response = new Object();//Simple | composite | integrated
    response.format = "json";
    response.type   = "composite"; 
    response.targetclass  = "panels";    
    sfBIMG.callAjax( url, format, methodRequest, dataInfo, response, true );
    
  },

  process: function ( data, url, elementParent ) {      
            
        format                = "html",
        methodRequest         = "POST",        
        dataInfo              = new Object();
        dataInfo              = data;
        response              = new Object();//Simple | composite | integrated
        response.format       = "json";
        response.type         = "other"; 
        response.targetclass  = elementParent; 
        sfBIMG.callAjax( url, format, methodRequest, dataInfo, response );
    
  },

  getOthersFavorites: function ( elementContainer, track ){
    var Tracks = new Array();
    Tracks.push(track);
    $.each( elementContainer.find('li'), function( key, element ) {                
        folder = $( element ).find('input[name="path"]').val();  
        file = $( element ).find('input[name="archivo"]').val();  
        trac = folder + file;        
        Tracks.push(trac);    
        
    });

    /* NO BORRAR USO POSIBLE DESPUES */
    
    /*Tracks = jQuery.grep(Tracks, function(value) {
        return value != track;
    });*/

    return Tracks;
  },

  setFavoriteByTrack: function ( track, url ) {
        var url = url;
        $.ajax({
            url: url,
            dataType: 'json',
            method: 'GET',
            data: {
                track: track
            },

            success: function (response) {
                alert(response.message);
            }
        });

   },


   getTracksRelated: function ( id, urlTracksRelated ){

        url = urlTracksRelated;      
        $.ajax({
          method: "POST",
          url: url,          
          data: {
            id: id
          },

          dataType: "html",
          success: function (data, textStatus, jqXHR) {                                    
            $('.content_right').append(data);

          },
          error: function (errorMessage) {

          }   
        });

   },


   getTrackData: function ( id, urlTrackData ){

        url= urlTrackData;      
        $.ajax({
          method: "POST",
          url: url,          
          data: {
            id: id
          },

          dataType: "html",
          success: function (data, textStatus, jqXHR) {                                    
            $('.tracksRelated').html(data);

          },
          error: function (errorMessage) {

          }   
         });
   },



   rateTracks: function ( track, urlTracksRate ){

        url= urlTracksRate;      
        $.ajax({
          method: "POST",
          url: url,          
          data: {
            track: track
          },

          dataType: "json",
          success: function (data, textStatus, jqXHR) {                                    
            //$('.tracksRelated').html(data);

          },
          error: function (errorMessage) {

          }   
        });
   },


}
    


var tools = {
  resetForm: function ( form ) {
    form.find('input, textarea, input:not([type="submit"])').removeAttr('value');
    form.find('input, textarea, input:not([type="submit"])').val('');
    form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

    form.find('select option').removeAttr('selected').find('option:first').attr('selected', 'selected');

  },

  copiarUsb: function ( playlistID, url  ) {
      url = url;
        $.ajax({
            url: url,
            dataType: 'json',
            method: 'POST',
            async: false,
            data: {
                playlistID: playlistID
            },
            success: function (response) {
                alert(response.message);
                //getDataByTrack();
            }
        });
   }


}


$.randomize = function( arr ) {  
           for(var j, x, i = arr.length; i; j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], arr[j] = x);
           return arr;   
};

