


    var gifURL = "{{ asset('progress-bar.gif')}}";
    var urlTracks = "{{ path('ajax_get_tracks_by_tracklist')}}";
    var urlFuente   = "{{ path('ajax_get_tracks_by_fuente')}}";
    
    $( function () {
        sfBIMG.confirmOut(true);
        /*var start = new Date;

        setInterval(function() {
                $('.timer').text((new Date - start) / 1000 + " Seconds");
        }, 1000);

        */
        var registros = true;

        //loadTopTracks();


        //$('.player').hide();
        //$('.searchAdvanced').hide();
        $('.favorites_panel').hide();
        //$('.playlists_planel').hide();
        $('.topTracks_panel').hide();
        $( "#searchAdvanced" ).submit(function( event ) {   
            var filtros = new Array();  
            
            $('.filtro').each(function ( key, value ) {
                var artista = new Array();

                artista.push('artista');
                valor = $(this).find('select.artista option:selected').val();                                                                     
                artista.push(valor);
                filtros.push(artista);                                                                

                var track = new Array();
                track.push('titulo');
                titulo = $(this).find('input.valor').val();                 
                track.push(titulo);

                var genero = new Array();
                genero.push('genero');
                selectGenero = $(this).find('select.genero option:selected').val();                  
                genero.push(selectGenero);                                
                filtros.push(genero);

                fechaPublicacion = $(this).find('input.publicacion').val();                    
                var fechaPub= new Array();
                fechaPub.push('fechaPublicacion');                 
                fechaPub.push(fechaPublicacion);
                filtros.push(fechaPub);   

                fechaGrab = $(this).find('input.grabacion').val();                    
                var fechaGrabacion= new Array();
                fechaGrabacion.push('fechaGrabacion');                 
                fechaGrabacion.push(fechaGrab);
                filtros.push(fechaGrabacion); 

                selectRegistros = $(this).find('select.registros option:selected').val();     
                var registros= new Array();
                registros.push('registros');                 
                registros.push(selectRegistros);
                filtros.push(registros);                
                /*DEMAS FILTROS*/
                
                
            });                

        var data = JSON.stringify(filtros);            

        event.preventDefault();
        $.ajax({
            method: 'GET',
            url: '{{ path('ajax_search_advanced')}}',
            dataType : 'html',
            data: 'search='+data,
            success : function( data ){                
                $('.infobox').html( data );                            
            }
        });
});


$('.lastTrack, .favorites, .fuentes').bxSlider({
  minSlides: 10,
  maxSlides: 10,
  slideWidth: 370,
  slideMargin: 10,
  captions: true,
  auto: true,
});




});


$(document).on( 'click', 'ul.favorites li', function(){  

    bloquearPantalla(gifURL);
    var path =  $(this).find('input[name=path]').val();
    var titulo =  $(this).find('input[name=titulo]' ).val();
    var trackFavorite =  $(this).find('input[id=track]' ).val();
    
    $('title').html(titulo);

    htmlEmbebedPlayer   ="<div class='page-header'><h3>'"+titulo+"' </h3></div>";

    /**/
    var htmlVolume = "<i class='glyphicon glyphicon-volume-up' style='margin-left:70px'></i>";
    var trackID = $('.favorites_panel').find('input[name=track]').val();
    
    $('#track').parent().css("background-color", 'rgb(240, 202, 82)');
    $('#track').parent().append(htmlVolume);
    
    //var html = $("<i class='glyphicon glyphicon-volume-up' style='margin-left:70px'></i> ").insertAfter($(this));
    
    $('.tracksByArtist').show("slow");
    /**/
    
    htmlEmbebed  = htmlEmbebedPlayer;
    htmlEmbebed  += "<embed type='application/x-vlc-plugin' name='videotest'  pluginspage='http://www.videolan.org' version='VideoLAN.VLCPlugin.' id='vlc' class='playerEmbeded' toolbar='true' windowless='true' bgcolor='blue'/>"                
    $('.player').html(htmlEmbebed);
    vlc =  document.getElementById('vlc');      
    var file = "file://"+path;    
    vlc.playlist.add(file);         
    
    nameClass = $(this).parent().attr('class');
    $('.playlists_planel').hide();      
    
    $('.favorites_panel').show("slow");
    
    $('.sliders').hide("slow");
    desbloquearPantalla(); 
    vlc.playlist.play();
});


$(document).on( 'click', 'ul.lastTrack li', function(){  

    sfBIMG.blockScreen(gifURL);
    var path =  $(this).find('input[name=path]').val();
    var titulo =  $(this).find('input[name=titulo]' ).val();

    htmlEmbebedPlayer   ="<div class='page-header'><h3>'"+titulo+"' </h3></div>";
    
    htmlEmbebed  = htmlEmbebedPlayer;
    htmlEmbebed  += "<embed type='application/x-vlc-plugin' name='videotest'  pluginspage='http://www.videolan.org' version='VideoLAN.VLCPlugin.' id='vlc' class='playerEmbeded' toolbar='true' windowless='true' bgcolor='blue'/>"                
    $('.player').html(htmlEmbebed);
    
    $('.favorites_panel').hide();
    $('.playlists_panel').hide();

    $('.sliders').hide("slow");

    sfBIMG.unBlockScreen(); 
    
    vlc =  document.getElementById('vlc');   

    var file = "file://"+path+titulo;  
     
    vlc.playlist.add(file);         
    
    nameClass = $(this).parent().attr('class');
          
    $('.topTracks_panel').show("slow");
    
    vlc.playlist.play();
});


$(document).on('click', '.fuentes li', function () {
    $('.sliders').hide("slow");
    $('.playlists_planel').hide("slow");    
    bloquearPantalla(gifURL);   
    padre = $(this).find('input[type="hidden"]').val();      
    getTracksByFuente(padre , urlFuente);
    /*$( ".list-group" ).draggable();
      $( ".list-group-item" ).draggable();
      $( ".playlistNow" ).draggable(); */   
    desbloquearPantalla(); 
});


$(document).on('click', '.fa-play-circle', function () {
    $('.sliders').hide();
    bloquearPantalla(gifURL);  
    padre         = $(this).parent().find('input').val();
    
    getTracks(padre , urlTracks);
    
    $(this).parent().css("background-color", 'rgb(240, 202, 82)');
    
    $("<i class='glyphicon glyphicon-volume-up' style='margin-left:70px'></i> ").insertAfter($(this));
    $('.tracksByArtist').show("slow");
    desbloquearPantalla();
    
}); 


/* COPIAR DESPUES */
 getfilelist: function ( cont, root ) {            
        $.get( urlMedia, { dir: root }, function( response ) {
          json = jQuery.parseJSON(response);
          jQuery( cont ).append(text(json.html).html());
          //$( cont ).append(response.html);
    
    /*        $( cont ).find( '.start' ).html( '' );
            $( cont ).removeClass( 'wait' ).append( response );
            $( cont ).find('ul:hidden').slideDown({ duration: 500, easing: null });*/
            
            /*if( '/media/gabriel/DATOS/VIDEOS' == root ) {
                alert("OK");
                $( cont ).find('ul:hidden').show();
            } else {
              alert("OK1");
                $( cont ).find('ul:hidden').slideDown({ duration: 500, easing: null });
            }*/
            
        });
  },

  play: function ( fileCurrent ){
            var nameFileCurrent  = fileCurrent.split("/").pop();
            
            var pathCurrent  = fileCurrent.replace(/\/[^\/]+$/, '');
            
            var htmlHeaderPlayer = "<h1 class='page-header'>Reproductor </h1>";
                                 
            
            htmlEmbebedPlayer  = "<embed type='application/x-vlc-plugin' name='videotest'  pluginspage='http://www.videolan.org' version='VideoLAN.VLCPlugin.' id='vlc' height='500' width='1075px' toolbar='true' windowless='true' bgcolor='blue'/>";
            htmlHeaderPlayer  += htmlEmbebedPlayer;
            $('.player').html(htmlHeaderPlayer);
            var vlc =  document.getElementById('vlc');
            
            vlc.playlist.stop();    
            vlc.playlist.clearItems();
                                
            var file = "file://"+fileCurrent;
            vlc.playlist.add(file);    

            vlc.playlist.play();
            var htmlListOptionsVideo  = "<ul class='list-group'>";
                htmlListOptionsVideo += "<li class='list-group-item'>";
            var inputHidden           = "<input type='hidden' id='oldNameFile' value='"+fileCurrent+"'>";        
            var inputText             = "<input type='text' id='newNameFile' value='"+nameFileCurrent+"'>";
            var inputHiddenPath       = "<input type='hidden' id='path' value='"+pathCurrent+"'>";
                htmlListOptionsVideo += inputHidden;
                htmlListOptionsVideo += inputHiddenPath;
                htmlListOptionsVideo += inputText;                        
                htmlListOptionsVideo += "<a class='btn btn-warning rename'><i class='glyphicon glyphicon-text-width'>Renombrar</i></a><a class='btn btn-warning guardar'><i class='glyphicon glyphicon-floppy-saved'></i></a>";
                htmlListOptionsVideo += "</li></ul>";

            
            $('.video').html(htmlListOptionsVideo);
            $('.video').show('slow');
            
            
        
  },

  tools: { 
      renameFile: function (nameFileOld, nameFileNew, container, root){              
        methodRequest: "POST",
        format: "json",
        dataInfo: {},
        response:  {},//Simple | composite | integrated
        response.format: "json",
        response.type: "integrated",
        response.targetclass: "info",
        response.targetelement: container,
        response.param: root,
        dataInfo.fileOld: nameFileOld,
        dataInfo.fileNew: nameFileNew,
        sfBIMG.callAjax( urlRename, format, methodRequest, dataInfo, response );                            
      },

      copiarUsb: function ( playlistID, url ) { 
        format:       "json",
        methodRequest:  "POST",
        dataInfo:     playlistID,
        response: {},//Simple | composite | integrated
        response.format: "json",
        response.type: "simple",
        response.targetclass: "info",
        
        sfBIMG.callAjax( url, format, methodRequest, dataInfo, response );  
    },
    generateM3U: function( playlistID, url ) {
        format:        "json",
        methodRequest: "GET",
        dataInfo: {},
        dataInfo.playlistID(playlistID),
        response: {}, //Simple | composite | integrated
        response.format: "json",
        response.type:   "simple",
        response.targetclass:  "info",
        sfBIMG.callAjax( url, format, methodRequest, dataInfo, response );                             
    }
  }