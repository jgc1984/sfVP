sfBIMG
******

Beta 0.0.1


Interface User to VLC
*************************

Features
------------

- Play local videos such as vob, mpg, mp4, mkv.
- Create playlist.
- Import massive files.

Main dependecies
----------------

- FFMPEG.
- VLC plugin firefox.
