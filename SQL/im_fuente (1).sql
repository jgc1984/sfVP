-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 19-09-2015 a las 19:59:22
-- Versión del servidor: 5.5.41-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `im_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_fuente`
--

CREATE TABLE IF NOT EXISTS `im_fuente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `portada` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lugarGrabacion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fechaRecorded` date NOT NULL,
  `fechaPublished` date NOT NULL,
  `soporte` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Volcado de datos para la tabla `im_fuente`
--

INSERT INTO `im_fuente` (`id`, `nombre`, `portada`, `lugarGrabacion`, `fechaRecorded`, `fechaPublished`, `soporte`) VALUES
(1, 'Quebrado', '_385a13c1571f73541e8ca9e194326b0a.jpg', 'Teatro Coliseo', '2008-05-09', '2009-05-07', ''),
(2, '1 y 1000 Noches', '_fe01efc3c9e9f6a40e5575de57e51732.jpg', 'Varias', '2012-05-11', '2013-05-18', ''),
(3, 'Audio & Agua', '_8cdfa2a5ad48a745f65ff9338e9adbc1.jpg', 'Luna Park', '2010-05-14', '2012-05-18', ''),
(4, 'Me Veras Volver', '_f61f7b535f5c08fcb42d292b9e71cf3e.jpg', 'Argentina, Chile, Peru, Venezuela', '2007-05-18', '2008-05-16', ''),
(5, 'El Ultimo Concierto', '_9927720b2542d4d871354aebd3016914.jpg', 'Monumental', '2005-05-19', '2007-05-11', ''),
(6, 'Las Bandas Eternas', '_8e68a391a629bdc05980503b83ac3304.jpg', 'Estadio Velez Sarfield', '2009-05-15', '2010-05-27', ''),
(7, 'Ahi Vamos', '_dcd3fa7cec7130e6ef03b2d35f87260c.jpg', 'Estadio Obras Sanitarias', '2007-05-10', '2008-05-15', ''),
(8, 'Comfort & Musica Para Volar', '_9069109ab74bb9d9e87b5d12cedfe68c.jpg', 'Miami', '1996-05-10', '1997-05-23', ''),
(9, 'From Brixton To Beijing', '_561b12fc8e8a6bdc737ccb284f9b3588.jpg', 'Brixton, Beijing', '2002-05-03', '2004-05-14', ''),
(10, 'LIVE AT VALLHALL – HOMECOMING', '_5243f1e8c77b107600ae5ea24524ffb7.jpg', 'OSLO', '2001-03-24', '2002-05-20', ''),
(11, 'SINGLES 96-06', '_1054bf5dee93cee4a41356c60c681192.jpg', 'BELGIQUE', '2004-05-07', '2005-05-20', ''),
(12, 'LIVE IN CONCERT', '_ef92e1cbc76011a0ef66999acc241ddc.jpg', 'OPEN AIR THEATRE,  SAN DIEGO, CALIFORNIA', '1993-10-03', '2001-02-20', ''),
(13, 'MUSIC WITHOUT FEAR', NULL, 'LA, STADIUM OLYPIQUE', '2001-10-04', '2002-05-10', ''),
(14, 'THAT ONE NIGHT', NULL, 'ESTADIO OBRAS SANITARIAS, BA ARG', '2005-10-09', '2007-03-06', ''),
(15, 'AHI VAMOS', NULL, 'ESTADIO OBRAS SANITARIAS, BA ARG', '2007-05-10', '2008-05-15', ''),
(16, 'ROCK WERCHTER', NULL, 'BELGIQUE', '2011-05-06', '2011-05-13', ''),
(17, 'FESTIVAL BIZARRE', '_59820a8593dbac48110663bd5d2a51f2.jpg', 'WEEZE, GERMANY', '2001-05-11', '2001-05-11', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
