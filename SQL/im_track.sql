-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 22-04-2015 a las 06:47:11
-- Versión del servidor: 5.5.41-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `im_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_track`
--

CREATE TABLE IF NOT EXISTS `im_track` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombreArchivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `artista` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dvdOriginal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `formato` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `album` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nroTrack` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=758 ;

--
-- Volcado de datos para la tabla `im_track`
--

INSERT INTO `im_track` (`id`, `titulo`, `nombreArchivo`, `path`, `artista`, `dvdOriginal`, `formato`, `album`, `nroTrack`) VALUES
(1, 'Fell On Blacks Days', '1994 - Soundgarden - Fell On Black Days.mpg', 'file:///media/gabriel/DATOS/VIDEOS/ROCK /', 'soundgarden', 'ffdfdf', 'mpg', NULL, NULL),
(2, '1991 - Pearl Jam - Jeremy [Original Version promo]', '1991 - Pearl Jam - Jeremy [Original Version promo].VOB', 'file:///media/gabriel/DATOS/VIDEOS/ROCK /', 'Pearl Jam', 'dfdf', 'VOB', NULL, NULL),
(3, '2005 - Raca Negra - Fortaleza - Correnteza de emocao', '2005 - Raca Negra - Fortaleza - Correnteza de emocao.mpg', 'file:///media/gabriel/DATOS/VIDEOS/LATINOS/', 'Raca Negra', 'Fortaleza', 'mpeg', NULL, NULL),
(4, '2012 - Raca Negra - Goiana - A Solidao Chegou', '2012 - Raca Negra - Goiana - A Solidao Chegou.mpg', 'file:///media/gabriel/DATOS/VIDEOS/LATINOS/', 'Raca Negra', 'Goiana', 'mpeg', NULL, NULL),
(5, '2012 - Raca Negra - Goiana - Covardia', '2012 - Raca Negra - Goiana - Covardia.mpg', 'file:///media/gabriel/DATOS/VIDEOS/LATINOS/', 'Raca Negra', 'Goiana', 'mpeg', NULL, NULL),
(6, '2012 - Raca Negra - Goiana - Tentando', '2012 - Raca Negra - Goiana - Tentando .mpg', 'file:///media/gabriel/DATOS/VIDEOS/LATINOS/', 'Raca Negra', 'Goiana', 'mpeg', NULL, NULL),
(7, '2012 - Raca Negra - Goiana - Gostava tanto de voce', '012 - Raca Negra - Goiana - Gostava tanto de voce.mpg', 'file:///media/gabriel/DATOS/VIDEOS/LATINOS/', 'Raca Negra', 'Goiana', 'mpeg', NULL, NULL),
(8, 'Radiohead - Creep [Video]', 'Radiohead - Creep [Video].VOB', 'file:///media/gabriel/DATOS/VIDEOS/ROCK /', 'radiohead', 'sdf', 'V', NULL, NULL),
(9, '1997 - Deftones - My Own Summer', '1997 - Deftones - My Own Summer.mpeg', 'file:///media/gabriel/DATOS/VIDEOS/ROCK /', 'Deftones', 'jkdjskd', 'V', NULL, NULL),
(10, '1992 - Stone Temple Pilots - Creep', '1992 - Stone Temple Pilots - Creep.vob', 'file:///media/gabriel/DATOS/VIDEOS/ROCK /', 'STP', 'asdf', 'V', NULL, NULL),
(11, '1993 - Nirvana - Unplugged - 04. The Man Who Sold The World (ntsc 5.1) [1993]', '1993 - Nirvana - Unplugged - 04. The Man Who Sold The World (ntsc 5.1) [1993].vob', 'file:///media/gabriel/DATOS/VIDEOS/ROCK /', 'Nirvana', 'asdfsdf', 'V', NULL, NULL),
(12, '1992 - Kyuss - Green Machine', '1992 - Kyuss - Green Machine.mpg', 'file:///media/gabriel/DATOS/VIDEOS/ROCK /', 'Kyuss', 'asdf', 'V', NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
