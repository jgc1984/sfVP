-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 08-05-2015 a las 22:11:46
-- Versión del servidor: 5.5.41-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `im_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_artista`
--

CREATE TABLE IF NOT EXISTS `im_artista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombreCorto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Volcado de datos para la tabla `im_artista`
--

INSERT INTO `im_artista` (`id`, `nombre`, `nombreCorto`) VALUES
(1, 'DIVIDIDOS', 'DVD2'),
(2, 'STONE TEMPLE PILOTS', 'STP'),
(3, 'LED ZEPPELIN', 'LZ'),
(4, 'GUSTAVO CERATI', 'GC'),
(5, 'LUIS ALBERTO SPINETTA', 'LAS'),
(6, 'MASSIVE ATTACK', 'MA'),
(7, 'MORCHEEBA', 'MB'),
(8, 'MORPHINE', 'MP'),
(9, 'PEARL JAM', 'PJ'),
(10, 'PORTISHEAD', 'PH'),
(11, 'PEDRO AZNAR', 'PA'),
(12, 'SADE', 'SD'),
(13, 'SODA STEREO', 'SS'),
(14, 'SOUNDGARDEN', 'SG'),
(15, 'THE GATHERING', 'TG'),
(16, 'THE CULT', 'TC'),
(17, 'HOOVERPHONIC', 'HP');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
