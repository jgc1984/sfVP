SELECT DISTINCT (p.id), pv.id, pv.product_id, ptx.taxon_id, ptx.product_id, t.id, tny.id, tt.translatable_id = t.id, tt2.translatable_id = t.id, pt.name, tt.name, tt.name
FROM sylius_product_translation pt
LEFT JOIN sylius_product p ON pt.translatable_id = p.id
JOIN sylius_product_variant pv ON p.id = pv.product_id
JOIN sylius_product_variant_image pvi ON pvi.variant_id = pv.id
JOIN sylius_product_taxon ptx ON ptx.product_id = p.id
JOIN sylius_taxon t ON ptx.taxon_id = t.id
LEFT JOIN sylius_taxon tx ON t.parent_id = tx.id
LEFT JOIN sylius_taxonomy tny ON tny.root_id = t.id
LEFT JOIN sylius_taxonomy_translation tt ON tt.translatable_id = t.id
LEFT JOIN sylius_taxon_translation tt2 ON tt2.translatable_id = t.id

WHERE pt.name LIKE 'Conf%'
OR tt.name LIKE 'locale%'
OR tt2.name LIKE 'Book%'
GROUP BY p.id, pv.id, pv.product_id, ptx.taxon_id, ptx.product_id, t.id, tny.id, tt.translatable_id = t.id, tt2.translatable_id = t.id, pt.name,tt.name, tt.name
LIMIT 0 , 30

-----------------------------------------------------------------


SELECT pt.name, tt.name 'Taxonomy', tt.locale 'Taxonomy Locale', tt2.name 'Taxon Name', tt2.description'Taxon Description'
FROM sylius_product_translation pt
LEFT JOIN sylius_product p ON pt.translatable_id = p.id
JOIN sylius_product_taxon ptx ON ptx.product_id = p.id
JOIN sylius_taxon t ON ptx.taxon_id = t.id
LEFT JOIN sylius_taxon tx ON t.parent_id = tx.id
JOIN sylius_taxonomy tny ON tny.root_id = t.id
JOIN sylius_taxonomy_translation tt ON tt.translatable_id = t.id
JOIN sylius_taxon_translation tt2 ON tt2.translatable_id = t.id
WHERE pt.name LIKE 'Conf%'
OR tt.name LIKE 'locale%'
OR tt2.name LIKE 'Camise%'

--------------------------------------------------------------------


SELECT DISTINCT (p.id)
, pv.id
, pvi.path
, pv.price
FROM sylius_product_translation pt
LEFT JOIN sylius_product p ON pt.translatable_id = p.id
JOIN sylius_product_variant pv ON p.id = pv.product_id
JOIN sylius_product_variant_image pvi ON pvi.variant_id = pv.id
JOIN sylius_product_taxon ptx ON ptx.product_id = p.id
JOIN sylius_taxon t ON ptx.taxon_id = t.id
LEFT JOIN sylius_taxon tx ON t.parent_id = tx.id
LEFT JOIN sylius_taxonomy tny ON tny.root_id = t.id
LEFT JOIN sylius_taxonomy_translation tt ON tt.translatable_id = t.id
LEFT JOIN sylius_taxon_translation tt2 ON tt2.translatable_id = t.id
GROUP BY p.id
, pv.id
, pvi.path
, pv.price

WHERE pt.name LIKE 'Conf%'
OR tt.name LIKE 'locale%'
OR tt2.name LIKE 'Book%'
GROUP BY p.id, pv.id, pv.product_id, ptx.taxon_id, ptx.product_id, t.id, tny.id, tt.translatable_id = t.id, tt2.translatable_id = t.id, pt.name,tt.name, tt.name
LIMIT 0 , 30

----------------------------------------------

$qryBase = "SELECT DISTINCT (p.id)
							, pv.id
							, pvi.path
							, pv.price
                              FROM sylius_product_translation pt
								LEFT JOIN sylius_product p ON pt.translatable_id = p.id
								JOIN sylius_product_variant pv ON p.id = pv.product_id
								JOIN sylius_product_variant_image pvi ON pvi.variant_id = pv.id
								JOIN sylius_product_taxon ptx ON ptx.product_id = p.id
								JOIN sylius_taxon t ON ptx.taxon_id = t.id
								LEFT JOIN sylius_taxon tx ON t.parent_id = tx.id
								LEFT JOIN sylius_taxonomy tny ON tny.root_id = t.id
								LEFT JOIN sylius_taxonomy_translation tt ON tt.translatable_id = t.id
								LEFT JOIN sylius_taxon_translation tt2 ON tt2.translatable_id = t.id
								GROUP BY p.id
								, pv.id
								, pvi.path
								, pv.price
                              ";








--------------------------------------------------------------------------

 public function hydrateSearchResultsAll($resultSetFromFulltextSearch = array(), $query)
    {
            $results = array();

            $qryBase = "SELECT IFNULL( tx.parent_id,  'Sin Padre' ) , 
                                      p.id 'Producto', 
                                      pt.name, 
                                      pt.description,
                                      tt.name 'Taxonomy', 
                                      tt.locale 'Taxonomy Locale', 
                                      tt2.name 'Taxon Name', 
                                      tt2.description 'Taxon Description',
                                      pvi.path,
                                      pv.price 
                              FROM sylius_product_translation pt
                              LEFT JOIN sylius_product p ON pt.translatable_id = p.id
                              INNER JOIN sylius_product_variant pv ON p.id = pv.product_id
                              INNER JOIN sylius_product_variant_image pvi ON pv.id = pvi.variant_id
                              JOIN sylius_product_taxon ptx ON ptx.product_id = p.id
                              JOIN sylius_taxon t ON ptx.taxon_id = t.id
                              LEFT JOIN sylius_taxon tx ON t.parent_id = tx.id
                              LEFT JOIN sylius_taxonomy tny ON tny.root_id = t.id
                              JOIN sylius_taxonomy_translation tt ON tt.translatable_id = t.id
                              JOIN sylius_taxon_translation tt2 ON tt2.translatable_id = t.id
                              ";
            /* Busca en todas las categorias */                  
            if($query->getSearchTerm()!= "" && $query->getSearchParam() == "all"){
                $qryBase .= "WHERE tt2.name LIKE '".$query->getSearchTerm()."'
                             OR pt.name LIKE  '".$query->getSearchTerm()."'
                             OR pt.description LIKE  '".$query->getSearchTerm()."'
                ";
                /*Obtiene todos los productos con sus categorias*/                              
                $stmt = $this->em->getConnection()->prepare($qryBase);                                            
            }

            /* Busca en una categoria*/
            if($query->getSearchTerm()!= "" && $query->getSearchParam() != "all"){              
                $qryBase .= "WHERE tt2.name = '".$query->getSearchParam()."'
                             OR pt.name LIKE '".$query->getSearchTerm()."'
                             OR pt.description LIKE '".$query->getSearchTerm()."'
                ";
                /*Obtiene todos los productos con sus categorias*/                              
                $stmt = $this->em->getConnection()->prepare($qryBase);                                            
            }

            /* Busca una categoria*/
            if($query->getSearchTerm()== "" && $query->getSearchParam() != "all"){                            
                $qryBase .= "WHERE tt2.name = '".$query->getSearchParam()."' ";
                /*Obtiene todos los productos con sus categorias*/                              
                $stmt = $this->em->getConnection()->prepare($qryBase);                                            
            }

            if($query->getSearchTerm()== ""){
            
                $stmt = $this->em->getConnection()->prepare($qryBase);                                             
            }

            $stmt = $this->em->getConnection()->prepare($qryBase);                                            
            $stmt->execute();
            foreach ($stmt->fetchAll() as $object) {
                $results[] = $object;
            }
        

        return $results;
    }


--------------


SELECT IFNULL( tx.id,  'N/D' )  'Parent', p . * , pt . * , t . * , tny . * , pts . * , tt . * , tty . *, pv.*, pvi.*
FROM sylius_product p
JOIN sylius_product_translation pts ON pts.translatable_id = p.id
JOIN sylius_product_variant pv ON pv.product_id = p.id
JOIN sylius_product_variant_image pvi ON pvi.variant_id = pv.id
JOIN sylius_product_taxon pt ON pt.product_id = p.id
JOIN sylius_taxon t ON t.id = pt.taxon_id
LEFT JOIN sylius_taxon tx ON tx.parent_id = t.id
JOIN sylius_taxon_translation tt ON tt.translatable_id = t.id
JOIN sylius_taxonomy tny ON tny.root_id = t.taxonomy_id
JOIN sylius_taxonomy_translation tty ON tty.translatable_id = tny.id
WHERE pts.name LIKE 'Conf%'
OR tt.name LIKE 'locale%'
OR tty.name LIKE 'Camise%'


-----------

SELECT IFNULL( tx.id,  'N/D' )  'Parent', pts.name, pts.description, tt.name, tny.root_id, tty.name, pvi.path, pv.price 
FROM sylius_product p
JOIN sylius_product_translation pts ON pts.translatable_id = p.id
JOIN sylius_product_variant pv ON pv.product_id = p.id
JOIN sylius_product_variant_image pvi ON pvi.variant_id = pv.id
JOIN sylius_product_taxon pt ON pt.product_id = p.id
JOIN sylius_taxon t ON t.id = pt.taxon_id
LEFT JOIN sylius_taxon tx ON tx.parent_id = t.id
JOIN sylius_taxon_translation tt ON tt.translatable_id = t.id
JOIN sylius_taxonomy tny ON tny.root_id = t.taxonomy_id
JOIN sylius_taxonomy_translation tty ON tty.translatable_id = tny.id
WHERE pts.name LIKE 'Book%'
OR tt.name LIKE 'Libro%'
OR tty.name LIKE 'Cat%'