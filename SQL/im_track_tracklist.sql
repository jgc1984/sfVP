-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 10-05-2015 a las 17:54:46
-- Versión del servidor: 5.5.41-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `im_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_track_tracklist`
--

CREATE TABLE IF NOT EXISTS `im_track_tracklist` (
  `track_id` int(11) NOT NULL,
  `tracklist_id` int(11) NOT NULL,
  PRIMARY KEY (`track_id`,`tracklist_id`),
  KEY `IDX_4ACA2C65ED23C43` (`track_id`),
  KEY `IDX_4ACA2C68C5F30E1` (`tracklist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `im_track_tracklist`
--

INSERT INTO `im_track_tracklist` (`track_id`, `tracklist_id`) VALUES
(132, 5),
(166, 2),
(166, 4),
(201, 1),
(202, 1),
(203, 1),
(204, 1),
(205, 2),
(205, 5),
(223, 1),
(224, 1),
(226, 2),
(226, 4),
(238, 1),
(239, 1),
(264, 4),
(275, 4),
(275, 5),
(333, 3),
(334, 3),
(335, 3),
(339, 3),
(340, 3);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `im_track_tracklist`
--
ALTER TABLE `im_track_tracklist`
  ADD CONSTRAINT `FK_4ACA2C65ED23C43` FOREIGN KEY (`track_id`) REFERENCES `im_track` (`id`),
  ADD CONSTRAINT `FK_4ACA2C68C5F30E1` FOREIGN KEY (`tracklist_id`) REFERENCES `im_tracklist` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
