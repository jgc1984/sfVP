-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 10-05-2015 a las 17:54:38
-- Versión del servidor: 5.5.41-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `im_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_tracklist`
--

CREATE TABLE IF NOT EXISTS `im_tracklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cantReproducciones` int(11) DEFAULT NULL,
  `cantTracks` int(11) DEFAULT NULL,
  `fechaLastPLay` datetime DEFAULT NULL,
  `duracion` time DEFAULT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `im_tracklist`
--

INSERT INTO `im_tracklist` (`id`, `nombre`, `descripcion`, `cantReproducciones`, `cantTracks`, `fechaLastPLay`, `duracion`, `tipo`, `created`, `updated`) VALUES
(1, 'soda papa', 'asdfdsf', 37, NULL, NULL, NULL, 'v', '2015-05-09 06:26:21', '2015-05-10 16:53:26'),
(2, 'Spinetta', 'assdf', 14, NULL, NULL, NULL, 'v', '2015-05-09 06:42:52', '2015-05-10 17:30:18'),
(3, 'Raca Negra', 'asdfdf', 1, NULL, NULL, NULL, 'v', '2015-05-10 15:06:54', '2015-05-10 15:07:47'),
(4, 'El Flaco Spinetta', 'asdf', 1, NULL, NULL, NULL, 'v', '2015-05-10 17:33:47', '2015-05-10 17:34:28'),
(5, 'Domingo Retro', 'dfdfdf', 1, NULL, NULL, NULL, 'v', '2015-05-10 17:51:08', '2015-05-10 17:51:31');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
