-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 23-05-2015 a las 20:04:17
-- Versión del servidor: 5.5.41-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `im_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_fuente`
--

CREATE TABLE IF NOT EXISTS `im_fuente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `portada` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lugarGrabacion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fechaRecorded` date NOT NULL,
  `fechaPublished` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `im_fuente`
--

INSERT INTO `im_fuente` (`id`, `nombre`, `portada`, `lugarGrabacion`, `fechaRecorded`, `fechaPublished`) VALUES
(1, 'Me Veras Volver', NULL, 'Estadio Monumental', '2010-01-01', '2010-01-01'),
(2, 'El Ultimo Concierto', '_961c348f69c891bb9413c8aeea5e6357.jpg', 'Estadio Monumental', '2010-01-01', '2010-01-01'),
(3, 'Spinetta & Las Bandas Eternas', '_0b240dcb2db57924daff3248151b1974.jpg', 'Estadio Velez Sarfield', '2010-01-01', '2010-01-01'),
(4, 'Quebrado Vivo', '_e95364783bfae351782f9650038efd96.jpg', 'Teatro Coliseo', '2010-01-01', '2010-01-01'),
(5, 'THAT ONE NIGHT', '_3907bd9b46ff2fc54627ae32915d323f.jpg', 'ESTADIO OBRAS SANITARIAS', '2010-01-01', '2010-01-01');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
