-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 19-09-2015 a las 19:59:14
-- Versión del servidor: 5.5.41-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `im_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `im_artista`
--

CREATE TABLE IF NOT EXISTS `im_artista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombreCorto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=58 ;

--
-- Volcado de datos para la tabla `im_artista`
--

INSERT INTO `im_artista` (`id`, `nombre`, `nombreCorto`) VALUES
(1, 'DIVIDIDOS', 'DVD2'),
(2, 'STONE TEMPLE PILOTS', 'STP'),
(3, 'LED ZEPPELIN', 'LZ'),
(4, 'GUSTAVO CERATI', 'GC'),
(5, 'LUIS ALBERTO SPINETTA', 'LAS'),
(6, 'MASSIVE ATTACK', 'MA'),
(7, 'MORCHEEBA', 'MB'),
(8, 'MORPHINE', 'MP'),
(9, 'PEARL JAM', 'PJ'),
(10, 'PORTISHEAD', 'PH'),
(11, 'PEDRO AZNAR', 'PA'),
(12, 'SADE', 'SD'),
(13, 'SODA STEREO', 'SS'),
(14, 'SOUNDGARDEN', 'SG'),
(15, 'THE GATHERING', 'TG'),
(16, 'THE CULT', 'TC'),
(17, 'HOOVERPHONIC', 'HP'),
(18, 'Queens Of The Stone Age', 'QOTA'),
(19, 'DEEP PURPLE', 'DP'),
(20, 'MEGADETH', 'MD'),
(21, 'SIMPLY RED', 'SR'),
(22, 'RACA NEGRA', 'RN'),
(23, 'KYUSS', 'KS'),
(24, 'THE SMASHING PUMKINGS', 'SP'),
(25, 'RADIOHEAD', 'RH'),
(26, 'METALLICA', 'MT'),
(27, 'SLAYER', 'SY'),
(28, 'JOE SATRIANI', 'JS'),
(29, 'ERIC JHONSON', 'EJ'),
(30, 'STEVE VAI', 'SV'),
(31, 'VARGAS BLUES BAND', 'VBB'),
(32, 'THE CURE', 'TCR'),
(33, 'ACDC', 'ACDC'),
(34, 'RAGE AGAINST THE MACHINE', 'RATM'),
(35, 'AUDIOSLAVE', 'AS'),
(36, 'NIRVANA', 'NV'),
(37, 'IRON MAIDEN', 'IM'),
(38, 'GODSMACK', 'GM'),
(39, 'THE ROLLING STONE', 'RS'),
(40, 'THE BEATLES', 'TB'),
(41, 'JIMMY HENDRIX', 'JH'),
(42, 'PANTERA', 'PTR'),
(43, 'PINK FLOYD', 'PF'),
(44, 'DAVID GILMOUR', 'DG'),
(45, 'SUMO', 'SM'),
(46, 'U2', 'U2'),
(47, 'TOOL', 'TOOL'),
(48, 'LUIS SALINAS', 'LS'),
(49, 'MIGUEL MATEOS', 'MM'),
(50, 'LOS ABUELOS DE LA NADA', 'LAN'),
(51, 'INXS', 'INXS'),
(52, 'CHRIS REA', 'CR'),
(53, 'ALAN  PARSONS', 'AP'),
(54, 'A-HA', 'A-HA'),
(55, 'CHARLY GARCIA', 'CG'),
(56, 'NORAH JONES', 'NJ'),
(57, '3 DOORS DOWN', '3DD');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
