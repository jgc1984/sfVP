<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


class PlaylistTrack
{
    

        
    /**
     * @var integer
     */
    private $position;

    /**
     * @var \AppBundle\Entity\Playlist
     */
    private $playlist;

    /**
     * @var \AppBundle\Entity\Track
     */
    private $track;


    /**
     * Set position
     *
     * @param integer $position
     * @return PlaylistTrack
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set playlist
     *
     * @param \AppBundle\Entity\Playlist $playlist
     * @return PlaylistTrack
     */
    public function setPlaylist(\AppBundle\Entity\Playlist $playlist = null)
    {
        $this->playlist = $playlist;

        return $this;
    }

    /**
     * Get playlist
     *
     * @return \AppBundle\Entity\Playlist 
     */
    public function getPlaylist()
    {
        return $this->playlist;
    }

    /**
     * Set track
     *
     * @param \AppBundle\Entity\Track $track
     * @return PlaylistTrack
     */
    public function setTrack(\AppBundle\Entity\Track $track = null)
    {
        $this->track = $track;

        return $this;
    }

    /**
     * Get track
     *
     * @return \AppBundle\Entity\Track 
     */
    public function getTrack()
    {
        return $this->track;
    }
    /**
     * @var integer
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
