<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Fuente
 */
class Fuente
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $portada;

    /**
     * @var string
     */
    private $lugarGrabacion;

    /**
     * @var DateTime
     */
    private $fechaRecorded;

    /**
     * @var DateTime
     */
    private $fechaPublished;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Fuente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set portada
     *
     * @param string $portada
     * @return Fuente
     */
    public function setPortada($portada)
    {
        $this->portada = $portada;

        return $this;
    }

    /**
     * Get portada
     *
     * @return string 
     */
    public function getPortada()
    {
        return $this->portada;
    }

    /**
     * Set lugarGrabacion
     *
     * @param string $lugarGrabacion
     * @return Fuente
     */
    public function setLugarGrabacion($lugarGrabacion)
    {
        $this->lugarGrabacion = $lugarGrabacion;

        return $this;
    }

    /**
     * Get lugarGrabacion
     *
     * @return string 
     */
    public function getLugarGrabacion()
    {
        return $this->lugarGrabacion;
    }

    /**
     * Set fechaRecorded
     *
     * @param DateTime $fechaRecorded
     * @return Fuente
     */
    public function setFechaRecorded($fechaRecorded)
    {
        $this->fechaRecorded = $fechaRecorded;

        return $this;
    }

    /**
     * Get fechaRecorded
     *
     * @return DateTime 
     */
    public function getFechaRecorded()
    {
        return $this->fechaRecorded;
    }

    /**
     * Set fechaPublished
     *
     * @param DateTime $fechaPublished
     * @return Fuente
     */
    public function setFechaPublished($fechaPublished)
    {
        $this->fechaPublished = $fechaPublished;

        return $this;
    }

    /**
     * Get fechaPublished
     *
     * @return DateTime 
     */
    public function getFechaPublished()
    {
        return $this->fechaPublished;
    }
    /**
     * @var Collection
     */
    private $tracks;

    /**
     * @var Artista
     */
    private $artista;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tracks = new ArrayCollection();
    }

   

    /**
     * Set artista
     *
     * @param Artista $artista
     * @return Fuente
     */
    public function setArtista(Artista $artista = null)
    {
        $this->artista = $artista;

        return $this;
    }

    /**
     * Get artista
     *
     * @return Artista 
     */
    public function getArtista()
    {
        return $this->artista;
    }
    
    public function __toString()
    {
        //$string = $this->getFechaPublished()."-".$this->getNombre();
        return strtoupper($this->getNombre());
    }

    /**
     * Add tracks
     *
     * @param \AppBundle\Entity\Track $tracks
     * @return Fuente
     */
    public function addTrack(\AppBundle\Entity\Track $tracks)
    {
        $this->tracks[] = $tracks;

        return $this;
    }

    /*public function setTracks(\Doctrine\Common\Collections\Collection $tracks)
    {
        $this->tracks = $tracks;
        foreach ($tracks as $track) {
            $tracks->setFuente($this);
        }
    }*/

    public function setTracks(\AppBundle\Entity\Track $tracks) 
    {
        $tracks->setFuente($this);
        $this->tracks->add($tracks);
        
        return $this;    
    } 

    /**
     * Remove tracks
     *
     * @param \AppBundle\Entity\Track $tracks
     */
    public function removeTrack(\AppBundle\Entity\Track $tracks)
    {
        $this->tracks->removeElement($tracks);
    }

    /**
     * Get tracks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTracks()
    {
//        return $this->tracks;
        return $this->tracks ? : $this->tracks = new ArrayCollection();
    }
    /**
     * @var string
     */
    private $soporte;


    /**
     * Set soporte
     *
     * @param string $soporte
     * @return Fuente
     */
    public function setSoporte($soporte)
    {
        $this->soporte = $soporte;

        return $this;
    }

    /**
     * Get soporte
     *
     * @return string 
     */
    public function getSoporte()
    {
        return $this->soporte;
    }
}
