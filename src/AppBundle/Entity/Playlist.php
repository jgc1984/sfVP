<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tracklist
 */
class Playlist
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $cantReproducciones;

    /**
     * @var \DateTime
     */
    private $fechaLastPLay;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tracks;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tracks = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Tracklist
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Tracklist
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set cantReproducciones
     *
     * @param string $cantReproducciones
     * @return Tracklist
     */
    public function setCantReproducciones($cantReproducciones)
    {
        $this->cantReproducciones = $cantReproducciones;

        return $this;
    }

    /**
     * Get cantReproducciones
     *
     * @return string 
     */
    public function getCantReproducciones()
    {
        return $this->cantReproducciones;
    }

    /**
     * Set fechaLastPLay
     *
     * @param \DateTime $fechaLastPLay
     * @return Tracklist
     */
    public function setFechaLastPLay($fechaLastPLay)
    {
        $this->fechaLastPLay = $fechaLastPLay;

        return $this;
    }

    /**
     * Get fechaLastPLay
     *
     * @return \DateTime 
     */
    public function getFechaLastPLay()
    {
        return $this->fechaLastPLay;
    }

    

    /**
     * Set duracion
     *
     * @param \DateTime $duracion
     * @return Tracklist
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * Get duracion
     *
     * @return \DateTime 
     */
    public function getDuracion()
    {
        return $this->duracion;
    }
    /**
     * @var integer
     */
    private $cantTracks;

    /**
     * @var string
     */
    private $tipo;


    /**
     * Set cantTracks
     *
     * @param integer $cantTracks
     * @return Tracklist
     */
    public function setCantTracks($cantTracks)
    {
        $this->cantTracks = $cantTracks;

        return $this;
    }

    /**
     * Get cantTracks
     *
     * @return integer 
     */
    public function getCantTracks()
    {
        return $this->cantTracks;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Tracklist
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }
    
     public function __toString()
    {
        return $this->nombre;
    }


    
    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;


    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Tracklist
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Tracklist
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    /**
     * @var integer
     */
    private $position;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tracklist;


    /**
     * Set position
     *
     * @param integer $position
     * @return Playlist
     */
    public function setPosition(\AppBundle\Entity\Track $track)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    
    /**
     * @var \DateTime
     */
    private $duracion;

   

    /**
     * Add tracks
     *
     * @param \AppBundle\Entity\Track $tracks
     * @return Playlist
     */
    public function addTrack(\AppBundle\Entity\Track $tracks)
    {
        $this->tracks[] = $tracks;

        return $this;
    }

    /**
     * Remove tracks
     *
     * @param \AppBundle\Entity\Track $tracks
     */
    public function removeTrack(\AppBundle\Entity\Track $tracks)
    {
        $this->tracks->removeElement($tracks);
    }

    /**
     * Get tracks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTracks()
    {
        return $this->tracks;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $users;


    

    public function generatePlaylist(){

        $nombre = $this->getNombre().".m3u";
        $arch = "/home/gabriel/Vídeos/playlists/".$nombre;
        
        $hdn = fopen( $arch,'w');
        fwrite($hdn,"#EXTM3U\n");


        foreach($this->getTracks() as $track){  
                $string = "\n#EXTINF:-1,";
                $string .= $track->getTitulo();                
                $string .= ".".$track->getFormato()."\n";                               
                fwrite($hdn, $string);
                fwrite($hdn, $track->getPath());                                                        
        }
        fclose($hdn);  
    }
    /**
     * @var \DateTime
     */
    private $hora_inicio;

    /**
     * @var \DateTime
     */
    private $hora_fin;


    /**
     * Set hora_inicio
     *
     * @param \DateTime $horaInicio
     * @return Playlist
     */
    public function setHoraInicio($horaInicio)
    {
        $this->hora_inicio = $horaInicio;

        return $this;
    }

    /**
     * Get hora_inicio
     *
     * @return \DateTime 
     */
    public function getHoraInicio()
    {
        return $this->hora_inicio;
    }

    /**
     * Set hora_fin
     *
     * @param \DateTime $horaFin
     * @return Playlist
     */
    public function setHoraFin($horaFin)
    {
        $this->hora_fin = $horaFin;

        return $this;
    }

    /**
     * Get hora_fin
     *
     * @return \DateTime 
     */
    public function getHoraFin()
    {
        return $this->hora_fin;
    }

    public function getDuration(){
        $parser = new MediaParser();
        $data = $parser->getFileInformation($this->getPath());
        $total = 0;
        foreach ($this->getTracks() as $track) {
            
        }
    }

    public function getInfoDuration(){
        $total = 0;
        foreach ($this->getTracks() as $track) {
            $parser = new MediaParser();
            $data = $parser->getFileInformation($track->getPath());                    
            $total += $data['duration']->total_milliseconds;   
        }

        //$timecode = new Timecode($total, Timecode::INPUT_FORMAT_TIMECODE); 
        // extract hours
        
        $obj = $this->calculateTimeDuration($total);

        return $obj['h'].":".$obj['m'].":".$obj['s'];
        
    }


    public function calculateTimeDuration($seconds)
    {
        // extract hours
        $hours = floor($seconds / (60 * 60));
     
        // extract minutes
        $divisor_for_minutes = $seconds % (60 * 60);
        $minutes = floor($divisor_for_minutes / 60);
     
        // extract the remaining seconds
        $divisor_for_seconds = $divisor_for_minutes % 60;
        $seconds = ceil($divisor_for_seconds);
     
        // return the final array
        $obj = array(
            "h" => (int) $hours,
            "m" => (int) $minutes,
            "s" => (int) $seconds,
        );


        return $obj;
    }





    
    /**
     * @var \UsuarioBundle\Entity\Usuario
     */
    private $usuario;


    /**
     * Set usuario
     *
     * @param \UsuarioBundle\Entity\Usuario $usuario
     * @return Playlist
     */
    public function setUsuario(\UsuarioBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \UsuarioBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $trackPlaylist;


    /**
     * Add trackPlaylist
     *
     * @param \AppBundle\Entity\Track $trackPlaylist
     * @return Playlist
     */
    public function addTrackPlaylist(\AppBundle\Entity\Track $trackPlaylist)
    {
        $this->trackPlaylist[] = $trackPlaylist;

        return $this;
    }

    /**
     * Remove trackPlaylist
     *
     * @param \AppBundle\Entity\Track $trackPlaylist
     */
    public function removeTrackPlaylist(\AppBundle\Entity\Track $trackPlaylist)
    {
        $this->trackPlaylist->removeElement($trackPlaylist);
    }

    /**
     * Get trackPlaylist
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTrackPlaylist()
    {
        return $this->trackPlaylist;
    }
}
