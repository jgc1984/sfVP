<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PHPVideoToolkit\MediaParser;
use DateTime;


/**
 * Track
 */
class Track
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $titulo;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $artista;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Track
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set nombreArchivo
     *
     * @param string $nombreArchivo
     * @return Track
     */
    public function setNombreArchivo($nombreArchivo)
    {
        $this->nombre_archivo = $nombreArchivo;

        return $this;
    }

    /**
     * Get nombreArchivo
     *
     * @return string 
     */
    public function getNombreArchivo()
    {
        return $this->nombre_archivo;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Track
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set artista
     *
     * @param string $artista
     * @return Track
     */
    public function setArtista($artista)
    {
        $this->artista = $artista;

        return $this;
    }

    /**
     * Get artista
     *
     * @return string 
     */
    public function getArtista()
    {
        return $this->artista;
    }
    /**
     * @var string
     */
    private $dvdOriginal;


    /**
     * Set dvdOriginal
     *
     * @param string $dvdOriginal
     * @return Track
     */
    public function setDvdOriginal($dvdOriginal)
    {
        $this->dvdOriginal = $dvdOriginal;

        return $this;
    }

    /**
     * Get dvdOriginal
     *
     * @return string 
     */
    public function getDvdOriginal()
    {
        return $this->dvdOriginal;
    }
    /**
     * @var string
     */
    private $formato;



    /**
     * @var integer
     */
    private $nroTrack;


    /**
     * Set formato
     *
     * @param string $formato
     * @return Track
     */
    public function setFormato($formato)
    {
        $this->formato = $formato;

        return $this;
    }

    /**
     * Get formato
     *
     * @return string 
     */
    public function getFormato()
    {
        return $this->formato;
    }



    /**
     * Set nroTrack
     *
     * @param integer $nroTrack
     * @return Track
     */
    public function setNroTrack($nroTrack)
    {
        $this->nroTrack = $nroTrack;

        return $this;
    }

    /**
     * Get nroTrack
     *
     * @return integer 
     */
    public function getNroTrack()
    {
        return $this->nroTrack;
    }

  
    /**
     * Set genero
     *
     * @param string $genero
     * @return Track
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return string 
     */
    public function getGenero()
    {
        return $this->genero;
    }
    
    public function __toString() {
        return $this->getTitulo();
    }
    /**
     * @var string
     */
    private $portada;


    /**
     * Set portada
     *
     * @param string $portada
     * @return Track
     */
    public function setPortada($portada)
    {
        $this->portada = $portada;

        return $this;
    }

    /**
     * Get portada
     *
     * @return string 
     */
    public function getPortada()
    {
        return $this->portada;
    }
    

    public function isFile(){        
        return ( file_exists($this->getPath()) )? true: false;
    }


   
   
    /**
     * @var string
     */
    private $nombre_archivo;

  

    /**
     * @var boolean
     */
    private $favorito;

    /**
     * @var string
     */
    private $calidad;

    /**
     * @var string
     */
    private $quality_video;

    /**
     * @var string
     */
    private $quality_audio;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var \DateTime
     */
    private $last_play;


    /**
     * @var \AppBundle\Entity\Fuente
     */
    private $fuente;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $playlists;


    

    /**
     * Set favorito
     *
     * @param boolean $favorito
     * @return Track
     */
    public function setFavorito($favorito)
    {
        $this->favorito = $favorito;

        return $this;
    }

    /**
     * Get favorito
     *
     * @return boolean 
     */
    public function getFavorito()
    {
        return $this->favorito;
    }

    /**
     * Set calidad
     *
     * @param string $calidad
     * @return Track
     */
    public function setCalidad($calidad)
    {
        $this->calidad = $calidad;

        return $this;
    }

    /**
     * Get calidad
     *
     * @return string 
     */
    public function getCalidad()
    {
        return $this->calidad;
    }

    /**
     * Set quality_video
     *
     * @param string $qualityVideo
     * @return Track
     */
    public function setQualityVideo($qualityVideo)
    {
        $this->quality_video = $qualityVideo;

        return $this;
    }

    /**
     * Get quality_video
     *
     * @return string 
     */
    public function getQualityVideo()
    {
        return $this->quality_video;
    }

    /**
     * Set quality_audio
     *
     * @param string $qualityAudio
     * @return Track
     */
    public function setQualityAudio($qualityAudio)
    {
        $this->quality_audio = $qualityAudio;

        return $this;
    }

    /**
     * Get quality_audio
     *
     * @return string 
     */
    public function getQualityAudio()
    {
        return $this->quality_audio;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Track
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Track
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set last_play
     *
     * @param \DateTime $lastPlay
     * @return Track
     */
    public function setLastPlay($lastPlay)
    {
        $this->last_play = $lastPlay;

        return $this;
    }

    /**
     * Get last_play
     *
     * @return \DateTime 
     */
    public function getLastPlay()
    {
        return $this->last_play;
    }


    /**
     * Set fuente
     *
     * @param \AppBundle\Entity\Fuente $fuente
     * @return Track
     */
    public function setFuente(\AppBundle\Entity\Fuente $fuente = null)
    {
        $this->fuente = $fuente;

        return $this;
    }

    /**
     * Get fuente
     *
     * @return \AppBundle\Entity\Fuente 
     */
    public function getFuente()
    {
        return $this->fuente;
    }

    /**
     * Add playlists
     *
     * @param \AppBundle\Entity\Playlist $playlists
     * @return Track
     */
    public function addPlaylist(\AppBundle\Entity\Playlist $playlists)
    {
        $this->playlists[] = $playlists;

        return $this;
    }

    /**
     * Remove playlists
     *
     * @param \AppBundle\Entity\Playlist $playlists
     */
    public function removePlaylist(\AppBundle\Entity\Playlist $playlists)
    {
        $this->playlists->removeElement($playlists);
    }

    /**
     * Get playlists
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlaylists()
    {
        return $this->playlists;
    }
    /**
     * @var integer
     */
    private $reproducido;

    /**
     * @var integer
     */
    private $nro_track;

    /**
     * @var string
     */
    private $size;


    /**
     * Set reproducido
     *
     * @param integer $reproducido
     * @return Track
     */
    public function setReproducido($reproducido)
    {
        $this->reproducido = $reproducido;

        return $this;
    }

    /**
     * Get reproducido
     *
     * @return integer 
     */
    public function getReproducido()
    {
        return $this->reproducido;
    }

 
    public function getPosicionInPlaylist(\AppBundle\Entity\Playlist $playlist)
    {
        
        $this->tracks[] = $tracks;

        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->playlists = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public static function getTime( $file )
    {
        $parser = new MediaParser();
        
        $data = $parser->getFileInformation($file);

        $time = mktime(0, $data['duration']->minutes, $data['duration']->seconds );
                        
        $fecha = new DateTime('NOW');

        $fecha->setTime(0, $data['duration']->minutes,  $data['duration']->seconds);
                                                
        return $fecha->format("i:s");                

    }

    public static function getResolution( $file )
    {
        $parser = new MediaParser();
        
        $data = $parser->getFileInformation($file);
        
        $width = $data['video']['dimensions']['width'];
        $height = $data['video']['dimensions']['height'];
                        
        $image['resolution']['width'] = $width;                        
        $image['resolution']['height'] = $height;

        return $image;

    }

    public static function getRawInfo( $file )
    {
        $parser = new MediaParser();
        
        $data = $parser->getFileInformation($file);
        
        $info['total_seconds'] = $data['duration']->total_seconds;

        $width = $data['video']['dimensions']['width'];
        $height = $data['video']['dimensions']['height'];
                        
        $info['resolution']['width'] = $width;                        
        $info['resolution']['height'] = $height;

        return $info;

    }




    public static function getRawTime( $file )
    {
        $parser = new MediaParser();
        
        $data = $parser->getFileInformation($file);

        return $data['duration']->total_seconds;
        
    }    


    public static function formatSizeUnits( $bytes ){        

        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;  
    }   

    /**
     * @var float
     */
    private $duracion;

    /**
     * @var \AppBundle\Entity\Genero
     */
    private $genero;


    /**
     * Set duracion
     *
     * @param float $duracion
     * @return Track
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * Get duracion
     *
     * @return float 
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return Track
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }
}
