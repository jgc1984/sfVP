<?php
/*
 Folder Tree with PHP and jQuery.

 R. Savoul Pelister
 http://techlister.com

*/
namespace AppBundle\lib;

class TreeView {

	private $files;
	private $folder;
	
	function __construct( $path ) {
		
		$files = array();	
		
		if( file_exists( $path)) {
			if( $path[ strlen( $path ) - 1 ] ==  '/' )
				$this->folder = $path;
			else
				$this->folder = $path . '/';
			
			$this->dir = opendir( $path );
			while(( $file = readdir( $this->dir ) ) != false )
				$this->files[] = $file;
			closedir( $this->dir );
		}
	}

	function create_tree2( ) {
			
		if( count( $this->files ) > 2 ) { /* First 2 entries are . and ..  -skip them */
			natcasesort( $this->files );
			$list = '<ul class="filetree" style="display: none;">';
			// Group folders first
			foreach( $this->files as $file ) {
				if( file_exists( $this->folder . $file ) && $file != '.' && $file != '..' && is_dir( $this->folder . $file )) {
					$input = "<input type='checkbox' value='".$this->folder."'/>";
					$list .= '<li class="folder collapsed"><a href="#" rel="' . htmlentities( $this->folder . $file ) . '/"> '.$input.' ' . htmlentities( $file ) . '</a></li>';
				}
			}
			// Group all files
			foreach( $this->files as $file ) {
				if( file_exists( $this->folder . $file ) && $file != '.' && $file != '..' && !is_dir( $this->folder . $file )) {
					$ext = preg_replace('/^.*\./', '', $file);
					$list .= '<li class="file ext_' . $ext . '"><a href="#" rel="' . htmlentities( $this->folder . $file ) . '">' . htmlentities( $file ) . '</a></li>';
				}
			}
			$list .= '</ul>';	
			return $list;
		}
	}


	function create_tree( $name = NULL  ) {
		$select = '<select class="form-control">';
		$select .= "<option value='".$this->folder."'>Elejir</option>";									
		$select .= "</select>";
		$select .= "<span class='glyphicon glyphicon-folder-close' aria-hidden='true'></span>";
		$select .= "<i class='fa fa-plus addFolder'></i>";
		if( count( $this->files ) > 2 ) {							
			natcasesort( $this->files );					
			$select = '<select class="form-control" name="'. $name .'">';
			$select .= "<option value='".$this->folder."'>Elejir</option>";									
			foreach( $this->files as $file ) {					
				if( file_exists( $this->folder . $file ) && $file != '.' && $file != '..' && is_dir( $this->folder . $file )) {							
					$select .= "<option value='".$this->folder . $file."'>".$file."</option>";									
				}	
			}	

			$select .= "</select>";
			$select .= "<span class='glyphicon glyphicon-folder-close' aria-hidden='true'></span>";
			//
			$select .="<i class='fa fa-plus addFolder'></i>";
			
			
		} 
		return $select;
	}

	
}