<?php

namespace AppBundle\Menu;

use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class MenuBuilder extends ContainerAware {

    protected $user;

    /**
     *
     * Función que Arma la estructura principal de menu
     *
     * @param FactoryInterface $menu
     * @param array $options
     */
    public function mainMenu(FactoryInterface $factory, array $options) {


        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'sidebar-menu');

        $menu->addChild('Dashboard', array('route' => 'homepage'))
                ->setAttribute('class', 'active')
                ->setAttribute('icon', 'fa fa-dashboard');
        
        if ($this->container->get('security.context')->isGranted(array('ROLE_USER'))) {
            $menu = $this->roleSuperAdmin($menu);
        }
        
        if ($this->container->get('security.context')->isGranted(array('ROLE_SECRETARIO'))) {
            $menu = $this->roleSecretario($menu);
        }
        
        if ($this->container->get('security.context')->isGranted(array('ROLE_PROFESOR'))) {
            $menu = $this->roleProfesor($menu);
        }

        return $menu;
    }
    
    public function createBreadcrumbsMenu(Request $request) {
    	//
        $bcmenu = $this->createMainMenu($request);
        return $this->getCurrentMenuItem($bcmenu);
    }
    
    public function getCurrentMenuItem($menu)
    {
        $voter = $this->container->get('centrodia.theme.menu.request');
		
        foreach ($menu as $item) {
            if ($voter->matchItem($item)) {
                return $item;
            }
			
            if ($item->getChildren() && $currentChild = $this->getCurrentMenuItem($item)) {
                return $currentChild;
            }
        }
		
        return null;
    }

    public function roleSuperAdmin($menu) {
        $menu = $this->empadronadorAdminMenu($menu);
        
                
        return $menu;
    }
    
    public function roleSecretario($menu) {
        $menu = $this->empadronadorMenu($menu);        
        $menu = $this->academicoMenu($menu);        
                
        return $menu;
    }
    
    public function roleProfesor($menu) {
        $menu = $this->empadronadorMenu($menu);                
                
        return $menu;
    }
    
    public function empadronadorAdminMenu($menu){
        $menu->addChild('empadronador', array('uri' => '#', 'label' => 'Empadronador'))                
                ->setAttribute('class', 'treeview');

        $menu['empadronador']->setChildrenAttribute('class', 'treeview-menu');

        $menu['empadronador']->setAttribute('icon', 'fa fa-users');
        
        $menu['empadronador']->addChild('tracks', array('uri' => '#', 'route' => 'track', 'label' => 'Alumnos'));
                                               
        $menu['empadronador']->addChild('playlists', array('route' => 'playlist', 'label' => 'Padres'));                

        $menu['empadronador']->addChild('fuente', array('route' => 'fuente', 'label' => 'Personal'));                
        
        
        
        return $menu;
    }
    
   
    
    
    public function userMenu(FactoryInterface $factory, array $options) {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav pull-right');

        $menu->addChild('User', array('label' => 'Hi visitor'))
                ->setAttribute('dropdown', true)
                ->setAttribute('icon', 'icon-user');

        $menu['User']->addChild('Edit profile', array('route' => 'acme_hello_profile'))
                ->setAttribute('icon', 'icon-edit');

        return $menu;
    }

}
