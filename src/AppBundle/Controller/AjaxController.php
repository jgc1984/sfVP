<?php

namespace AppBundle\Controller;

use AppBundle\Form\SettingsType;


use AppBundle\Form\filters\TrackFilterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Playlist;
use AppBundle\Entity\PlaylistTrack;
use AppBundle\Form\PlaylistType;
use AppBundle\Entity\Settings;
use AppBundle\Form\filters\TrackSearchAdvancedFilterType;
use AppBundle\Form\FindTrackType;
use Symfony\Component\Finder\Finder;
use PHPVideoToolkit\Timecode;
use PHPVideoToolkit\Video;
use PHPVideoToolkit\MediaParser;
use AppBundle\Entity\Track;
use Exception;
use DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;


class AjaxController extends Controller
{
    const DIR_USB = "/dev/sdb1";
    const DIR_DEFAULT_USB = "/media/";
    const FILE_USB_MOUNT = "/etc/mtab";
    const DIR_HOME = "/home/gabriel/Vídeos/playlists/"; 

    public function getAjaxDefaultAction(Request $request) {
        if( $request->isXmlHttpRequest() )
        {         
            $place = $request->get('place');

            switch ( $place ) {
                case 'pc':
                    $json = $this->searchInLocal( $request );        
                    break;

                case 'youtube':
                    $json = $this->searchInYoutube( $request );        
                    break;                            
            }
        }    
                    
        $response = new Response();
        $response->setContent(json_encode($json));
        
        return $response;
    }   
    
    public function getAjaxTracksByTracklistAction(Request $request) {
        
        $appManager = $this->get('app.manager');                            

        $tracks = $appManager->getTracksByPlaylist( $request->get('padre') );
        
        $info = $appManager->getInfoByPlaylist( $request->get('padre') );
                   
        $seconds = 0;    

        foreach ($tracks as $track) {            
            $seconds += $track['duracion'];            
        }    

        $totalDuration['duration'] = gmdate("H:i:s", $seconds);

        $playlistInfo = array_merge($info[0], $totalDuration);
        
        $playlist['info'] = $playlistInfo;        

        $playlist['tracks'] = $tracks;
        
        return $this->render('AppBundle:Playlist:listas.html.twig', array(
            'playlist' =>  $playlist,
        ));
       
    }

    public function getAjaxTracksByArtistaAction(Request $request) {
        
        $appManager = $this->get('app.manager');            

        $em = $this->getDoctrine()->getManager();   

        $tracks = $appManager->getTracksByArtista( $request->get('artista') );

        $artista['nombre'] = $request->get('artista');

        $artista['tracks'] = $tracks;        

        return $this->render('AppBundle:Artista:_tracks.html.twig', array(
            'artista' =>  $artista,
        ));
       
    }
    
        
    
    private function getThumbnails( $artistas, $client ){        
        
        $artistasThumbnails = array();

        foreach ($artistas as $artista) {
            $pathTemp = $this->get('kernel')->getRootDir() . '/../web/thumbnails/artistas/'; 
            $thumbFile = strtolower($artista->getNombre()).".jpg";
            $pathThumb = $pathTemp . $thumbFile;

            if( !file_exists( $pathThumb )){                
                $youtube = new \Google_Service_YouTube($client);
                $thumb = $this->downloadThumbnail($youtube, $artista);
                $artistaThumbnail['thumbnail'] = $thumb;
            }else{
                $artistaThumbnail['thumbnail'] = $thumbFile;
            }

            $artistaThumbnail['id'] = $artista->getId();
            $artistaThumbnail['artista'] = $artista->getNombre();
            $artistasThumbnails[] = $artistaThumbnail;
                    
        }        
        return $artistasThumbnails;                   
    } 



    private function downloadThumbnail($youtube, $artista ){
        $artistaResponse = $youtube->search->listSearch('id,snippet', array(
              'q' => strtolower($artista->getNombre()),
              'maxResults' => 1,
        ));    
    
        $thumb = $artistaResponse->getSnippet()->getThumbnails()->getDefault()['url'];
                
        if( !file_exists( $thumbFile )){
            copy( $thumb, $thumbFile );    
        }                
                                        
        return $thumbFile;
    }
    

    public function ajaxSearchAdvancedAction( Request $request){
        
        $dataSearch = json_decode($request->get('search'), true);

        $paginador = $this->get('knp_paginator');    

        $appManager = $this->get('app.manager');                        
                    
        $datos = $appManager->filtrarDatos($dataSearch); 

        
        return $this->render('AppBundle:Track:busqueda.html.twig', array(
            'datos' => $datos,
        ));
                        

    }

    public function getDirFilesDestinationAction(){
        $dir = "/media/DATOS/VIDEOS";

        $appManager = $this->get('app.manager');

        $folders = $appManager->getInfoDir( $dir );       

        return $this->render('AppBundle:Track:_folders.html.twig', array(
            'folders' => $folders,
        ));

    }
    


    /*public function ajaxSearchYoutubeAction( Request $request ){
        $client = new Google_Client();

        $happy = $this->container->getParameter('happy_r_google_api');


        $client->setClientId($happy['oauth2_client_id']);
        $client->setClientSecret($happy['oauth2_client_secret']);
        $client->setScopes('https://www.googleapis.com/auth/youtube');

        $happy = $this->container->getParameter('happy_r_google_api');
        
        $client->setDeveloperKey($happy['developer_key']);

        $youtube = new Google_Service_YouTube($client);

        //var_dump($client);
        //var_dump($youtube);

        $channelsResponse = $youtube->channels->listChannels('contentDetails', array(
            'mine' => 'true',
        ));
        var_dump($channelsResponse);

    }*/


    public function savePlaylistAction( Request $request){
        
        $dataSearch = json_decode($request->get('data'), true);

        $appManager = $this->get('app.manager');     

        $em = $this->getDoctrine()->getManager();

        $playlistRepo = $em->getRepository('AppBundle:Track');

        foreach ($dataSearch as $key => $value) {

            if( $value['name'] == "track"){
                $track = $playlistRepo->find($value['value']);
                $playlist = new PlaylistTrack();                
                $playlist->addTrack($track);                
            }elseif( $value['name'] == "listnew") {
                $playlist->setNombre($value['value']);
                $playlist->setDescripcion("Agreagado frontend");
            }

            
        }   

        $em->persist($playlist);     

        $em->flush();

        $json = json_encode(array('status'=> 'OK'));

        return new Response($json);
                        

    }


    public function ajaxGetPlaylistsAction( Request $request){
        
        $appManager = $this->get('app.manager');                        
                  
        $em = $this->getDoctrine()->getManager();

        $playlists = $em->getRepository('AppBundle:Playlist')->findBy( array('usuario'=> $this->getUser()->getId()) );

        //$playlists = $em->getRepository('AppBundle:Playlist')->findByUsuario($this->getUser()->getId());
                
        return $this->render('AppBundle:Home:_playlists.html.twig', array(
            'playlists' => $playlists,
            
        ));            
    }

    
    public function getAjaxTracksByFuenteAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('AppBundle:Fuente')->findOneBy(array('id' => $request->get('fuente')));
                    
        $tracks = $entity->getTracks();

        $template = "tracks.html.twig";
        
     
        return $this->render('AppBundle:Fuente:'.$template, array(
            'tracklist' => $entity,
        ));
       
    }   

    /*public function getAjaxFilesByDirAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
                
        $dirRoot  = $request->get('dir');        
        
        $appManager = $this->get('app.manager');  

        $files = $appManager->getFilesByDir( $dirRoot );
        
     
        return $this->render('AppBundle:Track:_files10.html.twig', array(
            'files' => $files,
            'dir'=>$dirRoot
        ));
       
    }*/   

    /*public function ajaxRootMediaAction( Request $request ){
        
        $pathRootMedia = $request->get('dir');     

        $name = $request->get('name');   
        
        $appManager = $this->get('app.manager');

        //$files = $appManager->getListFiles($pathRootMedia, false, true ); 
        $files = $appManager->getFilesPlane($pathRootMedia, true, $name);

        //$json = json_encode(array('status'=> 'OK', 'data'=> $files));
        
        return new Response(json_encode($files));
        //return new Response($files);

    }*/

    /*public function importLibraryAction( Request $request ){

        $dirRoot = "/home/gabriel";

        $appManager = $this->get('app.manager');

        $directories = $appManager->getInfoDir( $dirRoot );
        
        return $this->render('AppBundle:Track:import_library.html.twig', array('dirs'=>$directories));

    }*/

    public function ajaxRenameFileAction( Request $request ){

        $fileOld  = $request->get('fileOld');
        $fileNew  = $request->get('fileNew');

        if( file_exists($fileOld)){
            $newFile = $this->createRealPathNewFile( $fileOld, $fileNew );

            if ( rename($fileOld, $newFile ) ){
                $json = json_encode(array('status'=> 'OK', 'data'=> $newFile));
                //$json = array('status'=> 'OK', 'data'=> $newFile);
            }                    
            
        } else {
            $json = array('status'=> 'Error', 'data'=> 'No existe el archivo');
        }
        return new Response($json);
        //$response = new JsonResponse( $json );                    
    }

    
    public function ajaxGetInfoRootAction( Request $request ){

        $em = $this->getDoctrine()->getManager();

        $dirRoot  = $request->get('dir'); 

        $place  = $request->get('place');        

        $template = "dir1";

        $appManager = $this->get('app.manager');  

        $sources = array();

        $nameFolder = substr($dirRoot, strrpos($dirRoot, "/") + 1 );      
        
        $genero = $em->getRepository('AppBundle:Genero')->findOneBy(array('nombre' => $nameFolder ) );

        if( $genero ){
            $sources['genero'] = $genero;
        }  


        $fuente = $em->getRepository('AppBundle:Fuente')->findOneBy(array('nombre' => $nameFolder ) );  

        if( $fuente ){
            $sources['fuente'] = $fuente;
        } 

        $artista = $em->getRepository('AppBundle:Artista')->findOneBy(array('nombre' => $nameFolder ) );  

        if( $artista ){
            $sources['artista'] = $artista;
        }   
        


        $subDirs = $appManager->getInfoDir( $dirRoot );        
                        
        return $this->render('AppBundle:Track:'.$template.'.html.twig', array('sources'=> $sources,'place'=> $place,  'subDirs'=> $subDirs, 'dir'=> strtolower($nameFolder) ));
        
    } 

    public function ajaxAnalizeFilesAction( Request $request ){
        
        $fileInfo  = json_decode($request->get('file'), true);
            
        $em = $this->getDoctrine()->getManager();

        if( is_array( $fileInfo ) ){
            if( count( $fileInfo ) > 0 ){                
                $pathWeb = $this->get('kernel')->getRootDir() . '/../web/track/tmp/';     

                $name = substr($fileInfo['filename'], strrpos($fileInfo['filename'], "/") + 1 );
                $fileTemp = $pathWeb . $name;                                                    

                $video  = new Video($fileInfo['filename']);                
            
                $process = $video->extractFrame(new Timecode(40))
                        ->save($fileTemp.".jpg");

                $image['image_thumb'] = $name;   
                $image['image'] = $name;  
                $image['path'] = substr($fileInfo['filename'], 0, strrpos($fileInfo['filename'], "/") + 1);                                                                                                 
                $image['size'] = filesize($fileInfo['filename']) . PHP_EOL;                                        
                $image['formatSizeUnits'] = Track::formatSizeUnits($image['size']);                                 
                $image['time'] = Track::getTime($fileInfo['filename']);
                $image['timeRaw'] = Track::getRawTime($fileInfo['filename']);
                $info = Track::getRawInfo($fileInfo['filename']);


                $image = array_merge($image, $info);
            
            }
        }
        
        return new JsonResponse($image);        
                
    }

    public function getTimeFilesAction( Request $request ){            
            
        $em = $this->getDoctrine()->getManager();

        $appManager = $this->get('app.manager'); 

        $playlist = $request->get('playlist');

        $tracks = $appManager->getTracksByPlaylist( $playlist );

        $trac = array();
        
        foreach ($tracks as $track) {                        
            $file = $track['path'] . $track['nombre_archivo'];
            $time = Track::getRawTime($file);
            
            $trackObject = $em->getRepository('AppBundle:Track')->findOneBy(array('nombre_archivo'=>$track['nombre_archivo']));
            
            $trackObject->setDuracion($time);

            $em->persist($trackObject);
            
        }

        $em->flush();
                    

        return new Response(json_encode(array('message'=> 'Se ha actualziad')));
                
    } 


    public function ajaxImportFilesAnalizedAction( Request $request ){

        $em = $this->getDoctrine()->getManager();

        $file  = json_decode( utf8_encode($request->get('file')), true);
                        
        $data  = json_decode( $request->get('extra'), true );

        $filesImport = array();

                
        if( is_array( $file ) ){
            if( count( $file ) > 0 ){
                $pathTemp = $this->get('kernel')->getRootDir() . '/../web/track/tmp/';     
                $fileTemp = $pathTemp . utf8_decode($file['filename']) . ".jpg";
                $pathTrack = $this->get('kernel')->getRootDir() . '/../web/track/';
                $fileImg = $pathTrack . $file['filename'] . ".jpg";

                $pathSource = $file['path']. $file['filename'];
                
                $pathTarget = $data['dirTarget']. "/" . $file['filename'];
                
                if( copy( $pathSource, $pathTarget) ) {
                    unlink( $pathSource );
                }
                
                if( copy( $fileTemp, $fileImg) ){                        
                    unlink( $fileTemp );
                }

                $pista = new Track();

                if( isset( $data['genero']) ){                        
                    $genero = $em->getRepository('AppBundle:Genero')->find((int)$data['genero']);
                
                    if( isset($genero) ){
                        $pista->setGenero($genero);
                    }  
                }                    

                if( isset( $data['fuente']) ){
                    $fuente = $em->getRepository('AppBundle:Fuente')->find((int)$data['fuente']);

                    if( isset( $fuente )  ){
                        $pista->setFuente($fuente);
                    }  
                }

                if( isset( $data['artista'])  ){

                    $artista = $em->getRepository('AppBundle:Artista')->find((int)$data['artista']);                        
                    
                    if( isset( $artista ) ){
                        $pista->setArtista($artista);                            
                    }  
                }

                $pista->setPath( $file['path'] );
                $pista->setTitulo( $file['filename'] );                    
                $pista->setNombreArchivo( $file['filename'] );      
                $pista->setSize( $file['size'] );                                                     
                $pista->setDuracion( $file['duration'] );  

                $em->persist($pista);            
                            
            }

            try {
                $em->flush();                    
                $response['status'] = "ok";
                $response['message'] = "se guardo";
                $response['file'] = $pista;

            } catch (Exception $exc) {
                $response['status'] = "error";
                $response['message'] = $exc->getMessage();
                $exc->getMessage();
            }
        } 

        return new Response( json_encode( $response ) );   

    }

    
    public function getAjaxFilesByDirAction( Request $request ){

        $dirRoot  = $request->get('dir');

        $em = $this->getDoctrine()->getManager();
        
        $appManager = $this->get('app.manager');  

        $files = $appManager->getFilesByDir( $dirRoot );
        

        return $this->render('AppBundle:Track:_files.html.twig', array('files'=> $files, 'dir'=> $dirRoot));
        
    } 

    //ajaxSavePlaylist
    public function ajaxSavePlaylistAction( Request $request ){

        $playlistNew  = $request->get('playlist');

        $tituloTrack  = $request->get('track');
        
        $em = $this->getDoctrine()->getManager();    
                
        $track = $em->getRepository('AppBundle:Track')->findOneBy(array('nombre_archivo'=> $tituloTrack));

        $genero = $em->getRepository('AppBundle:Genero')->findOneBy(array('nombre' => $track->getGenero()->getNombre()));    

        $playlist = new Playlist();                
        $playlist->setNombre($playlistNew);                
        $playlist->setDescripcion("agregado frontend");  
        $playlist->setUsuario($this->getUser());
        $em->persist($playlist);     
        
        $playlistTrack = new PlaylistTrack();                
        $playlistTrack->setTrack($track);                
        $playlistTrack->setPlaylist($playlist);                
        $playlistTrack->setPosition($genero->getPosicion());    


        $em->persist($playlistTrack);     

        try{
            $em->flush();
            $response['status'] = "OK";
        }catch( Exception $exc){
            $response['status'] = "Error";
            $response['message'] = $exc->getMessage();
        }
        
        return new Response( json_encode( $response ) );
        
    } 

    public function ajaxAddTrackToPlaylistAction( Request $request ){

        $playlistID  = $request->get('playlistID');

        $track  = $request->get('track');
        
        $em = $this->getDoctrine()->getManager();    
                
        $track = $em->getRepository('AppBundle:Track')->findOneBy(array('nombre_archivo'=> $track));

        $playlist = $em->getRepository('AppBundle:Playlist')->find($playlistID);
                                
        $playlist->addTrack($track);     

        $playlistTrack = new PlaylistTrack();

        $playlistTrack->setTrack($track);
        $playlistTrack->setPlaylist($playlist);
        $playlistTrack->setPosition($track->getGenero()->getPosition());

        $em->persist($playlistTrack);     

        try{
            $em->flush();
            $response['status'] = "OK";
        }catch( Exception $exc){
            $response['status'] = "Error";
            $response['message'] = $exc->getMessage();
        }
        
        return new Response( json_encode( $response ) );
        
    } 

    //ajaxThumbByArtista
    public function ajaxThumbByArtistaAction( Request $request ){

        $artista  = $request->get('artista');
                
        $client = new \Google_Client();
        $client->setApplicationName("test");
        $apiKey = "AIzaSyApjjZ28U1-a8PRjGxEuNAJbQJXU68FXgc"; // Change this line.
        // Warn if the API key isn't changed.
        if (strpos($apiKey, "<") !== false) {
          echo missingApiKeyWarning();
          exit;
        }
        $client->setDeveloperKey($apiKey); 
        
        $artistaThumbnails = $this->getThumbnailsByArtista( $client, $artista );  
        
                
        return $this->render('AppBundle:Artista:_artistaThumbnails.html.twig', array('thumbs'=> $artistaThumbnails, 'artista'=> $artista ));
        
    } 

    private function getThumbnailsByArtista( $client, $artista ){

        $thumbs = array();

        $youtube = new \Google_Service_YouTube($client);

        $artistaResponse = $youtube->search->listSearch('id,snippet', array(
              'q' => strtolower($artista),
              'maxResults' => 10,
        ));    

    
        foreach ( $artistaResponse->getItems() as $thumb) {            
            $thumbnailsOnline = $thumb->getSnippet()->getThumbnails();
            
            $thumbnail['thumb'] = $thumbnailsOnline->getDefault()['url'];

            $thumbs[] = $thumbnail;

        }    

        return $thumbs;                
        
    }

    public function ajaxSaveThumbByArtistaAction( Request $request ){

        $thumbnail  = $request->get('thumbnail');
        
        $artista  = $request->get('artista');
        
        $pathTemp = $this->get('kernel')->getRootDir() . '/../web/thumbnails/artistas/'; 
        
        $thumbFile = strtolower( $artista ).".jpg"; 

        $pathThumb = $pathTemp . $thumbFile;

        $response = array();

        if( unlink( $pathThumb )){
            if( copy( $thumbnail, $pathThumb ) ){
                $response['status'] = "OK";
            }else{
                $response['status'] = "Error";
            }
        }
        
        return new Response(json_encode( $response ));        
        
        
    } 

    
    private function createRealPathNewFile( $nameFile, $newNameFile ){
        $inicioFormato = strrpos($nameFile, ".");
        
        $formato = substr($nameFile, $inicioFormato );        

        $rootPathTemp = substr($nameFile, 0, strrpos($nameFile, "/"));
        
        $newFile = $rootPathTemp."/".$newNameFile.$formato;

        return $newFile;
    }

    private function searchInLocal( $request ){
        $value = strtoupper($request->get('term'));        
        $class = $request->get('class');
        $property = $request->get('property');
        $searchMethod = $request->get('search_method');

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository($class)->$searchMethod($value);

        $json = array();

        if (!count($entities)) {
            $json[] = array(
                'label' => 'No se encontraron coincidencias',
                'value' => ''
            );
        } else {

            foreach ($entities as $entity) {    
                
                $file = $entity['path'].$entity['nombre_archivo'];

                $json[] = array(
                        'id' => $entity['id'],
                        'thumb'=> "/track/". $entity['titulo'] . ".jpg",
                        'title' => $entity[$property],
                        //'url' => $entity['path'].$entity['nombre_archivo'],
                        'url' => $entity['path'],
                        'nombre_archivo' => $entity['nombre_archivo'],                        
                        'artista' => $entity['artista']['id'],
                        'genero' => $entity['genero']['posicion']
                );
                
            }
        }

        return $json;
    }

    private function searchInYoutube( $request ){  

        $client = new \Google_Client();
        $client->setApplicationName("test");
        $apiKey = "AIzaSyApjjZ28U1-a8PRjGxEuNAJbQJXU68FXgc"; // Change this line.
        // Warn if the API key isn't changed.
        if (strpos($apiKey, "<") !== false) {
          echo missingApiKeyWarning();
          exit;
        }
        $client->setDeveloperKey($apiKey);    

        $youtube = new \Google_Service_YouTube($client);

        $responseYoutube = $youtube->search->listSearch('id,snippet', array(
              'q' => strtoupper($request->get('term')),              
              'maxResults' => 10,
              'type'=> 'video'
            ));    

        $responseResults = array();

        foreach ($responseYoutube as $response) {
                
                $thumb = $response->getSnippet()->getThumbnails()->getDefault()['url'];            
                $result['title'] = $response->getSnippet()->getTitle();
                $result['url'] = $response->getId()->getVideoId();                
                $result['thumb'] = $thumb;
                $responseResults[] = $result;
        }
                    
        
        return $responseResults;
                   

    } 

    public function getAjaxTrackDataAction(Request $request) {
        $em = $this->getDoctrine()->getManager();              
        
        $trac = $em->getRepository('AppBundle:Track')->find($request->get('id'));                    
        
        //$out = $trac->getTransformFile();

        $out = $trac->transformFile();
        

        $json[] = array(
                    'id' => $entity['id'],
                    'label' => $entity['artista']['nombreCorto']. " - ".$entity['artista']['nombre']." - ".$entity[$property],
                    'value' => $entity['path'],
                    'artista' => $entity['artista']['id']
                );                    

        $response = new Response();
        $response->setContent(json_encode($json));
        
        return $response;
    }
    
    public function copiarUsbAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        
        $repuesta = array('message');
                        
        $appManager = $this->get('app.manager');                            

        $tracks = $appManager->getTracksByPlaylist( $request->get('playlistID') );

        $info = $appManager->getInfoByPlaylist( $request->get('playlistID') );

        $dir = self::DIR_HOME.$info[0]['nombre'];                        
        
        if( !file_exists( $dir )){
            if( !mkdir($dir) ){
                die("no se pudo");
            }
        }
        

        foreach($tracks as $track){
            $source = $track['path'] . $track['nombre_archivo'];     

            $fileFull = $track['position'] . "-" . $track['nombre_archivo'];                          

            if( copy( $source, $dir."/".$fileFull ) ){
                $respuesta['message']="ok";
            }else{
                $respuesta['message']="error";
            }
        }
        
        $response = new Response();
        $response->setContent(json_encode($respuesta));
        
        return $response;
        
        
    }


    /*public function getAjaxTracksRelatedAction(Request $request) {        
                
        $em = $this->getDoctrine()->getManager();            
        
        $track = $em->getRepository('AppBundle:Track')->findBy(array('titulo'=> $request->get('title')));       
        
        //$track->setLastPlay(new \Datetime());

        //$em->persist( $track );

        //$em->flush();

        $tracksArtista = $track->getArtista()->getTracks();

        if (! is_null( $track->getFuente() ) ){
            $tracks = $track->getFuente()->getTracks();
        } elseif ( !is_null($tracksArtista) ) {
            $tracks = $tracksArtista;
        } else {
            $tracks = shuffle($em->getRepository('AppBundle:Track')
                ->find( array('genero'=> $track->getGenero())));           
        }
                                        

        return $this->render('AppBundle:Track:tracksRelated.html.twig', array(
            'tracks' => $tracks           
        ));
    }*/

    /* DEVUELVE LOS TRACKS RELACIONADOS */

    /* 
     * 1. Fuente: los otros tracks pertenecientes al fuente
     * 2. Artista: los demas tracks que posee el artista
     * 3. Genero: los tracks pertenecientes al genero del track actual
     */
    public function getAjaxTracksRelatedAction(Request $request) {        
                
        $em = $this->getDoctrine()->getManager();                    
        
        $appManager = $this->get('app.manager');

        $trackID = ( int ) $request->get('track');
        
        $track = $em->getRepository('AppBundle:Track')->find($trackID);   

        $track->setLastPlay(new \Datetime());

        $em->persist( $track );

        $em->flush();

        $trackRelated = array();
                
        if( !is_null( $track->getArtista() ) ){
            $tracksRelated['info'] = $track->getArtista()->getNombre();
            $tracksRelated['tracks'] = $track->getArtista()->getTracks();
        }elseif (!is_null($track->getGenero())) {
            $tracksRelated['info'] = $track->getGenero()->getNombre();
            $tracks = $em->getRepository('AppBundle:Track')->findBy(array('genero'=> $track->getGenero()->getId()));  
            $tracksRelated['tracks'] = $tracks;
        }
                                                    

        return $this->render('AppBundle:Track:tracksRelated.html.twig', array(
            'tracksRelated' => $tracksRelated           
        ));
    }

    public function ajaxPlaylistGeneratem3uAction(Request $request){
        $em = $this->getDoctrine()->getManager();
                
        $appManager = $this->get('app.manager');


        if($request->isXmlHttpRequest())
        {    
            
            $entity = $em->getRepository('AppBundle:Playlist')->findOneBy(array('id' => (int)$request->get('playlistID')));                    

            $entity->generatePlaylist();            

            $jsonResponse = array('status'=> 'OK', 
                                  'message'=> 'La playlist se ha guardado correctamente a un achivo M3U',
                                  'data'=> "/home/gabriel/Vídeos/playlists/".$entity->getNombre()
                                  );
        
        }

        //$response = new Response();
    
        /*$response->headers->set('Content-Type', 'audio/mpegurl');
        /*$response->headers->set('Content-Disposition', 'audio/mpegurl');    
        //$response->headers->set('Content-Disposition', 'attachment; filename="' . . '";');
    
        //return $response->send();   */
        return new JsonResponse($jsonResponse);    


    }

    //ajaxTrackSetFavorite
    public function ajaxTrackSetFavoriteAction(Request $request){
        $em = $this->getDoctrine()->getManager();
                
        $appManager = $this->get('app.manager');

        if($request->isXmlHttpRequest())
        {    
            
            $entity = $em->getRepository('AppBundle:Track')->findOneBy(array('id' => (int)$request->get('track')));                    

            $entity->setFavorito(true);

            $em->persist($entity);

            $em->flush();        

            $jsonResponse = array('status'=> 'OK', 
                                  'message'=> 'Se ha establecido el track como favorito correctamente',                                  
                                  );
        
        }

        return new JsonResponse($jsonResponse);    
    }

    private function findUsbConnected( $devicesMount,  $usb)
    {
        $file = fopen($devicesMount, "r");                          
            while(!feof($file)) 
            {
                $line = fgets($file);  
                $deviceInfo = explode(" ", $line);
                
                if( $deviceInfo['0'] == $usb){
                    return $deviceInfo['1'];
                }   
                                                                                            
            }
                
        fclose($file); 
        
    }
    
}
