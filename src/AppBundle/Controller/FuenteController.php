<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Fuente;
use AppBundle\Entity\Track;
use AppBundle\Form\FuenteType;
use AppBundle\Form\TrackType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Fuente controller.
 *
 */
class FuenteController extends Controller
{

    /**
     * Lists all Fuente entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Fuente')->findAll();

        return $this->render('AppBundle:Fuente:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Fuente entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Fuente();
                
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
                               
        if ($form->isValid()) {
		
        foreach($entity->getTracks() as $tema){
            $tema->setFuente($entity);                        
        }
                                                    
            $em = $this->getDoctrine()->getManager();
            $uploaded_file = $form['file']->getData();
             if ($uploaded_file) {
                //Sube la imagen
                $picture = FuenteType::processImage($uploaded_file, $entity);

                //setea el tamaño del recorte
                $targ_w = $request->request->get('w');
                $targ_h = $request->request->get('h');

                if ($targ_h && $targ_w) {
                    if (null !== $entity->getPortada()) {
                        $file = $entity->getPortada();
                        @unlink($file);
                    } else {
                        $file = $picture;
                    }
                    $file = $this->get('kernel')->getRootDir() . '/../web/portada/' . $picture;

                    $img_r = imagecreatefromjpeg($file);
                    $dst_r = ImageCreateTrueColor($targ_w, $targ_h);

                    imagecopyresampled($dst_r, $img_r, 0, 0, $request->request->get('x'), $request->request->get('y'), $targ_w, $targ_h, $request->request->get('w'), $request->request->get('h'));
                    imagejpeg($dst_r, $file, 90);
                }


                $entity->setPortada($picture);
            }
	
            $entity->setNombre(strtoupper($entity->getNombre()));    
            $entity->setLugarGrabacion(strtoupper($entity->getLugarGrabacion()));    
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('fuente'));
        }

        return $this->render('AppBundle:Fuente:fuente.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Fuente entity.
     *
     * @param Fuente $entity The entity
     *
     * @return Form The form
     */
    private function createCreateForm(Fuente $entity)
    {
        $form = $this->createForm(new FuenteType(), $entity);     

        return $form;
    }

    /**
     * Displays a form to create a new Fuente entity.
     *
     */
    public function newAction()
    {
        $entity = new Fuente();               
        
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:Fuente:fuente.html.twig', array(
            'entity' => $entity,            
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Fuente entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Fuente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Fuente entity.');
        }
        

        return $this->render('AppBundle:Fuente:show.html.twig', array(
            'entity'      => $entity,

        ));
    }

    /**
     * Displays a form to edit an existing Fuente entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Fuente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Fuente entity.');
        }

        $editForm = $this->createEditForm($entity);
        

        return $this->render('AppBundle:Fuente:fuente.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'modo_edicion' => true,
        ));
    }

    /**
    * Creates a form to edit a Fuente entity.
    *
    * @param Fuente $entity The entity
    *
    * @return Form The form
    */
    private function createEditForm(Fuente $entity)
    {
        
        $form = $this->createForm(new FuenteType(), $entity);
        
        return $form;
    }
    /**
     * Edits an existing Fuente entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Fuente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Fuente entity.');
        }

        
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $uploaded_file = $editForm['file']->getData();
             if ($uploaded_file) {
                //Sube la imagen
                $picture = TrackType::processImage($uploaded_file, $entity);

                //setea el tamaño del recorte
                $targ_w = $request->request->get('w');
                $targ_h = $request->request->get('h');

                if ($targ_h && $targ_w) {
                    if (null !== $entity->getPortada()) {
                        $file = $entity->getPortada();
                        @unlink($file);
                    } else {
                        $file = $picture;
                    }
                    $file = $this->get('kernel')->getRootDir() . '/../web/portada/' . $picture;

                    $img_r = imagecreatefromjpeg($file);
                    $dst_r = ImageCreateTrueColor($targ_w, $targ_h);

                    imagecopyresampled($dst_r, $img_r, 0, 0, $request->request->get('x'), $request->request->get('y'), $targ_w, $targ_h, $request->request->get('w'), $request->request->get('h'));
                    imagejpeg($dst_r, $file, 90);
                }


                $entity->setPortada($picture);
            }            
            $em->flush();

            return $this->redirect($this->generateUrl('fuente_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:Fuente:fuente.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'modo_edicion' => true,
        ));
    }
    

}
