<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Track;
use AppBundle\Form\TrackType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\filters\TrackSearchFilterType;
use AppBundle\Form\EditMasivTrackType;
use Exception;

/**
 * Track controller.
 *
 */
class TrackController extends Controller
{

    /**
     * Lists all Track entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $appManager = $this->get('app.manager');

        $aTracks = $appManager->listarTracks(array());

        $trackSearchFilter = new TrackSearchFilterType();

        $editMasiv = new EditMasivTrackType();

        $editMasivForm = $this->createForm($editMasiv);    

        $formFiltro = $this->createForm($trackSearchFilter);  

        $paginador = $this->get('knp_paginator');

        $aTracks = $paginador->paginate(
                $aTracks,
                $this->get('request')->query->get('page', 1),
                10
        );

        $formFiltro->handleRequest($request);
                
        if ($formFiltro->isValid()) {            
            $filtros = $formFiltro->getData();                     
            $aTracks = $appManager->listarTracks($filtros);           

            if ( $filtros['deleted'] )
            {   
                $fileDeleted = array();

                foreach ($aTracks as $track) {
                        if(!$track->isFile() ){                    
                            $fileDeleted[] = $track;
                        }
                }                        

                if( count( $fileDeleted ) > 0 )
                {
                    $aTracks = $fileDeleted;
                } else {
                    $aTracks = array();
                }

            }



            $aTracks = $paginador->paginate(
                $aTracks,
                $this->get('request')->query->get('page', 1),
                20
            );
        }   
        

        return $this->render('AppBundle:Track:index.html.twig', array(
            'entities' => $aTracks,
            'formFilter'=>$formFiltro->createView(),
            'formEditMasiv'=>$editMasivForm->createView(),
        ));
    }



    public function editMasivAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();        

        $dataApply = json_decode($request->get('filtro'));    

        $data = json_decode($request->get('data'));

        $appManager = $this->get('app.manager');                
                    
        for ($i = 0; $i < count ( $dataApply );  $i++) {                        
             
            foreach ($data as $valor) {                                     
                if ( $dataApply[$i][0] == "genero"){
                    if ( !empty ( $dataApply[$i][1] ) ){
                        $entity = $em->getRepository('AppBundle:Track')->find($valor);
                        $entity->setGenero( $dataApply[$i][1] );                                      
                        $em->persist($entity);
                    }
                }

                if ( $dataApply[$i][0] == "artista"){
                    if ( !empty ( $dataApply[$i][1] ) ){                        
                        $artista = $em->getRepository('AppBundle:Artista')->find($dataApply[$i][1]);                        
                        $entity = $em->getRepository('AppBundle:Track')->find($valor);
                        $entity->setArtista($artista);                                                              
                        $em->persist($entity);
                    }
                }

                if ( $dataApply[$i][0] == "fuente"){
                    if ( !empty ( $dataApply[$i][1] ) ){                        
                        $fuente = $em->getRepository('AppBundle:Fuente')->find($dataApply[$i][1]);                        
                        $entity = $em->getRepository('AppBundle:Track')->find($valor);
                        $entity->setFuente($fuente);                                                              
                        $em->persist($entity);
                    }
                }

                if ( $dataApply[$i][0] == "playlist"){
                    if ( !empty ( $dataApply[$i][1] ) ){
                        $playlist = $em->getRepository('AppBundle:Playlist')->find($dataApply[$i][1]);
                        $track = $em->getRepository('AppBundle:Track')->find($valor);
                        $playlist->addTrack($track);
                        $em->persist($playlist);

                    }
                }

                if ( $dataApply[$i][0] == "folder"){
                    if ( !empty ( $dataApply[$i][1] ) ){
                        $entity = $em->getRepository('AppBundle:Track')->find($valor);
                        $fileNew = $dataApply[$i][1]."/".$entity->getNombreArchivo();                  
                        copy( $entity->getPath(),  $fileNew);                        
                    }
                    
                }

                if ( $dataApply[$i][0] == "borrar"){
                    if ( $dataApply[$i][1] ){                        
                        $track = $em->getRepository('AppBundle:Track')->find($valor);                        
                        $em->remove($track);
                    }
                    
                }
                
            }
        }
        
            
        try{
            
            $em->flush();    
            $response = json_encode(array('status'=> 'OK', 'message'=> 'La edicion masiva se ha realizado correctamente'));

        }catch( Exception $exc){
            $response = json_encode(array('status'=> 'Error', 'message'=> $exc->getMessage() ));
        }
        

        return new Response( $response );
    }






    /**
     * Creates a new Track entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Track();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();                                                
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                    'ok', 'Se ha guardado correctamente el track .' . $entity->getTitulo() 
            );
            return $this->redirect($this->generateUrl('track'));
        }

        return $this->render('AppBundle:Track:track.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'modo_edicion'=> false
        ));
    }

    /**
     * Creates a form to create a Track entity.
     *
     * @param Track $entity The entity
     *
     * @return Form The form
     */
    private function createCreateForm(Track $entity)
    {
        $form = $this->createForm(new TrackType(), $entity);       
        return $form;
    }

    /**
     * Displays a form to create a new Track entity.
     *
     */
    public function newAction()
    {
        $entity = new Track();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:Track:track.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'modo_edicion'=> false
        ));
    }

    /**
     * Finds and displays a Track entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Track')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Track entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Track:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Track entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();        
        
        $entity = $em->getRepository('AppBundle:Track')->find($id);
        
        $path = $entity->getPath();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Track entity.');
        }


        $editForm = $this->createEditForm($entity);
        

        return $this->render('AppBundle:Track:track.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),            
            'modo_edicion' => true,
        ));
    }

    /**
    * Creates a form to edit a Track entity.
    *
    * @param Track $entity The entity
    *
    * @return Form The form
    */
    private function createEditForm(Track $entity)
    {
        $form = $this->createForm(new TrackType(), $entity);       

        return $form;
    }
    /**
     * Edits an existing Track entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Track')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Track entity.');
        }

        //$archivo = false;
        
        /*if(file_exists($entity->getPath())){
            $archivo = true;
        }*/
        
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {            
            $em->persist($entity);                    
            $em->flush();
            
            $this->get('session')->getFlashBag()->add(
                    'ok', 'Se ha actualizado correctamente el track .' . $entity->getTitulo() 
            );
            return $this->redirect($this->generateUrl('track'));
        }

        return $this->render('AppBundle:Track:track.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'modo_edicion' => true,
            
        ));
    }
    
    
    public function renameFileAction($id)
    {
                       
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Track')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Track entity.');
            }
            
            try{
                
                $file = substr ( $entity->getPath() , 7 );                
                $folderPos = strripos($file, '/');
                $folder = substr($file, 0, $folderPos);
                $pathAbsolute = $folder."/".$entity->getNombreArchivo();
                $fileName = $entity->getTitulo().".".$entity->getFormato();
                $fileNew = $folder."/".$entity->getTitulo().".".$entity->getFormato();
                
                rename($pathAbsolute, $fileNew);
                
                $entity->setNombreArchivo($fileName);
                
                $em->persist($entity);
                $em->flush();
                
                $this->get('session')->getFlashBag()->add(
                    'ok', 'Se ha borrado '.$entity->getNombreArchivo().'  asociado al track ' . $entity->getTitulo() 
            );
                //return $this->redirect($this->generateUrl('ambiente_edit', array('id' => $id)));
                return $this->redirect($this->generateUrl('track_edit', array('id'=> $entity->getId())));
            }catch (Exception $exc){
                echo $exc->getMessage();
            }

                    
            
    }

    
}
