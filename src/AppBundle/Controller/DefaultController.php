<?php

namespace AppBundle\Controller;

use AppBundle\Form\SettingsType;


use AppBundle\Form\filters\TrackFilterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Playlist;
use AppBundle\Entity\PlaylistTrack;
use AppBundle\Form\PlaylistType;
use AppBundle\Entity\Settings;
use AppBundle\Form\filters\TrackSearchAdvancedFilterType;
use AppBundle\Form\FindTrackType;
use Symfony\Component\Finder\Finder;
use PHPVideoToolkit\Timecode;
use PHPVideoToolkit\AudioFormat;
use PHPVideoToolkit\Video;
use PHPVideoToolkit\MediaParser;
use PHPVideoToolkit\VideoFormat_H264;
use PHPVideoToolkit\VideoFormat_Webm;
use PHPVideoToolkit\VideoFormat_Mp4;
use AppBundle\Entity\Track;
use Exception;
use DateTime;


use Symfony\Component\HttpFoundation\JsonResponse;

//require_once realpath(dirname(__FILE__) . '../../src/Google/autoload.php');


class DefaultController extends Controller
{
    

    public function indexAction()
    {
        
        $appManager = $this->get('app.manager');    

        $formSearch = $this->createForm(new TrackFilterType());

        $trackSearchFilter = new TrackSearchFilterType();

        $formSearchAdvanced = $this->createForm($trackSearchFilter);                     
                    
        $em = $this->getDoctrine()->getManager();                
        $entities = $em->getRepository('AppBundle:Playlist')->findAll();
        
        if($entities){
            $lasPlay = $appManager->getLastList();
            return $this->render('default/index.html.twig', array(
            'playlists' => $entities,
            'formSearch'   => $formSearch->createView(),
            'formSearchAdvanced'   => $formSearchAdvanced->createView(),
            'lastPlaylist'=>$lasPlay
        ));
        }else{
            return $this->render('default/index.html.twig');
        }
                
                                
    }
    
       
    
    
    public function getAjaxTracksByTracklistAction(Request $request) {
        
        $appManager = $this->get('app.manager');                            

        $tracks = $appManager->getTracksByPlaylist( $request->get('padre') );
        
        $info = $appManager->getInfoByPlaylist( $request->get('padre') );
        
           
        $seconds = 0;    

        foreach ($tracks as $track) {            
            $seconds += $track['duracion'];            
        }    

        $totalDuration['duration'] = gmdate("H:i:s", $seconds);

        $playlistInfo = array_merge($info[0], $totalDuration);
        
        $playlist['info'] = $playlistInfo;        

        $playlist['tracks'] = $tracks;
        
        return $this->render('AppBundle:Playlist:listas.html.twig', array(
            'playlist' =>  $playlist,
        ));
       
    }

    public function getAjaxTracksByArtistaAction(Request $request) {
        
        $appManager = $this->get('app.manager');            

        $em = $this->getDoctrine()->getManager();   

        $tracks = $appManager->getTracksByArtista( $request->get('artista') );

        $artista['nombre'] = $request->get('artista');

        $artista['tracks'] = $tracks;        

        return $this->render('AppBundle:Artista:_tracks.html.twig', array(
            'artista' =>  $artista,
        ));
       
    }
    
    
    
    public function configuracionAction() {
        $form = $this->createForm(new SettingsType());

                                                                                                                                    
        return $this->render('default/configuracion.html.twig', array(
                    'form' => $form->createView(),            
        ));
    }
    
    public function stepOneAction(Request $request) {
        //$entity = new Settings();        
        //$pathRoot = "/home";

       // $entity->setPathHome($pathRoot);
        //$form = $this->createForm(new SettingsType(), $entity);
        
        $appManager = $this->get('app.manager');
        
        if ( $request->isMethod('POST') ){
            $form->handleRequest($request);                            
                $em = $this->getDoctrine()->getManager();                                                
                $em->persist($entity);
                $em->flush();
                
                $this->get('session')->getFlashBag()->add(
                        'ok', 'Se ha guardado correctamente ' 
                );
                return $this->redirect($this->generateUrl('step_two'));
                            
                //$files = $appManager->getListFiles($filtros, true);                                      
        }
        
                                                                    
        return $this->render('default/stepOne.html.twig', array(
                    'form' => $form->createView(),    
                    'pathRoot'=> $pathRoot                                        
        ));
    }

    public function stepTwoAction(Request $request) {
        $entity = new Settings();        
        
        $entity->setPathHome($pathRoot);

        $form = $this->createForm(new SettingsType(), $entity);
        
        $appManager = $this->get('app.manager');
        
        if ( $request->isMethod('POST') ){
            $form->handleRequest($request);                            
                $em = $this->getDoctrine()->getManager();                                                
                $em->persist($entity);
                $em->flush();
                
                $this->get('session')->getFlashBag()->add(
                        'ok', 'Se ha guardado correctamente ' 
                );
                return $this->redirect($this->generateUrl('step_two'));
                            
                //$files = $appManager->getListFiles($filtros, true);                                      
        }
        
                                                                    
        return $this->render('default/stepOne.html.twig', array(
                    'form' => $form->createView(),    
                    'pathRoot'=> $pathRoot                                        
        ));
    }


    public function setPathUsuarioAction(Request $request) {
        $form = $this->createForm(new PathMultimediaType());
                                                
        $appManager = $this->get('app.manager');
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $filtros = $form->getData();  
                        
            $files = $appManager->getListFiles($filtros, true);  

                        
        }
                                                                    
        return $this->render('default/stepTwo.html.twig', array(
                    'form' => $form->createView(),                        
                    'files'=> $files,
                    'path'=> $filtros['path']
        ));
    }


    
    public function getFilesNewsAction(Request $request) {            
                                            
        $appManager = $this->get('app.manager');

        //$files = $appManager->getListFiles($path, false );  

        /*if( $this->getUser()->getSettings() ){
            $pathMediaUser = $this->getUser()->getSettings()->getPathMedia();
        }*/   

        //$params = array('path_media'=> $pathMediaUser, 'container'=> $this->container);

        $ruta = "/home/gabriel/Descargas";    
        //$settings = $this->createForm(new SettingsType( $params )); 
        $files = $appManager->getListFiles($ruta, false );

        //$files = $this->extractImage($files, true); 

        $directory = "/media/gabriel/DATOS/VIDEOS";
              
        //$findTrackFilesForm = new FindTrackType( $test );

        //$formFindFiles = $this->createForm( $findTrackFilesForm);

                                                                                      
        return $this->render('default/news.html.twig', array(
                    //'form2' => $formFindFiles->createView(),
                    //'settings' => $settings->createView(),
                    'files'=> $files,
                    'directory'=> $directory
                    
        ));
    }
    
    public function importarFilesAction(Request $request) {
                
        $appManager = $this->get('app.manager');
        
                
        $tracksImported = $appManager->importFiles( json_decode($request->get('sources')), $request->get('target') );  

                 
        return new JsonResponse($tracksImported);
        /*return $this->render('default/message.html.twig', array(                    
                    'tracksImported'=>$tracksImported
        ));*/
    }

    public function createDirectoryAction(Request $request) {                        
        
        $directoryParent = $request->get('dir_parent');   

        $dirRoot = substr($directoryParent, 0, strrpos( $directoryParent, '/'));

        $appManager = $this->get('app.manager'); 
        
        $name = $request->get('name');

        $response = [];

        $directoryChild = $directoryParent.strtoupper($name);
        //$directoryChild = $dirRoot."/".strtoupper($name);            

        if( mkdir( $directoryChild ) ){
            $folder = $appManager->getFilesPlane( $dirRoot, false);
            $response['folder'] = $folder;
            $response['status'] = "OK";
            $response['message'] = "Se ha creado correctamente";
        } else {
            $response['status'] = "Error";
            $response['message'] = "No se pudo crear";
        }
                 
        return new JsonResponse( $response );
        
    }

    private function getThumbnails( $artistas, $client ){        
        
        $artistasThumbnails = array();

        foreach ($artistas as $artista) {
            $pathTemp = $this->get('kernel')->getRootDir() . '/../web/thumbnails/artistas/'; 
            $thumbFile = strtolower($artista->getNombre()).".jpg";
            $pathThumb = $pathTemp . $thumbFile;

            if( !file_exists( $pathThumb )){                
                $youtube = new \Google_Service_YouTube($client);
                $thumb = $this->downloadThumbnail($youtube, $artista);
                $artistaThumbnail['thumbnail'] = $thumb;
            }else{
                $artistaThumbnail['thumbnail'] = $thumbFile;
            }

            $artistaThumbnail['id'] = $artista->getId();
            $artistaThumbnail['artista'] = $artista->getNombre();
            $artistasThumbnails[] = $artistaThumbnail;
                    
        }        
        return $artistasThumbnails;                   
    } 



    private function downloadThumbnail($youtube, $artista ){
        $artistaResponse = $youtube->search->listSearch('id,snippet', array(
              'q' => strtolower($artista->getNombre()),
              'maxResults' => 1,
        ));    
    
        $thumb = $artistaResponse->getSnippet()->getThumbnails()->getDefault()['url'];
                
        if( !file_exists( $thumbFile )){
            copy( $thumb, $thumbFile );    
        }                
                                        
        return $thumbFile;
    }
    
    
    public function homeAction( Request $request)
    {

        $entity = new Playlist();

        $generos = array();
        
        $formPlayList   = $this->createCreateForm($entity);
         
        $formSearch = $this->createForm(new TrackFilterType());

        $trackSearchFilter = new TrackSearchAdvancedFilterType();

        $formSearchAdvanced = $this->createForm($trackSearchFilter);
        
        $appManager = $this->get('app.manager');                        

        $root = "/home/gabriel";

        $dirRoot = $appManager->getInfoDir( $root );
                    
        $em = $this->getDoctrine()->getManager();   

        $fuentes = $em->getRepository('AppBundle:Fuente')->findAll();  

        $artistas = $em->getRepository('AppBundle:Artista')->findAll(); 


        $client = new \Google_Client();
        $client->setApplicationName("test");
        $apiKey = "AIzaSyApjjZ28U1-a8PRjGxEuNAJbQJXU68FXgc"; // Change this line.
        // Warn if the API key isn't changed.
        if (strpos($apiKey, "<") !== false) {
          echo missingApiKeyWarning();
          exit;
        }
        $client->setDeveloperKey($apiKey); 
        
        $artistasThumbnails = $this->getThumbnails( $artistas, $client );    

        $currentlyTracks = $appManager->getTracksCurrently();   
        
        $tracks = $appManager->getTracksMost();                                      

        $topTracks = $appManager->getTopTracks(); 

        $generos = $appManager->getGeneros();         
        
        if ( $request-> getMethod() == 'POST'){
            $formSearchAdvanced->bind($request);
            $filtros = $formSearchAdvanced->getData();
            
            $datos = $appManager->filtrarDatos($filtros); 
            
            
        }  

            return $this->render('AppBundle:Home:index.html.twig',  array(            
                'fuentes' => $fuentes,            
                'tracks' => $tracks,
                'formPlaylist'=> $formPlayList->createView(),
                'formSearch'   => $formSearch->createView(),                
                'topTracks' => $topTracks,                
                'generos' => $generos,                              
                'artistasThumbnails'=> $artistasThumbnails,
                'dirRoot'=> $dirRoot               
        ));                                                
    }


    /**
     * Creates a form to create a Playlist entity.
     *
     * @param Playlist $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Playlist $entity)
    {
        $form = $this->createForm(new PlaylistType(), $entity);
        

        return $form;
    }



    public function ajaxSearchAdvancedAction( Request $request){
        
        $dataSearch = json_decode($request->get('search'), true);

        $paginador = $this->get('knp_paginator');    

        $appManager = $this->get('app.manager');                        
                    
        $datos = $appManager->filtrarDatos($dataSearch); 

        
        return $this->render('AppBundle:Track:busqueda.html.twig', array(
            'datos' => $datos,
        ));
                        

    }

    public function getDirFilesDestinationAction(){
        $dir = "/media/DATOS/VIDEOS";

        $appManager = $this->get('app.manager');

        $folders = $appManager->getInfoDir( $dir );    

        return $this->render('AppBundle:Track:_folders.html.twig', array(
            'folders' => $folders,
        ));

    }
    


    /*public function ajaxSearchYoutubeAction( Request $request ){
        $client = new Google_Client();

        $happy = $this->container->getParameter('happy_r_google_api');


        $client->setClientId($happy['oauth2_client_id']);
        $client->setClientSecret($happy['oauth2_client_secret']);
        $client->setScopes('https://www.googleapis.com/auth/youtube');

        $happy = $this->container->getParameter('happy_r_google_api');
        
        $client->setDeveloperKey($happy['developer_key']);

        $youtube = new Google_Service_YouTube($client);

        //var_dump($client);
        //var_dump($youtube);

        $channelsResponse = $youtube->channels->listChannels('contentDetails', array(
            'mine' => 'true',
        ));
        var_dump($channelsResponse);

    }*/


    public function savePlaylistAction( Request $request){
        
        $dataSearch = json_decode($request->get('data'), true);

        $appManager = $this->get('app.manager');     

        $em = $this->getDoctrine()->getManager();

        $playlistRepo = $em->getRepository('AppBundle:Track');

        foreach ($dataSearch as $key => $value) {

            if( $value['name'] == "track"){
                $track = $playlistRepo->find($value['value']);
                $playlist = new PlaylistTrack();                
                $playlist->addTrack($track);                
            }elseif( $value['name'] == "listnew") {
                $playlist->setNombre($value['value']);
                $playlist->setDescripcion("Agreagado frontend");
            }

            
        }   

        $em->persist($playlist);     

        $em->flush();

        $json = json_encode(array('status'=> 'OK'));

        return new Response($json);
                        

    }


    public function ajaxGetPlaylistsAction( Request $request){
        
        $appManager = $this->get('app.manager');                        
                  
        $em = $this->getDoctrine()->getManager();

        $playlists = $em->getRepository('AppBundle:Playlist')->findBy( array('usuario'=> $this->getUser()->getId()) );

        //$playlists = $em->getRepository('AppBundle:Playlist')->findByUsuario($this->getUser()->getId());
                
        return $this->render('AppBundle:Home:_playlists.html.twig', array(
            'playlists' => $playlists,
            
        ));            
    }

    
    public function getAjaxTracksByFuenteAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('AppBundle:Fuente')->findOneBy(array('id' => $request->get('fuente')));
                    
        $tracks = $entity->getTracks();

        $template = "tracks.html.twig";
        
     
        return $this->render('AppBundle:Fuente:'.$template, array(
            'tracklist' => $entity,
        ));
       
    }   

    /*public function getAjaxFilesByDirAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
                
        $dirRoot  = $request->get('dir');        
        
        $appManager = $this->get('app.manager');  

        $files = $appManager->getFilesByDir( $dirRoot );
        
     
        return $this->render('AppBundle:Track:_files10.html.twig', array(
            'files' => $files,
            'dir'=>$dirRoot
        ));
       
    }*/   

    public function ajaxRootMediaAction( Request $request ){
        
        $pathRootMedia = $request->get('dir');     

        $name = $request->get('name');   
        
        $appManager = $this->get('app.manager');

        //$files = $appManager->getListFiles($pathRootMedia, false, true ); 
        $files = $appManager->getFilesPlane($pathRootMedia, true, $name);

        //$json = json_encode(array('status'=> 'OK', 'data'=> $files));
        
        return new Response(json_encode($files));
        //return new Response($files);

    }

    public function importLibraryAction( Request $request ){

        $dirRoot = "/home/gabriel";

        $appManager = $this->get('app.manager');

        $directories = $appManager->getInfoDir( $dirRoot );
        
        return $this->render('AppBundle:Track:import_library.html.twig', array('dirs'=>$directories));

    }

    public function ajaxRenameFileAction( Request $request ){

        $fileOld  = $request->get('fileOld');
        $fileNew  = $request->get('fileNew');

        if( file_exists($fileOld)){
            $newFile = $this->createRealPathNewFile( $fileOld, $fileNew );

            if ( rename($fileOld, $newFile ) ){
                $json = json_encode(array('status'=> 'OK', 'data'=> $newFile));
                //$json = array('status'=> 'OK', 'data'=> $newFile);
            }                    
            
        } else {
            $json = array('status'=> 'Error', 'data'=> 'No existe el archivo');
        }
        return new Response($json);
        //$response = new JsonResponse( $json );                    
    }

    
    public function ajaxGetInfoRootAction( Request $request ){

        $em = $this->getDoctrine()->getManager();

        $dirRoot  = $request->get('dir'); 

        $place  = $request->get('place');        

        $template = "dir1";

        $appManager = $this->get('app.manager');  

        $sources = array();

        $nameFolder = substr($dirRoot, strrpos($dirRoot, "/") + 1 );      
        
        $genero = $em->getRepository('AppBundle:Genero')->findOneBy(array('nombre' => $nameFolder ) );

        if( $genero ){
            $sources['genero'] = $genero;
        }  


        $fuente = $em->getRepository('AppBundle:Fuente')->findOneBy(array('nombre' => $nameFolder ) );  

        if( $fuente ){
            $sources['fuente'] = $fuente;
        } 

        $artista = $em->getRepository('AppBundle:Artista')->findOneBy(array('nombre' => $nameFolder ) );  

        if( $artista ){
            $sources['artista'] = $artista;
        }   
        


        $subDirs = $appManager->getInfoDir( $dirRoot );        
                        
        return $this->render('AppBundle:Track:'.$template.'.html.twig', array('sources'=> $sources,'place'=> $place,  'subDirs'=> $subDirs, 'dir'=> strtolower($nameFolder) ));
        
    } 



    public function ajaxAnalizeFilesAction( Request $request ){
        
        $files  = json_decode($request->get('files'), true);
            
        $em = $this->getDoctrine()->getManager();

        if( is_array( $files ) ){
            if( count( $files ) > 0 ){
                foreach ($files as $file) {                    
                    $pathWeb = $this->get('kernel')->getRootDir() . '/../web/track/tmp/';     

                    $name = substr($file['filename'], strrpos($file['filename'], "/") + 1 );
                    $fileTemp = $pathWeb . $name;                                                    

                    $video  = new Video($file['filename']);                
                
                    $process = $video->extractFrame(new Timecode(40))
                            ->save($fileTemp.".jpg");

                    $image['image_thumb'] = $name;   
                    $image['image'] = $name;  
                    $image['path'] = substr($file['filename'], 0, strrpos($file['filename'], "/") + 1);  
                    

                    $image['size'] = $this->getSize($file);                                        

                    
                    $info = Track::getRawInfo($file['filename']);


                    $images[] = array_merge($image, $info);
                                                                            
                }
            
            }
        }
        
        
        return $this->render('AppBundle:Track:files_analized.html.twig', array('thumbs'=> $images));
                
    }

    public function getTimeFilesAction( Request $request ){            
            
        $em = $this->getDoctrine()->getManager();

        $appManager = $this->get('app.manager'); 

        $playlist = $request->get('playlist');

        $tracks = $appManager->getTracksByPlaylist( $playlist );

        $trac = array();
        
        foreach ($tracks as $track) {                        
            $file = $track['path'] . $track['nombre_archivo'];
            $time = Track::getRawTime($file);
            
            $trackObject = $em->getRepository('AppBundle:Track')->findOneBy(array('nombre_archivo'=>$track['nombre_archivo']));
            
            $trackObject->setDuracion($time);

            $em->persist($trackObject);
            
        }

        $em->flush();
                    

        return new Response(json_encode(array('message'=> 'Se ha actualziad')));
                
    } 


    public function ajaxImportFilesAnalizedAction( Request $request ){

        /*
        array(1) {
          [0]=>
          array(5) {
            ["filename"]=>
            string(28) "Madonna - La Isla Bonita.vob"
            ["path"]=>
            string(62) "/home/gabriel/Soulseek Downloads/complete/70-80-90-00 y otros/"
            ["size"]=>
            string(9) "235.39 MB"
            ["resolution"]=>
            string(7) "720x576"
            ["duration"]=>
            string(5) "03:54"
          }
        }
                                                                
        */
        $em = $this->getDoctrine()->getManager();

        $files  = json_decode( utf8_encode($request->get('files')), true);
                        
        $data  = json_decode( $request->get('extra'), true );

        $filesImport = array();

        
        if( is_array( $files ) ){
            if( count( $files ) > 0 ){
                foreach ($files as $file) {                    
                    $pathTemp = $this->get('kernel')->getRootDir() . '/../web/track/tmp/';     
                    $fileTemp = $pathTemp . utf8_decode($file['filename']) . ".jpg";
                    $pathTrack = $this->get('kernel')->getRootDir() . '/../web/track/';
                    $fileImg = $pathTrack . $file['filename'] . ".jpg";

                    $pathSource = $file['path']. $file['filename'];
                    
                    $pathTarget = $data['dirTarget']. "/" . $file['filename'];
                    
                    copy( $pathSource, $pathTarget);                                
                    
                    if( copy( $fileTemp, $fileImg) ){                        
                        unlink( $fileTemp );
                    }

                    $pista = new Track();

                    if( isset( $data['genero']) ){                        
                        $genero = $em->getRepository('AppBundle:Genero')->find((int)$data['genero']);
                    
                        if( isset($genero) ){
                            $pista->setGenero($genero);
                        }  
                    }                    

                    if( isset( $data['fuente']) ){
                        $fuente = $em->getRepository('AppBundle:Fuente')->find((int)$data['fuente']);

                        if( isset( $fuente )  ){
                            $pista->setFuente($fuente);
                        }  
                    }

                    if( isset( $data['artista'])  ){

                        $artista = $em->getRepository('AppBundle:Artista')->find((int)$data['artista']);                        
                        
                        if( isset( $artista ) ){
                            $pista->setArtista($artista);                            
                        }  
                    }

                    $pista->setPath( $file['path'] );
                    $pista->setTitulo( $file['filename'] );                    
                    $pista->setNombreArchivo( $file['filename'] );      
                    $pista->setSize( $file['size'] );                                                     
                    $pista->setDuracion( $file['duration'] );  

                    $em->persist($pista);

                    $filesImport[] = $pista;            
                }
            
            }

            try {
                $em->flush();                    
                $response['status'] = "ok";
                $response['message'] = "se guardo";
                $response['files'] = $filesImport;

            } catch (Exception $exc) {
                $response['status'] = "error";
                $response['message'] = $exc->getMessage();
                $exc->getMessage();
            }
        } 

        return new Response( json_encode( $response ) );   

    }

    
    public function getAjaxFilesByDirAction( Request $request ){

        $dirRoot  = $request->get('dir');

        $em = $this->getDoctrine()->getManager();
        
        $appManager = $this->get('app.manager');  

        $files = $appManager->getFilesByDir( $dirRoot );

        return $this->render('AppBundle:Track:_files.html.twig', array('files'=> $files, 'dir'=> $dirRoot));
        
    } 

    //ajaxSavePlaylist
    public function ajaxSavePlaylistAction( Request $request ){

        $playlistNew  = $request->get('playlist');

        $tituloTrack  = $request->get('track');
        
        $em = $this->getDoctrine()->getManager();    
                
        $track = $em->getRepository('AppBundle:Track')->findOneBy(array('nombre_archivo'=> $tituloTrack));

        $genero = $em->getRepository('AppBundle:Genero')->findOneBy(array('nombre' => $track->getGenero()->getNombre()));    

        $playlist = new Playlist();                
        $playlist->setNombre($playlistNew);                
        $playlist->setDescripcion("agregado frontend");  
        $playlist->setUsuario($this->getUser());
        $em->persist($playlist);     
        
        $playlistTrack = new PlaylistTrack();                
        $playlistTrack->setTrack($track);                
        $playlistTrack->setPlaylist($playlist);                
        $playlistTrack->setPosition($genero->getPosicion());    


        $em->persist($playlistTrack);     

        try{
            $em->flush();
            $response['status'] = "OK";
        }catch( Exception $exc){
            $response['status'] = "Error";
            $response['message'] = $exc->getMessage();
        }
        
        return new Response( json_encode( $response ) );
        
    } 

    public function ajaxAddTrackToPlaylistAction( Request $request ){

        $playlistID  = $request->get('playlistID');

        $track  = $request->get('track');
        
        $em = $this->getDoctrine()->getManager();    
                
        $track = $em->getRepository('AppBundle:Track')->findOneBy(array('nombre_archivo'=> $track));

        $playlist = $em->getRepository('AppBundle:Playlist')->find($playlistID);
                                
        $playlist->addTrack($track);     

        $playlistTrack = new PlaylistTrack();

        $playlistTrack->setTrack($track);
        $playlistTrack->setPlaylist($playlist);
        $playlistTrack->setPosition(1);

        $em->persist($playlistTrack);     

        try{
            $em->flush();
            $response['status'] = "OK";
        }catch( Exception $exc){
            $response['status'] = "Error";
            $response['message'] = $exc->getMessage();
        }
        
        return new Response( json_encode( $response ) );
        
    } 

    //ajaxThumbByArtista
    public function ajaxThumbByArtistaAction( Request $request ){

        $artista  = $request->get('artista');
                
        $client = new \Google_Client();
        $client->setApplicationName("test");
        $apiKey = "AIzaSyApjjZ28U1-a8PRjGxEuNAJbQJXU68FXgc"; // Change this line.
        // Warn if the API key isn't changed.
        if (strpos($apiKey, "<") !== false) {
          echo missingApiKeyWarning();
          exit;
        }
        $client->setDeveloperKey($apiKey); 
        
        $artistaThumbnails = $this->getThumbnailsByArtista( $client, $artista );  
        
                
        return $this->render('AppBundle:Artista:_artistaThumbnails.html.twig', array('thumbs'=> $artistaThumbnails, 'artista'=> $artista ));
        
    } 

    private function getThumbnailsByArtista( $client, $artista ){

        $thumbs = array();

        $youtube = new \Google_Service_YouTube($client);

        $artistaResponse = $youtube->search->listSearch('id,snippet', array(
              'q' => strtolower($artista),
              'maxResults' => 10,
        ));    

    
        foreach ( $artistaResponse->getItems() as $thumb) {            
            $thumbnailsOnline = $thumb->getSnippet()->getThumbnails();
            
            $thumbnail['thumb'] = $thumbnailsOnline->getDefault()['url'];

            $thumbs[] = $thumbnail;

        }    

        return $thumbs;                
        
    }

    public function ajaxSaveThumbByArtistaAction( Request $request ){

        $thumbnail  = $request->get('thumbnail');
        
        $artista  = $request->get('artista');
        
        $pathTemp = $this->get('kernel')->getRootDir() . '/../web/thumbnails/artistas/'; 
        
        $thumbFile = strtolower( $artista ).".jpg"; 

        $pathThumb = $pathTemp . $thumbFile;

        $response = array();

        if( unlink( $pathThumb )){
            if( copy( $thumbnail, $pathThumb ) ){
                $response['status'] = "OK";
            }else{
                $response['status'] = "Error";
            }
        }
        
        return new Response(json_encode( $response ));        
        
        
    } 

     public function getAjaxTracksRelatedAction(Request $request) {        
                
        $em = $this->getDoctrine()->getManager();            
        
        $track = $em->getRepository('AppBundle:Track')->find((int)$request->get('track'));       
        
        var_dump($track->getArtista());
        exit();
        if( $track->getArtista()->getTracks() == null ){
            $tracks = $track->getArtista()->getTracks();
        }

        /*$tracks = $em->getRepository('AppBundle:Track')
                ->find( array('genero'=> $track->getGenero()->getId()));           */
                                                

        return $this->render('AppBundle:Track:tracksRelated.html.twig', array(
            'tracks' => $tracks           
        ));
    }


    private function createRealPathNewFile( $nameFile, $newNameFile ){
        $inicioFormato = strrpos($nameFile, ".");
        
        $formato = substr($nameFile, $inicioFormato );        

        $rootPathTemp = substr($nameFile, 0, strrpos($nameFile, "/"));
        
        $newFile = $rootPathTemp."/".$newNameFile.$formato;

        return $newFile;
    }


    

    

    private function getSize( $file ){
        $bytes = filesize($file['filename']) . PHP_EOL;

                    if ($bytes >= 1073741824)
                    {
                        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
                    }
                    elseif ($bytes >= 1048576)
                    {
                        $bytes = number_format($bytes / 1048576, 2) . ' MB';
                    }
                    elseif ($bytes >= 1024)
                    {
                        $bytes = number_format($bytes / 1024, 2) . ' KB';
                    }
                    elseif ($bytes > 1)
                    {
                        $bytes = $bytes . ' bytes';
                    }
                    elseif ($bytes == 1)
                    {
                        $bytes = $bytes . ' byte';
                    }
                    else
                    {
                        $bytes = '0 bytes';
                    }
        return $bytes;            
    }


    
}
