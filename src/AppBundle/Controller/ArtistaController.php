<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Artista;
use AppBundle\Form\ArtistaType;

/**
 * Artista controller.
 *
 */
class ArtistaController extends Controller
{

    /**
     * Lists all Artista entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Artista')->findAll();

        return $this->render('AppBundle:Artista:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Artista entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Artista();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $artista =$entity->getNombreCorto() . " - ".$entity->getNombre();
            
            $this->get('session')->getFlashBag()->add(
                    'ok', 'Se ha guardado correctamente el artista ' .$artista
            );
            
            return $this->redirect($this->generateUrl('artista'));
        }

        return $this->render('AppBundle:Artista:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Artista entity.
     *
     * @param Artista $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Artista $entity)
    {
        $form = $this->createForm(new ArtistaType(), $entity);

        return $form;
    }

    /**
     * Displays a form to create a new Artista entity.
     *
     */
    public function newAction()
    {
        $entity = new Artista();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:Artista:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Artista entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Artista')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Artista entity.');
        }
        

        return $this->render('AppBundle:Artista:show.html.twig', array(
            'entity'      => $entity
            
        ));
    }

    /**
     * Displays a form to edit an existing Artista entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Artista')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Artista entity.');
        }

        $editForm = $this->createEditForm($entity);
        

        return $this->render('AppBundle:Artista:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView()
            
        ));
    }

    /**
    * Creates a form to edit a Artista entity.
    *
    * @param Artista $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Artista $entity)
    {
        $form = $this->createForm(new ArtistaType(), $entity);

        return $form;
    }
    /**
     * Edits an existing Artista entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Artista')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Artista entity.');
        }

        
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $artista =$entity->getNombreCorto() . " - ".$entity->getNombre();
            
             $this->get('session')->getFlashBag()->add(
                    'ok', 'Se ha guardado correctamente el artista ' .$artista
            );
            
            return $this->redirect($this->generateUrl('artista'));
        }

        return $this->render('AppBundle:Artista:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView()
            
        ));
    }
    
}
