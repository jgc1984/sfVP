<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Playlist;
use AppBundle\Form\PlaylistType;

/**
 * Playlist controller.
 *
 */
class PlaylistController extends Controller
{

    /**
     * Lists all Playlist entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Playlist')->findAll();

        return $this->render('AppBundle:Playlist:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Playlist entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Playlist();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add(
                    'ok', 'Se ha guardado correctamente el Playlist .' . $entity->getNombre() 
            );
            return $this->redirect($this->generateUrl('playlist'));
        }

        return $this->render('AppBundle:Playlist:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Playlist entity.
     *
     * @param Playlist $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Playlist $entity)
    {
        $form = $this->createForm(new PlaylistType(), $entity);
        

        return $form;
    }

    /**
     * Displays a form to create a new Playlist entity.
     *
     */
    public function newAction()
    {
        $entity = new Playlist();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:Playlist:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Playlist entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Playlist')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Playlist entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Playlist:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Playlist entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Playlist')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Playlist entity.');
        }

        $editForm = $this->createEditForm($entity);
        

        return $this->render('AppBundle:Playlist:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView()
            
        ));
    }

    /**
    * Creates a form to edit a Playlist entity.
    *
    * @param Playlist $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Playlist $entity)
    {
        $form = $this->createForm(new PlaylistType(), $entity);
            
        return $form;
    }
    /**
     * Edits an existing Playlist entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:PlaylistTrack')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Playlist entity.');
        }

        
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            //$em->persist($entity);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add(
                    'ok', 'Se ha actualizado correctamente el Playlist .' . $entity->getNombre() 
            );
            
            return $this->redirect($this->generateUrl('playlist'));
        }

        return $this->render('AppBundle:Playlist:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),            
        ));
    }
    /**
     * Deletes a Playlist entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:PlaylistTrack')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Playlist entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Playlist'));
    }

    /**
     * Creates a form to delete a Playlist entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('Playlist_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }





    public function playlistSettingsAction($id)
    {                                        
        $appManager = $this->get('app.manager');                        
                    
        $em = $this->getDoctrine()->getManager();   

        $playlist = $em->getRepository('AppBundle:Playlist')->find($id);

        $tracks = $appManager->getTracksByPlaylist($id);
                
        if (!$playlist) {
            throw $this->createNotFoundException('Unable to find Playlist entity.');
        }

        $editForm = $this->createEditForm($playlist);
                                
        return $this->render('AppBundle:Playlist:playlist_settings.html.twig',  array(            
                'playlist' => $playlist,                            
                'tracks' => $tracks,                            
                'form'   => $editForm->createView()
        ));                                                    
    }
}
