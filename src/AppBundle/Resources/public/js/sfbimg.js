/************************************* FUNCIONES GENERALES ***********************************************/
var sfBIMG = {
  folderTargetShow: false,

  setFolderTargetShow: function( state ){
    this.folderTargetShow = state;
  },

  getFolderTargetShow: function(){
    return this.folderTargetShow;
  },

  /**/
  init: function init( opener ){
    this.opener = opener;

    sfBIMG.confirmOut(true);



  },

/************************ CALLBACK FUNCTIONS RESPONSE *****************************************************/  
  loadTracksByArtista: function( response ){
    $('.content_right').html( response );                

    sourceYoutube = $('#template-player').html();  
    
    templateHeader = Handlebars.compile(sourceYoutube);            
    
    $('.contenido').html(templateHeader);      

    vlc = document.getElementById("vlc");
    vlc.playlist.play();  
  },

  getFilesByDir: function ( dir ){
    var self = this;
    var data = "dir="+encodeURIComponent(dir);
    var req = new AjaxHTTPRequest(Routes.getFilesByDir(), "html", "POST", data, sfBIMG.getFilesByDirCallback, dir); 

    req.callAjax();  
  
  },

  /* FUNCIONES VARIAS CALLBACK*/
  getInfoDirRootCallback: function ( response, params=null ){
    var self = this;
    $( '.directorios-source' ).html();       
                                                  
    $( '.directorios-source' ).append(response);       
    
    sfBIMG.getFilesByDir(params);              
  },

  getInfoDirRootTargetCallback: function ( response, params=null ){
    $( '.directorios-target' ).html();       
                                                              
    $( '.directorios-target' ).append(response);  

    $( 'button.importar').show();     
  },

  callbackBefore: function (){    
    var html = "<div class='row proccessing'>";
    html += "<div class='col-md-8'>";
    html += "<img src=" + Routes.getGif() +"/>";
    html += "</div>";
    html += "</div>";
                                                              
    $( html ).insertAfter('.row:last');    
  },

  getFilesByDirCallback: function( response ){
    $( '.files' ).remove();                                                            
    $( '.content-import' ).append(response);                
  },


  callbackImport: function( response, params ){
    $( params ).remove();                                                                
    alert("Guardado correctamente !!!");
    return false;
  },

  analizedFilesCallback: function( response, params ){
    //$( '.content-import' ).append(response);
    var json = JSON.parse(response);

    /*var htmlDuration  = "<i class='fa fa-clock-o'></i>"+ json.time; 
    var htmlSize      = "<strong> "+json.formatSizeUnits +"</strong>";*/
    var htmlCheck     = "<i class='fa fa-check'></i>";
    var elementBefore = $( params ).find('input[name=file]');
    var buttonImport  = "<button title='importar a biblioteca' class='choice-target' style='color: #222;'><span aria-hidden='true' class='glyphicon glyphicon-save'></span></button>"; 
    var button        = "<button type='button' class='btn btn-info' data-toggle='tooltip' data-placement='top' title='Duracion:"+json.time+", Tamaño: "+json.formatSizeUnits+"'><i class='fa fa-info'></i></button>";
    var inputResolutionWidth= "<input type='hidden' value='"+json.resolution.width+"' name='resolution_width'>";
    var inputResolutionHeight = "<input type='hidden' value='"+json.resolution.height+"' name='resolution_height'>";
    var inputSize = "<input type='hidden' value='"+json.size+"' name='size'>";
    var inputTime = "<input type='hidden' value='"+json.timeRaw+"' name='duration'>";
    var inputPath = "<input type='hidden' value='"+json.path+"' name='path'>";
    var inputFilename = "<input type='hidden' value='"+json.image+"' name='filename'>";
          

    /*
    var file       = $( li ).find('input[name=filename]').val();        
    var path       = $( li ).find('input[name=path]').val();
    var size       = $( li ).find('input[name=size]').val();
    var width      = $( li ).find('input[name=resolution_width]').val();
    var height     = $( li ).find('input[name=resolution_height]').val();
    var duration   = $( li ).find('input[name=duration]').val();
    */

    $( htmlCheck ).insertBefore(elementBefore);
    $( elementBefore ).remove();

    //$( params ).append(htmlDuration);
    //$( params ).append(htmlSize);
    $( '.files-analized-options' ).append(buttonImport);
    $( params ).append(inputResolutionWidth);
    $( params ).append(inputResolutionHeight);
    $( params ).append(inputSize);
    $( params ).append(inputTime);
    $( params ).append(inputPath);
    $( params ).append(inputFilename);
    $( params ).append(button);
    $( params ).addClass("import-success");
    $( params ).addClass("analized-success");

    /*var html = "<div class='row'>";
    html = "<div class='col-md-8'>";
    html += "<a href=''>Ver archivos</a>";
    html += "</div>";
    html += "</div>";
    */
    //$( html ).insertBefore('.files');

    //$('.files').hide("slow");
  },

  getDirDestinationCallback: function( response ){                  
    $( '.content-import' ).append(response);                                                          
  },

  getImportFiles: function( response, params ){      
    $( '.processing-bar' ).remove();
    
    $( params ).attr("disabled", true );
    sfBIMG.getRemove();
  },

  /* FIN FUNCIONES CALLBACK RESPONSE */
  
  savePlaylist: function(){                
    html  = "<li class='list-group-item list-group-item-success'><i class='fa fa-check'></i>";
    html += playlistNew;
    html += "</li>";

    $( html ).insertBefore('.playlists li:first');
    $('.playlists li:last').remove();
  },
  
  getTracksByPlaylist: function(response) {
    $('.content_right').html(response);      
  },

  getPlaylists: function ( response ) {    
    $('.contenido').append( response );  
  },
  addTrackToPlaylist: function (response) {      
    html  = "<li class='list-group-item list-group-item-success'><i class='fa fa-check'></i>";
    html += "!!!Agregado";
    html += "</li>";

    $( html ).insertBefore('.playlists li:first');
    $('.playlists li:last').remove();        
  },

  getFilesToAnalized: function( list ){
    //var files = [];
    $('.directorios-source').hide("slow");

    $.each( $( list ), function ( index, element ) {      
      if( $( element ).find('input[name=file]').is(':checked') ) {
        var file = $( element ).find('input[type=hidden]').val();
        
        var fileImport = {
          filename: encodeURIComponent(file)

        }

        var data = "file="+JSON.stringify(fileImport);

        html = "<img src=" + Routes.getGif() +"/>";

        $( element ).append(html);

        $( '.directorios-source' ).find("select").attr("disabled", true );

        $( element ).find('input[type=checkbox]').attr("disabled", true );

        $( 'button.analizar' ).attr("disabled", true);

        var req = new AjaxHTTPRequest(Routes.getThumb(), "html", "GET", data, sfBIMG.analizedFilesCallback, element); 
  
        req.callAjax(); 

        $( element ).find('img').remove();
        $( 'button.analizar' ).attr("disabled", false);

        $( element ).addClass("import-success");
        $( element ).addClass("analized-success");
      }

    }); 

  },
  getRemove: function(){
    
    $.each( $('.analized').find('li'), function ( index, element ) {
      if( $( element ).find('input[type=checkbox]').is(':checked') ) {
          $( this ).remove();        
      }
    }); 
    
  },
  //sfBIMG.getFilesToImport( filesAnalized, dataExtra, element ); 
  getFilesToImport: function( list, dataExtra ){     

    $.each( $( list ).find('li.import-success'), function ( index, li ) {             
      //if( $( elem ).hasClass("import-success") || $( elem ).hasClass("analized-success")) {                  
          
          var file       = $( li ).find('input[name=filename]').val();        
          var path       = $( li ).find('input[name=path]').val();
          var size       = $( li ).find('input[name=size]').val();
          var width      = $( li ).find('input[name=resolution_width]').val();
          var height     = $( li ).find('input[name=resolution_height]').val();
          var duration   = $( li ).find('input[name=duration]').val();
          var resolution = width + "x" + height;
                  
          var fileInfoImport = {
            filename: encodeURIComponent(file),
            path: encodeURIComponent(path),
            size: size,          
            resolution: resolution,
            duration: duration
          };      
          

          var extra = JSON.stringify(dataExtra);
    
          var data = "file="+JSON.stringify(fileInfoImport)+"&extra="+extra;        
          
          var req = new AjaxHTTPRequest(Routes.getImportFiles(), "html", "GET", data, sfBIMG.callbackImport, li, null, null, sfBIMG.callbackBefore); 

          req.callAjax();   
          
     //   }
              
    }); 
    
  },
  
  getGener: function( datos ){        
        var Geners = [];

        $.each( datos.find('li'), function( key, element ) {
                console.log(element);
                valor = $(element).find('input[name="genero"]').val();                 
                var Gener = [];                                  
                pathFile = $(element).find('input[name="path"]').val();
                
                if( $.isNumeric(valor) ){
                  Gener.push(valor);                
                  Gener.push(pathFile);                
                  Geners.push(Gener);
                }
                
                                            
        });                

        Geners.sort(function(a, b) {
            return a[0] - b[0];
        });                

        return Geners;    
  },
      
  transferUsb: function ( playlist ) {      
        $.ajax({
            url: Routes.getTransferUsb(),
            dataType: 'json',
            method: 'POST',
            async: false,
            data: {
                playlistID: playlist
            },
            beforeSend : function (){
              sfBIMG.blockScreen(Routes.getGif(), "Transfiriendo a usb");
            },
            success: function (response) {
                alert(response.message);
                //getDataByTrack();
            }
        });
   },

  searchType: function( term, place, response ){
    switch(place){
        case 'youtube':
          var data = { term: term, place: place };
          break;
        case 'pc':
          var data = {
           term: term,
           place: place,
           class: Search.getClassName(),
           property: Search.getProperty(),
           search_method: Search.getMethod()
         };
         break;
      }      
      var req = new AjaxHTTPRequest(Routes.getAjaxDefault(), "json", "POST", data, sfBIMG.getTracksCallback, place, response); 

      req.callAjax();  
      
  },
  getTracksCallback: function( data, placesource, response ){      
    switch( placesource ){
      case 'youtube':
        response( $.map( data, function( item ) {                   
          return {               
             title: item.title,
             thumb: item.thumb,
             url: item.url
             
          }
        }));
        break;
      case 'pc':
        response( $.map( data, function( item ) {                                                                                         
          return {               
             id: item.id,
             thumb: item.thumb,
             url: item.url,                     
             title: item.title,
             artista: item.artista,
             nombre_archivo: item.nombre_archivo,
             position: item.genero

          }
        }));
        break;  
    }            
        
  },

  getOthersFavorites: function ( elementContainer, track ){
    var Tracks = new Array();
    Tracks.push(track);
    $.each( elementContainer.find('li'), function( key, element ) {                
        folder = $( element ).find('input[name="path"]').val();  
        file = $( element ).find('input[name="archivo"]').val();  
        trac = folder + file;        
        Tracks.push(trac);    
        
    });

    /* NO BORRAR USO POSIBLE DESPUES */
    
    /*Tracks = jQuery.grep(Tracks, function(value) {
        return value != track;
    });*/

    return Tracks;
  },

  setFavoriteByTrack: function ( response ) {
      alert(response.message);
  },

   getTracksRelated: function ( response ){                                    
      $('.content_right').html( response );
   },

   getThumbnails: function( response ){ 
      $('.content').append( response );      
      $('#library-import').modal('show');                                               
   },

   saveThumbnailArtist : function( response ){ 
      if( response.status == "OK" ){
          $('#library-import').modal('hide');
      }
   },
   getTimeFiles: function(response){
      alert(response.message);
   },
   getPlaylistsOnSearch: function (response) {      
      $( '.contenido' ).append( response );
      $( '#containerPlaylists').collapse("show");
   },

   build2: function( fileData ){  
        self = this;    
    
    //if( $( fileData ).parent().children().size() ){
        var elementCurrent = $( fileData ).next();                                
        var vlc = document.getElementById("vlc");
        var folder = $( elementCurrent ).find('input[name="path"]').val(); 
        var file = $( elementCurrent ).find('input[name="nombre_archivo"]').val(); 
        var duracion = $( elementCurrent ).find('input[name="duracion"]').val(); 
        var genero = $( elementCurrent ).find('input[name="genero"]').val(); 
        var position = $( elementCurrent ).find('input[name="position"]').val();

        if( typeof genero === "undefined" ){
            var genero = null;
        }
    
        if( file !== fileCurrent ){
                var fileCurrent = "file:///"+ folder + file;          
                vlc.playlist.add(fileCurrent);                

                tracksByGenero.push( {
                    information:{
                      genero: genero    
                    },                            
                    track:{  
                      titulo:  file,
                      nombre: file,
                      path:   folder,                       
                      duracion: duracion,
                      position: position                            
                    }
                });            
                self.build2( elementCurrent );                                                                                
        }     
        
    //}
        
    return tracksByGenero;
  },
  /*checkAll: function( list ){
   $.each( $( list ), function( index, element ){       
       $( element ).find('input[type=checkbox]').prop("checked", true);     
   });
  },*/

  hasTracksToImport: function( list ){      
   $.each( $( list ).find('li'), function( index, element ){       
      var input = $( element ).find('input[type=checkbox]');     
      if ( $( input ).is(':checked') ){ 
                
         if( sfBIMG.getFolderTargetShow() == false ){
            var req = new AjaxHTTPRequest(Routes.getDirDestination(), "html", "POST", null, sfBIMG.getDirDestinationCallback); 

            req.callAjax(); 
                        
            sfBIMG.setFolderTargetShow(true);               
        
         }
        
      }
   });

   
   
} 


}
    


var tools = {
  resetForm: function ( form ) {
    form.find('input, textarea, input:not([type="submit"])').removeAttr('value');
    form.find('input, textarea, input:not([type="submit"])').val('');
    form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

    form.find('select option').removeAttr('selected').find('option:first').attr('selected', 'selected');

  },


}


$.randomize = function( arr ) {  
   for(var j, x, i = arr.length; i; j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], arr[j] = x);
   return arr;   
};

