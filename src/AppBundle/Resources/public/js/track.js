var Track = {
	title: null,
	path:null,
	nombreArchivo: null,
	position: null,
	size: null,

	setTitle: function( title ){
		this.title = title;
	},
	getTitle: function(){
		return this.title;
	},

	setPath: function( path ){
		this.path = path; 
	},

	getPath: function(){
		return this.path;
	},

	setNombreArchivo: function( file ){
		this.nombreArchivo = file; 
	},

	getNombreArchivo: function(){
		return this.nombreArchivo;
	},

	setPosition: function( position ){
		this.position = position; 
	},

	getPosition: function(){
		return this.position;
	},

	setSize: function( size ){
		this.size = size; 
	},

	getSize: function(){
		return this.size;
	},

	toString: function(){
		return this.getPath() + this.getNombreArchivo();
	}


}
