var Search = {
	className: null,
	property: null,
	method: null,

	setClassName: function( className ){
		this.className = className;
	},

	setProperty: function( property ){
		this.property = property;
	},

	setMethod: function( method ){
		this.method = method;
	},

	getClassName: function(){
		return this.className;
	},

	getProperty: function(){
		return this.property;
	},

	getMethod: function(){
		return this.method;
	}
	

}