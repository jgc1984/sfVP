function PlaylistEnquee(){
	var dateToday=new Date();
	
	this.name = dateToday.getFullYear() + "-" + dateToday.getMonth() + "-" + dateToday.getDate();
	this.tracks = [];

	/* Se agregan los nuevos tracks y se establece un orden segun el genero */
	this.addTrack = function( track ){	
		this.tracks.push(track);	
		this.tracks.sort(function(a, b) {
        	return ((a.position < b.position) ? -1 : ((a.position > b.position) ? 1 : 0));
    	});					
	},

	this.getTracks = function(){						
		return this.tracks;
	}

	this.getName = function(){
		return this.name;	
	}
}