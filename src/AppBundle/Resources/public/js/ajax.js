/*  CLASE PARA INSTANCIAR SOLO UNA VEZ LAS PETICIONES AJAX
 *  
 */
function AjaxHTTPRequest(url, formatResponse="html", method="POST", data, callbackSuccess, callbackParameters, response, messageLoading, callbackBefore ) {  
    this.url=  url;
    this.formatResponse= formatResponse;
    this.method= method;
    this.data= data; 
    this.callbackSuccess= callbackSuccess;
    this.callbackParameters= callbackParameters;
    this.response= response;
    this.messageLoading= messageLoading;    
    this.callbackBefore = callbackBefore;  
        
    this.callAjax = function(){
        var request = this ;
        $.ajax({
               url: this.url,
               dataType: this.formatResponse,    
               method: this.method,  
               data: this.data,               
               async: false,
               beforeSend: function(){                                
                /* EJECUTA LA CALLBACK FUNCTION BEFORE SI ES PASADA - UTIL PARA LAS FUNCIONES MODALES LAS CUALES YA BLOQUEAN EL BROWSER */
                if( request.callbackBefore != null || typeof request.callbackBefore != "undefined"){                                    
                  request.callbackBefore();
                }else{
                  request.blockScreen(Routes.getGif(), request.messageLoading);                
                }
               },
               success: function( response ) {                                                                                     
                  request.callbackSuccess( response, request.callbackParameters, request.response );
               },
               complete: function(){
                $('.proccessing').remove();
                request.unBlockScreen();
               },
               error: function( data ){
                alert(data);
                return false;
               }


        });    
    },

    
    this.blockScreen = function ( gif, message ) {      
      $.blockUI({
          message: message + '<img src="' + gif + '"/>',
          css: {
              top: '100px',
              border: 'none',
              padding: '15px',
              backgroundColor: '#000',
              '-webkit-border-radius': '10px',
              '-moz-border-radius': '10px',
              opacity: .5,
              color: '#fff'
          },
          timeout:15000
      });

    },
  
    this.unBlockScreen = function () {
        setTimeout($.unblockUI, 1000);
    },

    this.confirmOut=  function ( confirm ){
      if( confirm == true ){
        window.addEventListener("beforeunload", function (e) {
                      var confirmationMessage = "\o/";

                      (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                      vlc.playlist.stop();
                      return confirmationMessage;     
                                           //Webkit, Safari, Chrome
        });    
      }  
    },

    this.messageAlert= function( message ){
        alert( message );
    }

}