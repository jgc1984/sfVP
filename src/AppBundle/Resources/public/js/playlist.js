function Playlist( name ){
	this.name = name;
	this.tracks = [];

	/* Se agregan los nuevos tracks y se establece un orden segun el genero */
	this.addTrack = function( track ){	
		var options = new Array(":aspect-ratio=4:3", "--http-port=8080");
		//var id = vlc.playlist.add("rtsp://servername/item/to/play", "fancy name", options);

		var fileCurrent = "file:///"+ track;  		
		
		vlc.playlist.add(fileCurrent, null, options);  
		this.tracks.push(track);		
	},

	this.getName = function(){
		return this.name;
	},
	
	this.getTracks = function(){						
		return this.tracks;
	},

	this.play = function(){
		vlc.playlist.play();  
	},

	this.playAll = function(){

	},

	this.toString = function(){
		return this.getName;
	}

	
}