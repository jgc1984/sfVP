

Handlebars.registerHelper('iff', function(a, operator, b, opts) {
    var bool = false;
    switch(operator) {        
       case '==':           
           bool = a == b;                       
           break;
       case '>':
           bool = a > b;
           break;
       case '<':
           bool = a < b;
           break;                   
       default:
           throw "Unknown operator " + operator;
    }
 
    if (bool) {
        return opts.fn(this);
    } else {
        return opts.inverse(this);
    }
});

Handlebars.registerHelper("len", function(json) {    
    return Object.keys(json).length;
});

Handlebars.registerHelper('linkPages', function(link, options, html="", i=0) {    
    console.log("Debuggin helper");
    i += 1;
    //var count = Object.keys(links).length;
    
    html += "<a href='#'>\n";
    html += i;
    
    
    /*for (var i = 1; i <= pages; i++) {
        html += i;
    }    */
    return html + "</a>\n";

});



Handlebars.registerHelper('if_eq', function(a, b, opts) {
    if (a == b) {
        return opts.fn(this);
    } else {
        return opts.inverse(this);
    }
});