$( document ).on('click', 'button.playAll', function ( event ) {
    event.preventDefault();    
    
    var vlc =  document.getElementById('vlc');      
    
    if( typeof vlc == "undefined" || vlc === null){
      var header = $('#template-player').html();  
      var templateHeader = Handlebars.compile(header);            
      var templateContent = templateHeader(); 
      $('.contenido').html(templateContent);
      var vlc =  document.getElementById('vlc');      
    }
    tracks = $( this ).parent().parent();            
    
    vlc.playlist.clear();

    var playlist = new Playlist("untitled");

    $.each( $( tracks ).find('.track'), function( key, element ) {                
       path           = $( element ).find('input[name="path"]').val(); 
       nombre_archivo = $( element ).find('input[name="nombre_archivo"]').val();
       position       = $( element ).find('input[name="position"]').val();            
       
       Track.setPath( path );    
       Track.setNombreArchivo( nombre_archivo );    
       Track.setPosition( position );                

       playlist.addTrack(Track);
       
    });

    console.log(playlist.getName());     

                
    playlist.play();     

    $( this ).attr("disabled");
    $( this ).css("background-color", "#ccc");                                                          
    
});

$( document ).on('click', '.track', function () {            
    vlc.playlist.stop();
    vlc.playlist.items.clear();            
    var path = $( this ).parent().find('input[name=path]').val();    
    var filename = $( this ).parent().find('input[name=archivo]').val();    
    var file = path + filename;
    vlc.playlist.add("file:///"+file);
    vlc.playlist.play();
});

$( document ).on('click', 'button.trackItem', function () {                  
    vlc.playlist.playItem("/media/DATOS/VIDEOS/ROCK/SODA STEREO/1997/EL ULTIMO CONCIERTO/rock-1997 - Soda Stereo - EL Ultimo Concierto - Paseando por roma.mpg.vob");
});

$( document ).on('click', 'button.backtrack', function () {              
    vlc = document.getElementById("vlc");
    vlc.playlist.prev();
    vlc.playlist.play();
});

$( document ).on('click', 'button.next-track', function () {              
    vlc = document.getElementById("vlc");
    vlc.playlist.next();
    vlc.playlist.play();
});


$(document).on( 'click',".stop", function( event ) {            
  document.getElementById('vlc').vlc.playlist.stop();                          
});

$(document).on( 'click', '.fa-star', function(){
     track = $(this).parent().parent().parent().find('input').val();
     setFavoriteByTrack(track);
});

/*$(document).on('click', '.track', function () {  
          $('.sliders').hide("slow");
            var htmlEmbebedPlayer  = "<embed type='application/x-vlc-plugin' name='videotest'  pluginspage='http://www.videolan.org' version='VideoLAN.VLCPlugin.' id='vlc' height='500' width='1075px' toolbar='true' windowless='true' bgcolor='blue'/>";            
            $('.player').html(htmlEmbebedPlayer);                               
            var path = $(this).parent().find('input[name="path"]').val();            
            var archivo = $(this).parent().find('input[name="archivo"]').val();
            link = "<a href='#' class='list-group-item active'><i class='fa fa-volume-up active'></i></a>";            
            track = path + archivo;
            
            vlc =  document.getElementById('vlc');  
            vlc.playlist.add("file:///"+track);
            vlc.playlist.play();

});*/



/*$(document).on( 'click',".fa-random", function( event ) { 

         $('.sliders').hide();

         htmlEmbebed  = "<embed type='application/x-vlc-plugin' name='videotest'  pluginspage='http://www.videolan.org' version='VideoLAN.VLCPlugin.' id='vlc' class='playerEmbeded' toolbar='true' windowless='true' bgcolor='blue'/>";

         $('.content').html(htmlEmbebed); 

         var vlc =  document.getElementById('vlc');
          
         var datos = $('.tracksByArtist ol.list-group');
         
         var tracks = new Array();

         $.each( datos.find('li.track'), function( key, value ) {
              
             var pathFolder = $( value ).find('input[name="path"]').val();
             var name_file = $( value ).find('input[name="nombre_archivo"]').val();

             var filePathFull = pathFolder + name_file;
                          
             tracks.push(filePathFull);

         
             var aleatorio = $.randomize(tracks);

             $.each(aleatorio, function(key, value) {
                   var file = "file://"+value;
                   
                   vlc.playlist.add(file);

             });       
                            
             vlc.playlist.play();
                  
         });                   

});*/



/*$(document).on( 'click', '#save_playlist', function( event ) { 
  var data = JSON.stringify($('#playlist').serializeArray());
  event.preventDefault();
     $.ajax({
         method: 'GET',
         url: Routes.getSavePlaylists,
         dataType : 'html',
         data: 'data='+data,
         success : function( response ) {
           
           var json = JSON.parse(response);                                            
             if ( json.status == "OK") {
              alert("Lista creada correctamente");
             }
         }
      
      });
});*/