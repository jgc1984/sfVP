var bool = false;  
var hasElements = false;

$( document ).on('click', '.content-import p a', function ( event ){  
  event.preventDefault();
  $('.directorios').show("slow");

});

/* SE PODRIA UTILIZAR POSTERIORMENTE PARA CARGA EN DEMANDA DE LOS SLIDERS 
$(".tracks li img").on({    
    mouseenter: function () {
        var slider = $( this ).parent().parent();
        $( slider ).bxSlider();
    },
    mouseleave: function () {
        console.log(2);
    }
});

*/

/* PARA BUSCAR EL DIRECTORIO QUE CONTIENE LOS ARCHIVOS A IMPORTAR */
$( document ).on('change', 'select[name=source]', function (){            
        var dir = $( this ).find('option:selected').val();              

        var data = "dir="+encodeURIComponent(dir)+"&place="+$( this ).attr('name');
                
        var req = new AjaxHTTPRequest(Routes.getInfoDirRoot(), "html", "POST", data, sfBIMG.getInfoDirRootCallback, dir, null, null, sfBIMG.callbackBefore); 

        req.callAjax();  
      
        $( this ).find('option:selected').removeClass("glyphicon-folder-close").addClass("glyphicon-folder-open");            
        
});

/* ANALISIS DE LOS ARCHIVOS SELECCIONADOS*/
$( document ).on('click', 'button.analizar', function ( event ){
  event.preventDefault();        
     
  var listFiles = $( '.files ul li' ); 

  sfBIMG.getFilesToAnalized(listFiles);    

});

/* PARA BUSCAR EL DIRECTORIO DONDE IMPORTAR LOS ARCHIVOS*/
$( document ).on('change', 'select[name=target]', function (){            
  var dir = $( this ).find('option:selected').val();              

  var data = "dir="+encodeURIComponent(dir)+"&place="+$( this ).attr('name');
  
  
  var req = new AjaxHTTPRequest(Routes.getInfoDirRoot(), "html", "POST", data, sfBIMG.getInfoDirRootTargetCallback, null, null, null, sfBIMG.callbackBefore); 

  req.callAjax();       

  $( this ).find('option:selected').removeClass("glyphicon-folder-close").addClass("glyphicon-folder-open");            
        
});

/* SELECCIONA TODOS LOS ARCHIVOS DEL LISTADO QUE SE VAN A ANALIZAR*/
$( document ).on('click', 'input[name=select_all]', function (){   
   var listaarchivos = $('.files ul li');    
   sfBIMG.checkAll(listaarchivos);
});

/* SELECCIONA TODOS LOS ARCHIVOS DEL LISTADO QUE SE VAN A IMPORTAR
$( document ).on('click', 'input[name=select_analized]', function (){   
   var listFilesAnalized = $('.analized li');    
   sfBIMG.checkAll(listFilesAnalized  );
});*/

/* SE COMPRUEBA QUE HAYA ARCHIVOS A IMPORTAR */
/*$( document ).on('click', '.analized input[type=checkbox]', function (){   
  var list = $('.analized');
  sfBIMG.hasTracksToImport(list);    
});*/


$( document ).on('click', '.choice-target', function ( event ){ 
  if( sfBIMG.getFolderTargetShow() == false ){
    var req = new AjaxHTTPRequest(Routes.getDirDestination(), "html", "POST", null, sfBIMG.getDirDestinationCallback); 

    req.callAjax(); 
                
    sfBIMG.setFolderTargetShow(true);               

 }

});   
/* IMPORTAR LOS ARCHIVOS  */
$( document ).on('click', '.importar', function ( event ){    

    event.preventDefault();      

    /*var artista = $('input[name=artista]').val();  

    if( typeof artista === "undefined"){
      artista = null;
    }  

    var fuente = $('input[name=fuente]').val();

     if( typeof fuente === "undefined"){
      fuente = null;
    }  

    var genero = $('input[name=genero]').val();

    if( typeof genero === "undefined"){
      genero = null;
    }*/  

    var dirTarget = $('select[name=target]:last').find('option:selected').val();    
    
    //var dataExtra = { artista: artista, fuente: fuente, genero: genero, dirTarget: dirTarget }  
    var dataExtra = { dirTarget: dirTarget }  

    var element = $('ul.files-found');    
    
    sfBIMG.getFilesToImport( element, dataExtra); 

  });




  

