var Routes = {
	gif: null,
	tracksByPlaylist: null,
	tracksByFuente: null,
	playlists: null,
	savePlaylist: null,
	addTracksToPlaylist: null,
	generateM3u: null,
	transferUsb: null,
	infoDirRoot: null,
	thumb: null,
	importFiles: null,
	thumbnailsArtista: null,
	thumbArtista: null,
	tracksArtista: null,
	trackRate: null,
	trackData: null,
	tracksRelated: null,
	filesInDir: null,	
	dirDest: null,
	timeFiles: null,
	default: null,

	setGif: function ( url ){
		this.gif = url;	
	},

	getGif: function (){
		return this.gif;	
	},

	setTracksByPlaylist: function ( url ){
		this.tracksByPlaylist = url;	
	},

	getTracksByPlaylist: function ( ){
		return this.tracksByPlaylist;	
	},

	setTracksByFuente: function ( url ){
		this.tracksByFuente = url;	
	},

	getTracksByFuente: function ( ){
		return this.tracksByFuente;	
	},

	setPlaylists: function ( url ){
		this.playlists = url;	
	},

	getPlaylists: function ( ){
		return this.playlists;	
	},

	setSavePlaylist: function ( url ){
		this.savePlaylist = url;	
	},

	getSavePlaylist: function ( ){
		return this.savePlaylist;	
	},

	setPlaylist: function ( url ){
		this.playlist = url;	
	},

	getPlaylists: function ( ){
		return this.playlist;	
	},

	setAddTrackToPlaylist: function ( url ){
		this.addTracksToPlaylist = url;	
	},

	getAddTrackToPlaylist: function ( ){
		return this.addTracksToPlaylist;	
	},

	setGenerateM3U: function ( url ){
		this.generateM3u = url;	
	},

	getGenerateM3U: function ( ){
		return this.generateM3u;	
	},

	setTransferUsb: function ( url ){
		this.transferUsb = url;	
	},

	getTransferUsb: function ( ){
		return this.transferUsb;	
	},

	setInfoDirRoot: function ( url ){
		this.infoDirRoot = url;	
	},

	getInfoDirRoot: function (){		
		return this.infoDirRoot;	
	},

	setThumb: function ( url ){
		this.thumb = url;	
	},

	getThumb: function (){
		return this.thumb;	
	},

	setImportFiles: function ( url ){
		this.importFiles = url;	
	},

	getImportFiles: function (){
		return this.importFiles;	
	},

	setThumbnailsNews: function ( url ){
		this.thumbnailsArtista = url;	
	},

	getThumbnailsNews: function (){
		return this.thumbnailsArtista;	
	},

	setThumbnailsByArtista: function ( url ){
		this.thumbArtista = url;	
	},

	getThumbnailsByArtista: function (){
		return this.thumbArtista;	
	},

	setTracksRelated: function ( url ){
		this.tracksRelated = url;	
	},

	getTracksRelated: function (){
		return this.tracksRelated;	
	},

	setTrackData: function ( url ){
		this.tracksData = url;	
	},

	getTrackData: function (){
		return this.trackData;	
	},

	setTrackRate: function ( url ){
		this.tracksRate = url;	
	},

	getTrackRate: function (){
		return this.trackRate;	
	},

	setTracksByArtista: function ( url ){
		this.tracksArtista = url;	
	},

	getTracksByArtista: function (){
		return this.tracksArtista;	
	},

	setFilesByDir: function ( url ){
		this.filesInDir = url;	
	},

	getFilesByDir: function (){
		return this.filesInDir;	
	},
	
	setDirDestination: function ( url ){
		this.dirDest = url;	
	},

	getDirDestination: function (){
		return this.dirDest;	
	},

	setTimeFiles: function ( url ){
		this.timeFiles = url;	
	},

	getTimeFiles: function (){
		return this.timeFiles;	
	},

	setAjaxDefault: function( url ){
		this.default = url;		
	},

	getAjaxDefault: function(){
		return this.default;		
	}


}
