/**JSON**/

$.ajax({
            method: 'POST',
            url: Routes.getSavePlaylist(),
            dataType : 'json',
            data: 'playlist='+playlistNew+'&track='+tituloCurrentTrack,
            success : function( data ){                
                html  = "<li class='list-group-item list-group-item-success'><i class='fa fa-check'></i>";
                html += playlistNew;
                html += "</li>";

                $( html ).insertBefore('.playlists li:first');
                $('.playlists li:last').remove();
            },

            error: function( data ){
                alert(data.message);
            }
    });

$.ajax({
        method: 'POST',
        url: Routes.getThumbnailsByArtista(),
        dataType : 'json',
        data: 'thumbnail='+encodeURIComponent(thumbnail)+'&artista='+artista,
        success : function( data ){ 
            if( data.status == "OK" ){
                $('#library-import').modal('hide');
            }
        },

        error: function( data ){
            alert( data.status );
        }
});

$.ajax({
         url: Routes.getTracksRelated(),
         dataType: "json",
         method: "post",
         data: {
           term: title,
           place: place,
           class: "{{class}}",
           property: "{{property}}",
           search_method: "{{search_method}}"
         },

         success: function( data ) {            
            sourceTemplateYoutube = $('#template-tracks-youtube').html();  
            templateYoutube = Handlebars.compile(sourceTemplateYoutube);            
            template = templateYoutube(data);          
            $(template).insertBefore('.sliders');            
         }        
  });

$.ajax({
         url: Routes.getTracksRelated(),
         dataType: "json",
         method: "post",
         data: {
           term: title,
           place: place,
 
         },

         success: function( data ) {            
            sourceTemplateYoutube = $('#template-tracks-youtube').html();  
            templateYoutube = Handlebars.compile(sourceTemplateYoutube);            
            template = templateYoutube(data);          
            $(template).insertBefore('.sliders');            
         }        
  });


/* HTML */
$.ajax({
            method: 'POST',
            url: Routes.getThumbnailsNews(),
            dataType : 'html',
            data: 'artista='+encodeURIComponent(artista),
            success : function( data ){ 
                $('.content').append( data );
                sfBIMG.unBlockScreen();
                $('#library-import').modal('show');                                               
            },

            error: function( data ){
                alert(data.message);
            }
    });




$.ajax({
        url: Routes.getAddTrackToPlaylist(),
        dataType: 'html',    
        method: 'POST',  
        data: "track="+track+"&playlistID="+playlistID,        
        success: function (response) {      
                html  = "<li class='list-group-item list-group-item-success'><i class='fa fa-check'></i>";
                html += "!!!Agregado";
                html += "</li>";

                $( html ).insertBefore('.playlists li:first');
                $('.playlists li:last').remove();        
          }
      });


 $.ajax({
    url: Routes.getPlaylists(),
    dataType: 'html',    
    method: 'POST',          
    success: function (response) {      
      $( response ).insertAfter( '.player' );
      
    }
  });   


$.ajax({
              url: Routes.getInfoDirRoot(),
              dataType: "html",
              method: "POST",              
              data: data,
              success: function( response ){                                                      
                           
                $( '.directorios-source' ).html();       
                                                              
                $( '.directorios-source' ).append(response);       

                setTimeout(5000);
                
                sfBIMG.unBlockScreen();


                //glyphicon-folder-close

                getFilesByDir(dir);
                
                
              },

              error:  function ( response ){
                alert( response );
              }
                
        }); 

$.ajax({
              url: Routes.getInfoDirRoot(),
              dataType: "html",
              method: "POST",              
              data: data,
              success: function( response ){                                                      
                           
                $( '.directorios-target' ).html();       
                                                              
                $( '.directorios-target' ).append(response);  

                $( 'button.importar').show();     

                setTimeout(5000);
                
                //sfBIMG.unBlockScreen();
                                                      
              },

              error:  function ( response ){
                alert( response );
              }
                
        }); 


$.ajax({
      url: Routes.getThumb(),
      dataType: "html",
      method: "GET",              
      data: data_thumb,
      success: function( response ){      
        //$( '.processing-bar' ).remove();
        setTimeout(5000);
        //sfBIMG.unBlockScreen();
        $( '.content-import' ).append(response);

        var html = "<div class='row'>";
        html = "<div class='col-md-8'>";
        html += "<a href=''>Ver archivos</a>";
        html += "</div>";
        html += "</div>";

        $( html ).insertBefore('.files');

        $('.files').hide("slow");
        //$( this ).attr("disabled", true );
      },
      error:  function ( response ){
        alert( "Error al procesar" );
        //sfBIMG.unBlockScreen();
        return false;

      }

    }); 

$.ajax({
              url: Routes.getFilesByDir(),
              dataType: "html",
              method: "POST",              
              data: data,
              success: function( response ){                
                $( '.files' ).remove();                                                            
                $( '.content-import' ).append(response);                
                                            
              },

              error:  function ( response ){
                alert( response );
              }
                
  }); 


$.ajax({
      url:  Routes.getImportFiles(),
      dataType: "html",
      method: "GET",              
      data: data_import,
      success: function( response ){      
        $( '.processing-bar' ).remove();
        
        $( this ).attr("disabled", true );
        getRemove();
      },
      error:  function ( response ){
        alert( response );
        $( this ).attr("disabled", false );
      }
    });


                                      