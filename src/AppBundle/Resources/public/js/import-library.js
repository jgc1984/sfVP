  $( document ).on('click', '.glyphicon-play-circle', function ( event ){
   event.preventDefault();
   $('.player').show();
   var vlc = document.getElementById("vlc");
   var file = $( this ).parent().parent().find('input[type=hidden]').val();

   if( vlc.input.state == 3 ){
    alert("se dentrna la reporduccion del video");
    vlc.playlist.stop();
    vlc.playlist.items.clear();
  }

  var param = "file:///"+file;
  vlc.playlist.add(param);        

  vlc.playlist.play();

});




$( document ).keypress( function( e ) {

    if ( e.which == 13 ) {
      var searchtxt = $('#root').val();
      if( searchtxt !=" "){
        e.preventDefault();
        var level = $('input[name=level]').val();
        //callAjax: function ( url, format, methodRequest, dataInfo, handleresponse )
        var url = "{{ path('get_info_dir_root') }}";
        alert(searchtxt);

        var data = "dir="+searchtxt+"&level="+level;
        //sfBIMG.callAjax( url, "html", "GET", data, "test" );
        $.ajax({
          url: url,
          dataType: "html",
          method: "GET",              
          data: data,
          success: function( response ){
            $('.content').append(response);
          },
          error:  function ( response ){
            alert( response );
          }

        }); 

        $( this ).attr("disabled", true);

      }
    }
  });



  $( document ).on('click', 'button.reset', function ( event ){
    $( '.content' ).html("");

  });


  $( document ).on('click', '.add_folder', function ( event ){
    event.preventDefault();      
    $('p.new').show("slow");
    $( this ).hide();
  });
   
$( document ).on('click', 'a.search_root', function ( event ){
    event.preventDefault();
    var rootDir = $( this ).parent().find('input').val();
    var level = $( this ).parent().find('input[name=level]').val();
        //callAjax: function ( url, format, methodRequest, dataInfo, handleresponse )        
        var data = "dir="+rootDir+"&level="+level;
        //sfBIMG.callAjax( url, "html", "GET", data, "test" );
        $.ajax({
          url: Routes.getInfoDirRoot(),
          dataType: "html",
          method: "POST",              
          data: data,
          success: function( response ){            
            $('.content').append( response );
            
          },
          error:  function ( response ){
            alert( response );
          }

        }); 

        $( this ).attr("disabled", true);
        
});


$( document ).on('change', 'select', function (){            
        var dir = $( this ).find('option:selected').val();              

        var data = "dir="+encodeURIComponent(dir);
        
        sfBIMG.blockScreen( Routes.getGif(), "Procesando");

        $.ajax({
              url: Routes.getInfoDirRoot(),
              dataType: "html",
              method: "POST",              
              data: data,
              success: function( response ){                
                $( 'select[name=root]' ).find("option:selected").addClass("glyphicon glyphicon-folder-open");  
                var padre = $( 'select[name=root]' ).parent();
                
                $( ".files" ).remove();    

                $( ".player" ).remove();

                $( '.folders' ).append(response);                

                setTimeout(5000);
                sfBIMG.unBlockScreen();
                
                
              },

              error:  function ( response ){
                alert( response );
              }
                
        }); 

        $( this ).attr("disabled", true);
        
});


$( document ).on('click', '.analizar', function ( event ){
    event.preventDefault();        
       
    //var listaarchivos = $( 'ul.files-found li' ); 
    var listaarchivos = $( '.files ul li' ); 

    /*
    var imgInfo = {
      parrafo: "Procesando",
      img: "{{ asset('progress-bar.gif')}}"
    }      
    var containerBar = $('#template-barra').html();  
    var template = Handlebars.compile(containerBar);            
    var templateContent = template(imgInfo);                   
    $('.files').append(templateContent);  
    */    

    var ist = getFilesToAnalized(listaarchivos);  
    
    var data_thumb = "files="+JSON.stringify(ist);

    $('.files-analized').remove();

    //$('.folders').hide("slow");    

    sfBIMG.blockScreen( Routes.getGif(), "Procesando" );

    $.ajax({
      url: Routes.getThumb(),
      dataType: "html",
      method: "GET",              
      data: data_thumb,
      success: function( response ){      
        //$( '.processing-bar' ).remove();
        setTimeout(5000);
        sfBIMG.unBlockScreen();
        $( '.content' ).append(response);
        
        $( this ).attr("disabled", true );
      },
      error:  function ( response ){
        alert( "Error al procesar" );
        sfBIMG.unBlockScreen();
        return false;

      }

    }); 


  });


  $( document ).on('click', '.back', function ( event ){
    event.preventDefault();   
    var select = $('select:last');    
    $( select ).parent().parent().remove();
    var sel = $('select:last');    
    $( sel ).attr("disabled", false );
    $('.files').remove();
  });



  $( document ).on('click', 'a.analized li', function ( event ){    
    event.preventDefault();
    $( this ).addClass("list-group-item-success");
    $( this ).find("input[type=checkbox]").attr("value", true);
  });

  
  $( document ).on('click', '.importar', function ( event ){    
    console.log("ok");
    event.preventDefault();
    
    var filesAnalized = $( '.analized li' ); 

    var imgInfo = {
      parrafo: "Procesando",
      img: Routes.getGif()
    }

    var artista = $('input[name=artista]').val();  

    if( typeof artista === "undefined"){
      artista = null;
    }  

    var fuente = $('input[name=fuente]').val();

     if( typeof fuente === "undefined"){
      fuente = null;
    }  

    var genero = $('input[name=genero]').val();

    if( typeof genero === "undefined"){
      genero = null;
    }  

    var dataExtra = {
      artista: artista,
      fuente: fuente,
      genero: genero
    }  
    

    var filesImport = JSON.stringify( getFilesToImport( filesAnalized ) ); 

    var extra = JSON.stringify(dataExtra);
    
    var data_import = "files="+filesImport+"&extra="+extra;
    
    $.ajax({
      url:  Routes.getImportFiles(),
      dataType: "html",
      method: "GET",              
      data: data_import,
      success: function( response ){      
        $( '.processing-bar' ).remove();
        
        $( this ).attr("disabled", true );
        getRemove();
      },
      error:  function ( response ){
        alert( response );
        $( this ).attr("disabled", false );
      }
    });

  });

  $( document ).on('click', 'a.fa-remove', function ( event ){    
    event.preventDefault();
    alert("algo");
  });



  function getFilesToAnalized( lista ){
    var archivos = [];

    $.each( $( lista ), function ( index, element ) {
      
      if( $( element ).find('input[name=file]').is(':checked') ) {
        var file = $( element ).find('input[type=hidden]').val();
        
        var fileImport = {
          filename: encodeURIComponent(file)

        }


        archivos.push(fileImport);

        //$( element ).remove();
        $( element ).addClass("list-group-item-success");
        $( element ).addClass("analized-success");
        //$( element ).find('input[type=checkbox]').attr("disabled", true );

      }
    }); 

    return archivos;
  }


  


  




