/* REPRODUCE LOS SLIDERS CORRESPONDIENTES AL GENERO*/
$( document ).on('click', 'button.play', function () {    
            
      var fileData =   $( this ).parent();
      var list     =   $( fileData ).parent();
      
      folder       =   $( fileData ).find('input[name="path"]').val(); 
      file         =   $( fileData ).find('input[name="nombre_archivo"]').val(); 
      genero       =   $( fileData ).find('input[name="genero"]').val();             
      position     =   $( fileData ).find('input[name="position"]').val(); 


      $('head title').html(file);                    
      $('.player').show();
                                                                  
      var duracionTotal = 0;

      var fileInfo      = { file: file,  folder: folder, position: position   }                    

      var header = $('#template-content').html();  
      var templateHeader = Handlebars.compile(header);            
      var templateContent = templateHeader(fileInfo);                   
      
      $('.contenido').html(templateContent);
      
      var vlc =  document.getElementById('vlc');
      var fileCurrent = "file:///"+ folder + file;  
      
      vlc.playlist.add(fileCurrent);         
      
      var tracks = sfBIMG.build2( fileData );        
      
      var sourceSideRelated = $('#template-tracks-genero').html();

      var templateSideRelated = Handlebars.compile(sourceSideRelated);            
      
      var template = templateSideRelated(tracks); 
      
      var req = new AjaxHTTPRequest(Routes.getPlaylists(), "html", "POST", null, sfBIMG.getPlaylists); 

      req.callAjax();          
      
      $('.content_right').html(template);

      $('.20-30').hide();

      $('.30-40').hide();

      $('.40-50').hide();
      
      
      vlc.playlist.play(); 
});

$( document ).on('click', 'input[name=playlist]', function () {  
    if( $( this ).is(':checked') ) {
      track = $('input[name="track_current"]').val(); 
      playlistID = $( this ).parent().find('input[name=playlist_id]').val(); 

      var data = "track="+track+"&playlistID="+playlistID;
      var req = new AjaxHTTPRequest(Routes.getAddTrackToPlaylist(), "html", "POST", data, sfBIMG.addTrackToPlaylist); 

      req.callAjax(); 
            
      $( this ).attr("disabled", true ); 
    }
    
});





