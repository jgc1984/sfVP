$( document ).on('click', '.play-playlist', function ( event ) {
    event.preventDefault();

    $('.gener').hide();  

    playlist = $( this ).parent().find('input[name="playlist"]').val();  

    var data = "padre="+playlist;    
       
    var req = new AjaxHTTPRequest(Routes.getTracksByPlaylist(), "html", "POST", data, sfBIMG.getTracksByPlaylist); 

    req.callAjax();      
                
    var elementLi =  $( this ).parent().parent();    

    $.each( elementLi.parent().find('li'), function( index, element ){        
        if( $( element ).hasClass("playlist-info-header") ){
            $( element ).removeClass("playlist-info-header");
            $( element ).find('i:last').remove();            
        }

    });

    //elementLi.addClass("list-group-item-info");
    elementLi.addClass("playlist-info-header");
    
    $("<i class='glyphicon glyphicon-volume-up'></i> ").insertAfter( $( this ) );

    $( this ).parent().parent().find('i').removeClass("fa-play-circle-o");
    
        
    var header = $('#template-player').html();  
    var templateHeader = Handlebars.compile(header);            
    var templateContent = templateHeader();                   
    $('.contenido').html(templateContent);                              
                     
});

$( document ).on('click', '.duracion', function ( event ) {
    playlist = $( this ).parent().find('input[name="playlist"]').val();

    var data = "playlist="+playlist;
    
    var req = new AjaxHTTPRequest(Routes.getTimeFiles(), "json", "POST", data, sfBIMG.getTimeFiles); 

    req.callAjax();                  

     
});  


$( document ).on('click', 'button.playlists', function ( event ) {
    event.preventDefault();

    var req = new AjaxHTTPRequest(Routes.getPlaylists(), "html", "POST", null, sfBIMG.getPlaylistsOnSearch); 

    req.callAjax(); 
    
});


$( document ).on('click', '.artistas button', function () {     

    artista = $( this ).parent().find('input[name="artista"]').val();      

    var data = "artista="+artista;    
       
    var req = new AjaxHTTPRequest(Routes.getTracksByArtista(), "html", "POST", data, sfBIMG.loadTracksByArtista); 

    req.callAjax();                          

});



$( document ).on( 'click', 'button.guardar_playlist', function( event ){  
    event.preventDefault();       
    var playlistNew =  $( this ).parent().parent().find('input[name=playlist_new]').val();    
    var tituloCurrentTrack =  $( 'input[name=track_current]' ).val();
    var data = 'playlist='+playlistNew+'&track='+tituloCurrentTrack;
    var req = new AjaxHTTPRequest(Routes.getSavePlaylist(), "json", "POST", data, sfBIMG.savePlaylist()); 

    req.callAjax();   


});



$( document ).on('click', '.addTrackToEnquee', function (){
    var track = $( this).parent().find('input[name=pathFull]').val();
    var title = $( this).parent().find('input[name=title]').val();
    var position = $( this).parent().find('input[name=position]').val();
    var filename = $( this).parent().find('input[name=nombre_archivo]').val();
    
    Lockr.sadd("track", { titulo: title, path: track, position: position, nombre_archivo: filename });    
    Lockr.smembers("track");    
    
    var html = "<li class='list-group-item'>"+title+"</li>";
    var html2 = "<div class='alert alert-success' role='alert'> <p><i class='fa fa-check'>"+title+"</i></p> </div>";
    $( html ).insertBefore('.tracks-enquee li:last');  
    $( html2 ).insertAfter('.tracks-by-genero'); 
    
    var trackNew = { titulo: title, path: track, position: position, nombre_archivo: filename };
    
    playlistEnquee.addTrack(trackNew);  

    console.log(playlistEnquee.getName());

    //$('.tracks-enquee').show("slow");    

    
});


$( document ).on('click', '.youtube-founded li img', function ( event ) {
    event.preventDefault();
    
    var url = $( this ).parent().parent().find('input[name=url]').val();
    var title = $( this ).parent().parent().find('input[name=title]').val();
    var trac = {
        title: title,
        url: url
    }

    
    sourceYoutube = $('#template-youtube').html();  
    
    templateYoutube = Handlebars.compile(sourceYoutube);            

    template = templateYoutube(trac);          
    //$(template).insertBefore('.sliders');            
    $('.content-sliders').html(template);
        
});

$( document ).on('click', 'button.play-search-pc', function (){
    var tracksOnMemory = Lockr.getAll();

    console.log(tracksOnMemory);
    
    var path = $( this ).parent().parent().find('input[name=pathFull]').val();  
    var title = $( this ).parent().parent().find('input[name=title]').val();  
    var trackID = $( this ).parent().parent().find('input[name=trackID]').val(); 
    var nombre_archivo = $( this ).parent().parent().find('input[name=nombre_archivo]').val();  

    var fileInfo = {
      path: path,
      title: title,
      nombre_archivo: nombre_archivo

    }

    var header = $('#template-player').html();  
    var templateHeader = Handlebars.compile(header);            
    var templateContent = templateHeader(title);                   
    $('.contenido').html(templateContent);    
    
    var file = path + nombre_archivo;
    vlc =  document.getElementById('vlc'); 
    var fileCurrent = "file:///"+ file;  
      
    vlc.playlist.add(fileCurrent);  
    vlc.playlist.play();

    var data = "title="+title+"&track="+trackID;      
    
    var req = new AjaxHTTPRequest(Routes.getTracksRelated(), "html", "POST", data, sfBIMG.getTracksRelated); 

    req.callAjax();  
});

$( document ).on('click','.thumbnails .editar', function ( event ) {
    
    event.preventDefault();
        
    $('#vlc').hide("slow");

    var li = $( this ).parent().parent();

    artista = $( li ).find('input[type=hidden]').val();

    $('#library-import').remove();

    var data = 'artista='+encodeURIComponent(artista);

    var req = new AjaxHTTPRequest(Routes.getThumbnailsNews(), "html", "POST", data, sfBIMG.getThumbnails); 

    req.callAjax();  

    
});



$( document ).on('click','.guardar_thumbnail', function ( event ) {
    event.preventDefault();
    var thumbnail;
    
    $.each( $('.thumbnails').find('li') , function( key, value ) {
        if( $( value ).find('input[name=thumb]').is(':checked') ) {
           thumbnail = $( value ).find('input[name=thumbnail]').val();            
           
        }

    });
    

    var artista = $('input[name=artista]').val();

    var data = 'thumbnail='+encodeURIComponent(thumbnail)+'&artista='+artista;

    var req = new AjaxHTTPRequest(Routes.getThumbnailsByArtista(), "json", "POST", data, sfBIMG.saveThumbnailArtist); 

    req.callAjax();  
    
});


/* FUNCIONES VARIAS */

function search( source ){
  var place;
  $.each( source, function( index, element ){
     if( $( element ).is(':checked') ) {
        place = $( this ).val(); 

     } 
  });
   
  return place;

}

function getResultsByAjax( title, place ){
  
  $.ajax({
         url: Routes.getTracksRelated(),
         dataType: "json",
         method: "post",
         data: {
           term: title,
           place: place,
           class: "{{class}}",
           property: "{{property}}",
           search_method: "{{search_method}}"
         },

         success: function( data ) {            
            sourceTemplateYoutube = $('#template-tracks-youtube').html();  
            templateYoutube = Handlebars.compile(sourceTemplateYoutube);            
            template = templateYoutube(data);          
            $(template).insertBefore('.sliders');            
         }        
  });
}

function getResultsYoutubeByAjax( title, place ){
  
  $.ajax({
         url: Routes.getTracksRelated(),
         dataType: "json",
         method: "post",
         data: {
           term: title,
           place: place,
 
         },

         success: function( data ) {            
            sourceTemplateYoutube = $('#template-tracks-youtube').html();  
            templateYoutube = Handlebars.compile(sourceTemplateYoutube);            
            template = templateYoutube(data);          
            $(template).insertBefore('.sliders');            
         }        
  });
}


function build( list ){
    
    var tracksByGenero = []; 
    
    
    if( $( list ).children().size() ){
        
        $( list ).find('li').each(function( key, element ) {  
            console.log( element );              
    
            if( key <= 10){
                folder = $( element ).find('input[name="path"]').val(); 
                file = $( element ).find('input[name="nombre_archivo"]').val(); 
                duracion = $( element ).find('input[name="duracion"]').val(); 

                var genero = $( element ).find('input[name="genero"]').val(); 

                if( typeof genero === "undefined" ){
                    var genero = null;
                }
            
                if( file !== fileCurrent ){
                        var fileCurrent = "file:///"+ folder + file;          
                        vlc.playlist.add(fileCurrent);                

                        tracksByGenero.push( {
                              titulo:  file,
                              nombre: file,
                              path:   folder, 
                              duracion: duracion,
                              genero: genero
                        });            
                }     
                            
            }
                                                
        }); 
    }
    

    return tracksByGenero;
}


