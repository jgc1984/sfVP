<?php

namespace AppBundle\Services;

use AppBundle\Entity\Track;
use AppBundle\lib\TreeView;
use Exception;
use FFMpeg\FFProbe;
use Symfony\Component\Finder\Finder;
use PHPVideoToolkit\ImageFormat_Jpeg;
use PHPVideoToolkit\Video;
use PHPVideoToolkit\Timecode;
use FFMpeg\FFMpeg;
use FFMpeg\Format\Video\X264;
use FFMpeg\Format\Video\WebM;
use FFMpeg\Format\Video\Ogg;
use PHPVideoToolkit\VideoFormat_Mp4;
use PHPVideoToolkit\VideoFormat_H264;
use PHPVideoToolkit\VideoFormat_Webm;


//$get = require __DIR__.'../../lib/getID.php';
//$get3 = require __DIR__.'../../lib/getID3/getid3/getid3.lib.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppManager
 *
 * @author gabriel
 */
class AppManager {

    protected $container;

    //const RUTA = "/home/gabriel/Descargas";
    //const RUTAV = "/media/gabriel/DATOS/VIDEOS/ROCK";
    const LINUX_PATH = "/media/";
    const WINDOWS_PATH="D: \ ";
    const DIR_TMP = "/tmp/";

    protected $formatoVideo = array('vob', 'VOB', 'avi', 'mpg', 'mpeg', 'webm', 'mp4', 'flv');
    protected $formatoAudio = array('ape', 'flac', 'wav', 'ogg', 'mp3');
    protected $formatosAvailable = array('vob', 'mpeg', 'mp4');

    public function __construct($container) {
        $this->container = $container;
    }

    /*
     * Devuelve un listado de  activos 
     */

    public function getListFiles($filtros, $split = true, $onlyFiles = false ) {

        
        $finder = new Finder();
        $finder->files()->in(isset($filtros['path']) ? $filtros['path'] : $filtros);              
        $finder->files()->name('*.vob');
        $finder->files()->name('*.mp4');
        $finder->files()->name('*.mpeg');
        $finder->files()->name('*.mpg');
                        
        $response = array('count'=>0, 'files'=>0, 'status'=> '', 'message'=> '');

        foreach ($finder as $file) {
            
            $localTrack = array();

                if ( $split ){

                    $localTracks = $this->splitInformation( $file );
                } else {
                    if ( $onlyFiles ){
                        $localTrack['fileName'] = $file->getRelativePathname();
                    } else {
                        $localTrack['path'] = $file->getRealpath();
                        $localTrack['fileName'] = $file->getRelativePathname();
                        $localTrack['titulo'] = $file->getRelativePathname();
                        $localTrack['size'] = filesize($file->getRealpath()) . PHP_EOL;
                        $localTrack['size'] = $this->formatSizeUnits($localTrack['size']); 
                        $localTrack['formato'] = pathinfo( $localTrack['path'], PATHINFO_EXTENSION );  
                    }
                    
                    $localTracks[]=$localTrack;                    
                }
                                                
        }
        
        return $localTracks;
    }


    public function getFilesPlane( $pathreal, $onlySelect = false , $name = null){

        $path = urldecode( $pathreal );

        $response = array();

        $pathUrl = array();

        $oldFolder = ""; 

                        
        if ( $onlySelect ){            
            $div = "<div class='col-md-2'>";
                
            $tree = new TreeView( $path );

            $html = $tree->create_tree( $name );                
            $div .= $html;                         
            $div .= "</div>";
        
            $response['html'] = htmlentities($div);
            $response['status'] = "OK";    

        } else {            
            $tree = new TreeView( $path );
            $html = $tree->create_tree( );
                         
            $response['html'] = htmlentities($html);
            $response['status'] = "OK";    
        }
                    
        
        
        return $response;

    }

    private function saveFiles($localTracks, $target, $simple = true ) {
        $response = array();
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getManager();

        if ( count( $localTracks ) > 0 ){

            if( $simple ){
                foreach ($localTracks as $file) {
                    $pista = new Track();
                    $pista->setTitulo($file['fileName']);                    
                    $pista->setNombreArchivo($file['fileName']);  

                    $target = $this->fixPath( $target);

                    $pista->setPath( $target );
                    
                    $artista = $em->getRepository('AppBundle:Artista')->findOneBy(array('nombre'=> $file['artista']));

                    $fuente = $em->getRepository('AppBundle:Fuente')->findOneBy(array('nombre'=> $file['fuente']));
                    $pista->setArtista($artista);
                    $pista->setFuente($fuente);

                    //'FOLKLORE', 1 => 'POP', 2 =>'SMOOTH JAZZ', 3 => 'TRIP HOP', 4 => 'ALTERNATIVO', 5 => 'TRIP ROCK', 6 => 'ROCK', 7 => 'BLUES', 8 => 'HARD ROCK', 9 =>'HEAVY METAL', 10=> 'LATIN'
                    switch ( $file['genero'] ) {
                        case 'FOLKLORE':
                            $pista->setGenero(0);                            
                            break;
                        case 'POP':
                            $pista->setGenero(1);                            
                            break;    
                        case 'SMOOTH JAZZ':
                            $pista->setGenero(2);                            
                            break;  
                        case 'TRIP HOP':
                            $pista->setGenero(3);                            
                            break;  
                        case 'ALTERNATIVO':
                            $pista->setGenero(4);                            
                            break; 
                        case 'ROCK':
                            $pista->setGenero(6);                            
                            break; 
                        case 'BLUES':
                            $pista->setGenero(7);                            
                            break; 
                        case 'HARD ROCK':
                            $pista->setGenero(8);                            
                            break; 
                        case 'HEAVY METAL':
                            $pista->setGenero(9);                            
                            break;
                        case 'LATIN':
                            $pista->setGenero(10);                            
                            break;         

                                                
                    }
                    
                    
                    $em->persist($pista);
                }

                try {
                    $em->flush();                    
                    $response['status'] = "ok";
                    $response['countFiles'] = count($localTracks);

                } catch (Exception $exc) {
                    $response['status'] = "error";
                    $response['message'] = $exc->getMessage();
                    $exc->getMessage();
                }        
            }else{
                foreach ($localTracks as $file) {
                    $pista = new Track();
                    $pista->setTitulo($file['fileName']);
                    $pista->setNombreArchivo($file['fileName']);                    
                    $pista->setPath($file['path']);
                    if (isset($file['dvdOriginal'])) {
                        $pista->setDvdOriginal($file['dvdOriginal']);
                    }
                    $em->persist($pista);
                }

                try {
                    $em->flush();
                    $response['status'] = "ok";
                    $response['countFiles'] = count($localTracks);
                } catch (Exception $exc) {
                    $response['status'] = "error";
                    $response['message'] = $exc->getMessage();
                    $exc->getMessage();
                }        
            }
            
        }
        

        return $response;
    }

    public function importFiles( $tracks, $dirTarget ) {
        //$archivos = $this->getListFiles($filtros);
        $test = explode('/', $dirTarget);
        $res = array();
        foreach ($tracks as $track) {
            if( file_exists( $track ) ){
                if( is_dir( $dirTarget ) ){                    
                    $target = $dirTarget ."/". basename( $track );
                    $fileNew = basename( $track );
                    
                    if ( copy( $track, $target ) ) {                        
                            $filesImported['fileName'] = basename( $track );
                            $filesImported['path'] = $target ;
                            $filesImported['genero'] = $test['5'] ;
                            $filesImported['artista'] = $test['6'] ;
                            $filesImported['anio'] = $test['7'] ;
                            $filesImported['fuente'] = $test['8'] ;
                            $format = pathinfo($target, PATHINFO_EXTENSION);
                            
                            if ( $format == "mpg"){
                                /*$ffmpeg = FFMpeg::create();
                                $video = $ffmpeg->open($target);            
                                $video->save(new WebM(), '/tmp/export-x264.webm');
                                */
                                $fileNew .= ".vob";

                                $dirConver = "/home/gabriel/Vídeos/";

                                $targetTemp = $dirConver.$filesImported['genero'];

                                $newTarget = $targetTemp. "/".$fileNew;

                                $hasSpace = strrpos($track," ");

                                if( $hasSpace ){
                                                                                                
                                    $track = preg_replace('/\s+/', '\ ', $track);                                    
                                    $newTarget = preg_replace('/\s+/', '\ ', $newTarget);
                                 
                                }
                                                                
                                if( file_exists( $targetTemp ) && is_dir ( $targetTemp ) ){                                    
                                    $command = "ffmpeg -i ".$track." -af 'volume=8' -acodec copy -acodec copy  -target pal-dvd ".$newTarget;                                    
                                                                
                                } else {
                                    mkdir( $targetTemp );                            
                                    $command = "ffmpeg -i ".$track." -af 'volume=8' -acodec copy -acodec copy  -target pal-dvd ".$newTarget;
                                
                                }

                                
                                $salida = shell_exec($command);
                                
                                if ( $salida ){
                                    unlink( $track );
                                }
                            }                    
                            unlink( $track );
                            $res[] = $filesImported;
                        
                    } 
                }
            }             
        }

        
        $files = $this->saveFiles( $res, $dirTarget );

        $response = array_merge( $res, $files );
        
        return $response;
    }




    public function procesarImagen() {
        if ($uploaded_file) {
            //Sube la imagen
            $picture = AlumnoType::processImage($uploaded_file, $alumno);

            //setea el tamaño del recorte
            $targ_w = $request->request->get('w');
            $targ_h = $request->request->get('h');

            if ($targ_h && $targ_w) {
                if (null !== $alumno->getFoto()) {
                    $file = $alumno->getFoto();
                    @unlink($file);
                } else {
                    $file = $picture;
                }
                $file = $this->get('kernel')->getRootDir() . '/../web/foto/' . $picture;

                $img_r = imagecreatefromjpeg($file);
                $dst_r = ImageCreateTrueColor($targ_w, $targ_h);

                imagecopyresampled($dst_r, $img_r, 0, 0, $request->request->get('x'), $request->request->get('y'), $targ_w, $targ_h, $request->request->get('w'), $request->request->get('h'));
                imagejpeg($dst_r, $file, 90);
            }


            $alumno->setFoto($picture);
        }
    }

    public function checkUSB($param) {
        if (file_exists($param)) {
            return true;
        } else {
            return false;
        }
    }

    private function getDuration($file) {
        if (file_exists($file)) {
            ## open and read video file
            $handle = fopen($file, "r");
## read video file size
            $contents = fread($handle, filesize($file));
            fclose($handle);
            $make_hexa = hexdec(bin2hex(substr($contents, strlen($contents) - 3)));
            if (strlen($contents) > $make_hexa) {
                $pre_duration = hexdec(bin2hex(substr($contents, strlen($contents) - $make_hexa, 3)));
                $post_duration = $pre_duration / 1000;
                $timehours = $post_duration / 3600;
                $timeminutes = ($post_duration % 3600) / 60;
                $timeseconds = ($post_duration % 3600) % 60;
                $timehours = explode(".", $timehours);
                $timeminutes = explode(".", $timeminutes);
                $timeseconds = explode(".", $timeseconds);
                $duration = $timehours[0] . ":" . $timeminutes[0] . ":" . $timeseconds[0];
            }
            return $duration;
        } else {
            return false;
        }
    }
    
    public function getLastList(){
        $playListRepository = $this->container->get('doctrine')->getRepository('AppBundle:Playlist');
        $listas = $playListRepository->getLastList();
        $listUltima = array('most'=>"00:00:00");
        
        foreach($listas as $lista){
            
            $new = $lista['updated']->format('H:i:s');
            if($new > $listUltima['most']){
                $listUltima['most']= $new;
                $listUltima['lista']=$lista;                
            }
        }
        
        $last = $this->container->get('doctrine')->getRepository('AppBundle:Playlist')->findOneBy(array('id' => $listUltima['lista']['id']));
        
        return $last;
        
        
    }

    public function configureSearchTrack($filtros){

    }

    public function listarTracks($filtros) {        
        
        $trackRepository = $this->container->get('doctrine')->getRepository('AppBundle:Track');
        $aTracks = $trackRepository->getListarTracks($filtros);
        
        return $aTracks;
    }

    public function getTracksMost() {
        $trackRepository = $this->container->get('doctrine')->getRepository('AppBundle:Track');
        $aTracksMost = $trackRepository->getTracksMost();
        return $aTracksMost;
    }

    public function getTracksByGenero( $genero ) {
        $trackRepository = $this->container->get('doctrine')->getRepository('AppBundle:Track');
        $aTracksGenero = $trackRepository->getTracksByGenero( $genero );
        return $aTracksGenero;
    }

    
    public function getTracksByArtista( $artista ) {
        $trackRepository = $this->container->get('doctrine')->getRepository('AppBundle:Track');
        $aTracksArtista = $trackRepository->getTracksByArtista( $artista );
        return $aTracksArtista;
    }


    public function getTracksCurrently() {
        $trackRepository = $this->container->get('doctrine')->getRepository('AppBundle:Track');
        $aTracksCurrently = $trackRepository->getTracksCurrently();
        return $aTracksCurrently;
    }

    public function getTracksFavorites() {
        $trackRepository = $this->container->get('doctrine')->getRepository('AppBundle:Track');
        $aTracksFavorites = $trackRepository->getTracksFavorites();
        return $aTracksFavorites;
    }


    public function filtrarDatos( $filtros ) {
        $trackRepository = $this->container->get('doctrine')->getRepository('AppBundle:Track');
        $aFiltros = $trackRepository->filtrar( $filtros );
        
        return $aFiltros;
    }

    public function getTopTracks() {
        $trackRepository = $this->container->get('doctrine')->getRepository('AppBundle:Track');
        $aTopTracks = $trackRepository->getTopTracks();
        
        return $aTopTracks;
    }


    public function getTracksByPlaylist($playlist) {
        $tracksRepository = $this->container->get('doctrine')->getRepository('AppBundle:PlaylistTrack');
        $aTracks = $tracksRepository->getTracksByPlaylist($playlist);
        
        return $aTracks;
    }

    public function getInfoByPlaylist($playlist) {
        $tracksRepository = $this->container->get('doctrine')->getRepository('AppBundle:PlaylistTrack');
        $aInfo = $tracksRepository->getInfoByPlaylist($playlist);
        
        return $aInfo;
    }

    public function getGeneros(){

        $generosArr = array();
        $generoRepository = $this->container->get('doctrine')->getRepository('AppBundle:Genero');
        $generos = $generoRepository->findAll();
        if( count( $generos ) > 0 ){
            foreach ($generos as $genero) {
                $cntGeneros = $this->getTracksByGenero( $genero->getNombre() );
                
                if( count( $cntGeneros ) > 0 ){
                    $generosData['nombre'] = $genero->getNombre();  
                    $generosData['tracks'] = $this->getTracksByGenero( $genero->getNombre() );                      
                    $generosArr[]= $generosData;
                }
                
            }
        }      
       
        return $generosArr;

    }

    private function formatSizeUnits( $bytes){        

        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;  
    } 

    
    private function splitInformation( $file ){
        $track = explode('-', $file->getRelativePathname());

            $trackRepository = $this->container->get('doctrine')->getRepository('AppBundle:Track');

            $pathMedia = $file->getRealpath();

            $localTrack['path'] = $pathMedia;

            $trackLocal = $trackRepository->findOneBy(array('path' => $pathMedia));


            if (!$trackLocal) {

                if (isset($track[0])) {
                    if (is_numeric($track[0])) {
                        $localTrack['anio'] = $track[0];
                    } else {
                        $localTrack['anio'] = "no data";
                    }
                }

                if (isset($track[1])) {
                    $localTrack['artista'] = $track[1];
                } else {
                    $localTrack['artista'] = "sin info";
                }

                if (isset($track[2])) {
                    $localTrack['place'] = $track[2];
                } else {
                    $localTrack['place'] = "sin info";
                }

                if (isset($track[3])) {
                    $localTrack['titulo'] = $track[3];
                } else {
                    $localTrack['titulo'] = "sin info";
                }

                $localTrack['fileName'] = $file->getFilename();

                $ext = pathinfo($file->getFilename(), PATHINFO_EXTENSION);

                $localTrack['formato'] = $ext;

                $localTrack['size'] = filesize($file->getRealpath()) . PHP_EOL;

                $localTrack['size'] = $this->formatSizeUnits($localTrack['size']);

                         

                if (in_array($ext, $this->formatoVideo)) {
                    $localTrack['dvdOriginal'] = "test";
                    //$localTracks[] = $localTrack;
                }
                
            }
        return $localTrack;
    }

    private function fixPath( $file ){
        $char = substr( $file, -1);

        //var_dump($char);
        if ( $char != "/") $file .= "/";

        //var_dump($har);

        return $file;    
    }

    public function getInfoDir( $dir, $subDirs = array() ){
        /* Realiza una busqueda no recursiva */
        $finder = new Finder();

        if( file_exists( $dir ) ){
            $finder->directories()->in($dir);
            $finder->depth('== 0');        

            foreach ($finder as $file) {
                $dirInfo['nombre'] = $file->getRelativePathname();
                $dirInfo['path'] = $file->getRealpath(); 

                /*$genero = $this->container->get('doctrine')->getRepository('AppBundle:Genero')->findOneBy(array('nombre' => $dirInfo['nombre']) );

                if( $genero ){
                    $dirInfo['genero'] = $genero;
                }  


                $fuente = $this->container->get('doctrine')->getRepository('AppBundle:Fuente')->findOneBy(array('nombre' => $dirInfo['fuente'] ) );  

                if( $fuente ){
                    $dirInfo['fuente'] = $fuente;
                } 

                $artista = $this->container->get('doctrine')->getRepository('AppBundle:Artista')->findOneBy(array('nombre' => $dirInfo['artista'] ));  

                if( $artista ){
                    $dirInfo['artista'] = $artista;
                }*/  
                $subDirs[] = $dirInfo;                   
            }
        }

        
        return $subDirs;
    }

    public function getFilesByDir( $dir, $infos = array() ){

        $em = $this->container->get('doctrine')->getManager();            

        $files = new Finder();
        
        $files->files()->in($dir);
        $files->depth('== 0')
                       ->name('*.mpg')
                       ->name('*.mpeg')
                       ->name('*.VOB')
                       ->name('*.vob')
                       ->name('*.avi')
                       ->name('*.webm')
                       ->name('*.ts')
                       ->name('*.mp4')
                       ->name('*.mkv'); 
        
        $files->files()->size('>= 5K');                          

        foreach ($files as $file) {            
            $dirInfo['nombre'] = $file->getRelativePathname();            
            $dirInfo['path'] = $file->getRealpath(); 
            $dirInfo['registered'] =  false;
                        
            $entity = $em->getRepository('AppBundle:Track')->findOneBy(array('nombre_archivo' => $dirInfo['nombre']));                
            if( !is_null( $entity ) ){                
                if( $dirInfo['nombre'] == $entity->getNombreArchivo() ){                    
                    $dirInfo['registered'] =  true;    
                }
                            
            }   
            
            $infos[] = $dirInfo;                   
        }
        
        return $infos;
    }

    
}

