<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class FindTrackType extends AbstractType
{
    
    private $tracks;

    public function __contruct( $tracks ){
      $this->tracks = $tracks;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tracks = $this->tracks;
        
        $builder                                  
            ->add('fileTracks', 'choice', array(
                              'choices'  => $tracks,
                              'required' => false,
                              'attr'=> array('class'=> 'form-control warning')
                  ))
            
           
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null
        ));
    }
    
    

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_findfiles';
    }
}
