<?php

namespace AppBundle\Form\filters;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FilterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('target',  new TargetType(), array(
                                    'empty_value'=> 'Elejir un target',                                                                 
                                    )
                        )
                 ->add('value', 'text', array(
                                    'required'=> false,                                                                 
                                    'trim'=> true
                                    ))      
                 ->add('range_from', 'text', array(
                                    'required'=> false,                                                                 
                                    'trim'=> true
                                    ))                                
                 ->add('range_to', 'text', array(
                                    'required'=> false,                                                                 
                                    'trim'=> true
                                    )) 
                           ;                        
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
       $resolver->setDefaults(array(     
            'cascade_validation' => true,
            'csrf_protection'   => false                      
        ));
    }

    public function getName()
    {
        return 'filter';
    }
}