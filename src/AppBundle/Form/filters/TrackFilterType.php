<?php

namespace AppBundle\Form\filters;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TrackFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('track', 'cd_autocomplete_track', array(
                            'attr' => array('class' => 'form-control', 'placeholder'=> 'Artista, Tema, Genero'),
                            'class'=> 'AppBundle:Track',
                            'property' => 'titulo',
                            'search_method'=> 'getTrackByLike',                            
                            ))                                             
                           ;                        
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
       $resolver->setDefaults(array(     
            'cascade_validation' => true,
            'csrf_protection'   => false                      
        ));
    }

    public function getName()
    {
        return 'track_filter';
    }
}