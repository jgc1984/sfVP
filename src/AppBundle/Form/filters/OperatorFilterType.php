<?php

namespace AppBundle\Form\filters;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OperatorFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('artista',  'entity', array(
                                    'class'=> 'AppBundle:Artista',
                                    'label'=> 'Artista'                                                                                                                                                     
                                    )
                        )           
                           ;                        
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
       $resolver->setDefaults(array(     
            'cascade_validation' => true,
            'csrf_protection'   => false                      
        ));
    }

    public function getName()
    {
        return 'operator_filter';
    }
}