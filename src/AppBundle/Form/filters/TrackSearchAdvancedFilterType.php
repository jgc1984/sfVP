<?php

namespace AppBundle\Form\filters;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AppBundle\Form\filters\TrackSearchFilterType;

class TrackSearchAdvancedFilterType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    
    
      $builder->add('filtro', 'collection', array(
          'type' => new TrackSearchFilterType(),
          'prototype'=> true,
          'allow_add' => true,
          
      ));
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
   $resolver->setDefaults(array(     
    'cascade_validation' => true,
    'csrf_protection'   => false                      
    ));
 }

 public function getName()
 {
  return 'search';
}
}