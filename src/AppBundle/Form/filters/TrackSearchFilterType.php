<?php

namespace AppBundle\Form\filters;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class TrackSearchFilterType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    $trackFields = 
        array('titulo' => 'Titulo', 
              'formato' => 'Formato', 
              'genero'=> 'Genero', 
              'calidad'=>'Calidad',              
              'artista'=>'Artista',
              'quality_video'=> 'Calidad Video',
              'quality_audio'=> 'Calidad Audio',
              );

    $operadores = array('=' => '=', 'like' => 'Like');

    $fuenteFields = array('nombre' => 'Nombre', 'lugar_grabacion' => 'Lugar Grabacion', 'soporte'=> 'Soporte', 'fechaGrabacion'=>'Fecha Grabacion', 'fechaPublicacion'=>'Fecha Publicacion');

    $builder
      ->add('target', 'choice', array(
            'empty_value'=> 'TARGET',
            'choices'  => array('track' => 'Track', 'fuente' => 'Fuente'),
            'required' => false,
            'attr'=> array('class'=> 'form-control')
      ))
      ->add('track', 'choice', array(
                              'empty_value'=> 'TRACK',
                              'choices'  => $trackFields,
                              'required' => false,
                              'attr'=> array('class'=> 'form-control')
        ))

      ->add('genero', 'choice', array(
                              'empty_value'=> 'GENERO',
                              'choices'  => array(0 => 'FOLKLORE', 1 => 'POP', 2 =>'SMOOTH JAZZ', 3 => 'TRIP HOP', 4 => 'ALTERNATIVO', 5 => 'TRIP ROCK', 6 => 'ROCK', 7 => 'BLUES', 8 => 'HARD ROCK', 9 =>'HEAVY METAL'),
                              'required' => false,
                              'attr'=> array('class'=> 'form-control btn-warning genero')
                  ))
      ->add('formato', 'choice', array(
                              'choices'  => array('vob' => 'VOB', 'mp4' => 'MP4', 'mpeg'=> 'MPEG', 'avi'=> 'AVI', 'mpg'=> 'MPG', 'mkv'=> 'MKV'),
                              'required' => false,
                              'attr'=> array('class'=> 'form-control artista')
                  ))

      ->add('vq', 'choice', array(
                              'choices'  => array('LQ' => 'LQ', 'NQ' => 'NQ', 'HQ'=> 'HQ'),
                              'required' => false,
                              'attr'=> array('class'=> 'form-control')
                  ))
      ->add('aq', 'choice', array(
                              'choices'  => array('LQ' => 'LQ', 'NQ' => 'NQ', 'HQ'=> 'HQ'),
                              'required' => false,
                              'attr'=> array('class'=> 'form-control')
                  ))


      ->add('fechaGrabacion', 'date' , 
                        array(
                                  'required' => false,
                                  'widget' => 'single_text',
                                  'input' => 'datetime',
                                  'format' => 'dd-MM-yyyy',
                                  'label'=> 'Fecha de Grabacion', 
                                  'attr'=> array('class'=> 'form-control grabacion'))
                        )
      ->add('fechaPublicacion', 'date' , 
                        array(
                                  'required' => false,
                                  'widget' => 'single_text',
                                  'input' => 'datetime',
                                  'format' => 'dd-MM-yyy',
                                  'label'=> 'Fecha de Publicacion', 
                                  'attr'=> array('class'=> 'form-control publicacion'))
                        )
      ->add('year','text',  
                        array(
                          'required' => false,
                          'attr'=> array('class'=> 'form-control', 'placeholder'=>'1999', 'maxlength'=> 4)
          ))
      ->add('artista', 'entity',array(
                                        'empty_value'=> 'ARTISTA',
                                        'required'=> false,
                                        'attr'=> array('class'=> 'form-control artista'),
                                        'class'=> 'AppBundle:Artista',                                         
                                        'query_builder' => function (EntityRepository $repository) {
                                    
                                          $qb = $repository->createQueryBuilder('a')                                                  
                                                  ->orderBy('a.nombre');                                                  
                                          return $qb;
                                        }                                      
                                    ))

      ->add('fuente', 'choice', array(
                              'empty_value'=> 'FUENTE',
                              'choices'  => $fuenteFields,
                              'required' => false,
                              'attr'=> array('class'=> 'form-control')
        ))

      ->add('operador', 'choice', array(
            'empty_value'=> 'Operador',
                              'choices'  => $operadores,
                              'required' => false,
                              'attr'=> array('class'=> 'form-control')
        ))

      ->add('valor', 'text', array(                      
                    'required' => false,
                    'attr'=> array('class'=> 'form-control valor', 'placeholder'=> 'Valor')
                    ))
      ->add('registers', 'choice', array(
                              'empty_value'=> 'REGISTROS',
                              'choices'  => array(5 => '5', 10 => '10', 15 =>'15'),
                              'required' => false,
                              'attr'=> array('class'=> 'form-control registros')
                  ))
      ->add('deleted', 'checkbox', array('required' => false, 
                                         'label'=> 'Borrados',
                                         'attr'=> array('class'=> 'form-control') ))              
        ;         
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
   $resolver->setDefaults(array(     
    'cascade_validation' => true,
    'csrf_protection'   => false                      
    ));
 }

 public function getName()
 {
  return 'searchAdvanced';
}
}