<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use UtilBundle\Form\DataTransformer\ObjectToIdTransformer;
use Doctrine\ORM\EntityRepository;


class TrackType extends AbstractType
{
    
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //$entityManager = $this->arrayParams['container']->get('doctrine')->getManager();        

        //$fuenteTransformer = new ObjectToIdTransformer($entityManager, $this->arrayParams['fuente'], self::entityFuente);
        
        
        /*if(isset($this->arrayParams['fuente']) && $this->arrayParams['fuente']){
            $builder
                ->add($builder
                        ->create('fuente', 'hidden')
                        ->addModelTransformer($fuenteTransformer));
        }*/
                        
        $builder            
           
            ->add('titulo', 'text',array('label'=>'Titulo',  'attr'=> array('class'=> 'form-control botones')))
            ->add('nombreArchivo', 'text',array('label'=>'Archivo', 'attr'=> array('class'=> 'form-control botones')))
            ->add('path', 'text',array('label'=>'Path',  'attr'=> array('class'=> 'form-control botones')))
            ->add('pathVideo', 'choice', array(
                              'choices'  => array('/media/gabriel/DATOS/VIDEOS/ROCK' => 'ROCK', 
                                                  '/media/gabriel/DATOS/VIDEOS/POP' => 'POP', 
                                                  '/media/gabriel/DATOS/VIDEOS/ALTERNATIVO' => 'ALTERNATIVO',
                                                  '/media/gabriel/DATOS/VIDEOS/LATINOS' => 'LATINOS',
                                                  '/media/gabriel/DATOS/VIDEOS/RETRO' => 'RETRO',
                                                  '/media/gabriel/DATOS/VIDEOS/TECHNO' => 'TECHNO',
                                                  ),
                              'required' => false,
                              'mapped'=> false,
                              'attr'=> array('class'=> 'form-control botones')
                  ))
            ->add('fuente', null, array('label'=>'Fuente',  'attr'=> array('class'=> 'form-control botones') ))
            ->add('artista', 'entity',array(
                                        'required'=> false,
                                        'attr'=> array('class'=> 'form-control warning'),
                                        'class'=> 'AppBundle:Artista', 
                                        'empty_value'=> 'Artista',
                                        'label'=> 'Artista', 
                                        'query_builder' => function (EntityRepository $repository) {
                                    
                                          $qb = $repository->createQueryBuilder('a')                                                  
                                                  ->orderBy('a.nombre');                                                  
                                          return $qb;
                                        }                                      
                                    )
                        )           
            ->add('genero')
            ->add('calidad', 'choice', array(
                              'choices'  => array(0 => 'NQ', 1 => 'HQ'),
                              'required' => false,
                              'attr'=> array('class'=> 'form-control warning')
                  ))
                    
           /* ->add('duracion', 'text',array(
                'required'=>false 
                ,'attr'=> array('class'=> 'form-control form-custom')))*/

           ->add('formato', 'choice', array(
                              'choices'  => array('vob' => 'VOB', 'mp4' => 'MP4', 'mpeg'=> 'MPEG', 'avi'=> 'AVI', 'mpg'=> 'MPG', 'mkv'=> 'MKV'),
                              'required' => false,
                              'attr'=> array('class'=> 'form-control warning')
                  ))

           
           ->add('playlists', 'entity', array(
                'attr'=> array('class'=> 'form-control warning'),
                'required'=> false,
                'empty_value'=> 'Elejir una lista',
                'multiple' => true,   // Multiple selection allowed                            
                'class'    => 'AppBundle:Playlist',
            ))
           
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Track'
        ));
    }
    
    

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_track';
    }
}
