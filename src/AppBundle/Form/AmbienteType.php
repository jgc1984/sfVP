<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AmbienteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', 'text',array('attr'=> array('class'=> 'form-control form-custom')))
            ->add('descripcion', 'textarea',array('attr'=> array('class'=> 'form-control form-custom')))   
            ->add('horaComienzo', 'text', array('attr'=> array('class'=> 'form-control timepicker')))
            ->add('horaFin', 'text', array('attr'=> array('class'=> 'form-control timepicker')))
            ->add('listas')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Ambiente'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ambiente';
    }
}
