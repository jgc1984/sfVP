<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class EditMasivTrackType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {


    $builder
      ->add('target', 'choice', array(
                              'choices'  => array('genero' => 'Genero', 'folder' => 'Carpeta', 'artista' =>'Artista', 'fuente' => 'Fuente', 'playlists' => 'Playlists', 'borrar' => 'Eliminar'),
                              'required' => false,
                              'attr'=> array('class'=> 'form-control target')
                  ))

      ->add('genero', 'choice', array(
                              'choices'  => array(0 => 'FOLKLORE', 1 => 'POP', 2 =>'SMOOTH JAZZ', 3 => 'TRIP HOP', 4 => 'ALTERNATIVO', 5 => 'TRIP ROCK', 6 => 'ROCK', 7 => 'BLUES', 8 => 'HARD ROCK', 9 =>'HEAVY METAL'),
                              'required' => false,
                              'attr'=> array('class'=> 'form-control form-custom genero')
                  ))
      /*->add('formato', 'choice', array(
                              'choices'  => array('vob' => 'VOB', 'mp4' => 'MP4', 'mpeg'=> 'MPEG', 'avi'=> 'AVI', 'mpg'=> 'MPG', 'mkv'=> 'MKV'),
                              'required' => false,
                              'attr'=> array('class'=> 'form-control')          
                  ))*/

      ->add('folder', 'choice', array(
                              'choices'  => array('/media/gabriel/DATOS/VIDEOS/ROCK' => 'ROCK', 
                                                  '/media/gabriel/DATOS/VIDEOS/POP' => 'POP', 
                                                  '/media/gabriel/DATOS/VIDEOS/ALTERNATIVO' => 'ALTERNATIVO',
                                                  '/media/gabriel/DATOS/VIDEOS/LATINOS' => 'LATINOS',
                                                  '/media/gabriel/DATOS/VIDEOS/RETRO' => 'RETRO',
                                                  '/media/gabriel/DATOS/VIDEOS/TECHNO' => 'TECHNO',
                                                  ),
                              'required' => false,
                              'attr'=> array('class'=> 'form-control'),          
                              'mapped'=> false,
                              
                  ))

      ->add('artista', 'entity',array(
                                        'attr'=> array('class'=> 'form-control'),          
                                        'required'=> false,                                        
                                        'class'=> 'AppBundle:Artista', 
                                        'empty_value'=> 'Artista',
                                        'label'=> 'Artista', 
                                        'query_builder' => function (EntityRepository $repository) {
                                    
                                          $qb = $repository->createQueryBuilder('a')                                                  
                                                  ->orderBy('a.nombre');                                                  
                                          return $qb;
                                        }                                      
                                    ))

      ->add('fuente', 'entity',array(
                                        'required'=> false,                                        
                                        'class'=> 'AppBundle:Fuente', 
                                        'empty_value'=> 'Fuente',
                                        'label'=> 'Artista', 
                                        'attr'=> array('class'=> 'form-control'),
                                        'query_builder' => function (EntityRepository $repository) {
                                    
                                          $qb = $repository->createQueryBuilder('f')                                                  
                                                  ->orderBy('f.nombre');                                                  
                                          return $qb;
                                        }                                      
                                    ))
      ->add('playlists', 'entity',array(
                                        'required'=> false,                                        
                                        'class'=> 'AppBundle:Playlist', 
                                        'empty_value'=> 'Playlist',
                                        'attr'=> array('class'=> 'form-control'),          
                                        'label'=> 'Playlist', 
                                        'query_builder' => function (EntityRepository $repository) {
                                    
                                          $qb = $repository->createQueryBuilder('p')                                                  
                                                  ->orderBy('p.nombre');                                                  
                                          return $qb;
                                        }                                      
                                    ));
      
             
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
   $resolver->setDefaults(array(     
    'cascade_validation' => true,
    'csrf_protection'   => false                      
    ));
 }

 public function getName()
 {
    return 'editMasivTrack';
 }

}