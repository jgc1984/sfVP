<?php

namespace AppBundle\Form;

use AppBundle\Entity\Fuente;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class FuenteType extends AbstractType {
       
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $accept = 'image/*';

        $assert = new Assert\File(array(
            'maxSize' => '5120k',
            'mimeTypes' => array(
                'image/png',
                'image/jpeg',
                'image/gif'
            ),
            'mimeTypesMessage' => 'Por favor seleccione una imágen con formato válido',
        ));
        
        $builder
                ->add('nombre', 'text', array('label'=> 'Nombre', 'attr'=> array('class'=> 'form-control form-custom-holder')))                
                ->add('lugarGrabacion', 'text', array('label'=> 'Lugar de Grabacion', 'attr'=> array('class'=> 'form-control form-custom-holder')))
                ->add('soporte', 'choice', array(
                              'choices'  => array('dvd5' => 'DVD5', 'dvd9' => 'DVD9'),
                              'required' => false,
                              'attr'=> array('class'=> 'form-control form-custom')
                  ))
                ->add('fechaRecorded', 'date' , 
                        array(
                                  'required' => false,
                                  'widget' => 'single_text',
                                  'input' => 'datetime',
                                  'format' => 'dd/MM/yyyy',
                                  'label'=> 'Fecha de Grabacion', 
                                  'attr'=> array('class'=> 'form-control date form-custom-holder'))
                        )
                ->add('fechaPublished', 'date' ,  
                        array(
                                  'required' => false,
                                  'widget' => 'single_text',
                                  'input' => 'datetime',
                                  'format' => 'dd/MM/yyyy',
                                 'label'=> 'Fecha de Publicacion', 
                                 'attr'=> array('class'=> 'form-control date form-custom-holder'))
                        )
                ->add('tracks', 'collection', array(
                            'type' => new TrackType(),
			    'by_reference' => false,
                            'allow_add' => true,   
                            'allow_delete' => true,                            
                            )
                          )
                //->add('artista', null , array('label'=> 'Artista', 'attr'=> array('class'=> 'form-control')))
                ->add('file', 'file', array(
                            'required' => false,
                            'label' => 'Foto',
                            'mapped' => false,
                            'attr' => array('accept' => $accept),
                            'constraints' => array($assert),
                            'auto_initialize' => false,
                            'attr' => array('class' => 'input-file uniform_on')
                        ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Fuente',
            'cascade_validation'=>true  
        ));
    }

    public static function processImage(UploadedFile $uploaded_file, Fuente $fuente) {
        $path = 'portada/';

        $uploaded_file_info = pathinfo($uploaded_file->getClientOriginalName());
        $file_name = $fuente->getId() . "_" . md5(rand()) .
                "." .
                $uploaded_file_info['extension']
        ;

        $uploaded_file->move($path, $file_name);

        return $file_name;
    }

    /**
     * @return string
     */
    public function getName() {
        return 'appbundle_fuente';
    }

}
