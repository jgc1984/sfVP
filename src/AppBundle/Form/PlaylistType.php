<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AppBundle\Form\TrackType;

class PlaylistType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', 'text',array('required'=> false, 'attr'=> array('class'=> 'form-control')))
            ->add('descripcion', 'textarea',array('required'=> false, 'attr'=> array('class'=> 'form-control')))                        
            //->add('tipo','text',array('attr'=> array('class'=> 'form-control form-custom')))
            ->add('tracks', 'collection', array(
                'type' => new TrackType(),

                'allow_add' => true,
                'allow_delete' => true,

                'prototype' => true,
                'prototype_name' => 'track__name__',
                
            ))
            ->add('hora_inicio')
            ->add('hora_fin')
      
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Playlist'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_playlist';
    }
}
