<?php

namespace CentroDia\UsuarioBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tree\Fixture\Role;

class LoadUsuarioData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $this->cargarUsuarios($manager);
    }
    
    public function cargarUsuarios(){                                
        
        $userManager = $this->container->get('fos_user.user_manager');
        $paula = $userManager->createUser();   
        $paula->setUsername('pautapia');
        $paula->setEmail('pautapia1@gmail.com');
        $paula->setPlainPassword('pau1234');
        $paula->setEnabled(true);
        $paulaP = $this->getReference('paula');
        $paula->setPersonal($paulaP);
        $paula->addRole('ROLE_ADMIN');        
        $userManager->updateUser($paula);
        
        $gabriel = $userManager->createUser();  
        $gabriel->setUsername('jgcabral');
        $gabriel->setEmail('jgc1984@gmail.com');
        $gabriel->setPlainPassword('paula@gabriel');  
        $gabriel->setEnabled(true);        
        $gabriel->addRole('ROLE_ADMIN');        
        $gabriel->setPersonal($this->getReference('gabriel'));
        $userManager->updateUser($gabriel);
        
        $maria = $userManager->createUser();  
        $maria->setUsername('mtapia');
        $maria->setEmail('mariadesign08@gmail.com');
        $maria->setPlainPassword('maria2015');  
        $maria->setEnabled(true);
        $maria->addRole('ROLE_ADMIN');
        $maria->setPersonal($this->getReference('maria'));
        $userManager->updateUser($maria);
        
        $silvia = $userManager->createUser();          
        $silvia->setUsername('silvia2015');
        $silvia->setEmail('silvia@gmail.com');
        $silvia->setPlainPassword('silvia2015');  
        $silvia->setEnabled(true);
        $silvia->addRole('ROLE_ADMIN');
        $silvia->setPersonal($this->getReference('silvia'));        
        $userManager->updateUser($silvia);
        
                                               
        
        //Secretario
        $michel= $userManager->createUser();  
        $michel->setUsername('mboero');
        $michel->setEmail('mboero@gmail.com');
        $michel->setPlainPassword('michel2015');  
        $michel->setEnabled(true);
        $michel->setPersonal($this->getReference('michel'));
        $michel->addRole('ROLE_SECRETARIO');
        $userManager->updateUser($michel);
        
        $alexis = $userManager->createUser();  
        $alexis->setUsername('aboero');
        $alexis->setEmail('aboero@gmail.com');
        $alexis->setPlainPassword('alexis2015');  
        $alexis->setEnabled(true);
        $alexis->setPersonal($this->getReference('alexis'));
        $userManager->updateUser($alexis);
        
        $cmaidana = $userManager->createUser();  
        $cmaidana->setUsername('cmaidana');
        $cmaidana->setEmail('cmaidana@gmail.com');
        $cmaidana->setPlainPassword('maidana2015');  
        $cmaidana->setEnabled(true);
        $cmaidana->setPersonal($this->getReference('claudio'));
        $cmaidana->addRole('ROLE_PROFESOR');
        $userManager->updateUser($cmaidana);
        
        $laufleitas = $userManager->createUser();  
        $laufleitas->setUsername('laufleitas');
        $laufleitas->setEmail('lau@gmail.com');
        $laufleitas->setPlainPassword('laura2015');  
        $laufleitas->setEnabled(true);
        $laufleitas->setPersonal($this->getReference('fleitas'));
        $laufleitas->addRole('ROLE_PROFESOR');
        $userManager->updateUser($laufleitas);
        
        $cmaid = $userManager->createUser();  
        $cmaid->setUsername('gkuchera');
        $cmaid->setEmail('gkuchera@gmail.com');
        $cmaid->setPlainPassword('kuchera2015');  
        $cmaid->setEnabled(true);
        $cmaid->setPersonal($this->getReference('geremias'));
        $cmaid->addRole('ROLE_PROFESOR');
        $userManager->updateUser($cmaid);
        
    }
            
    
    /**
     * 
     * {@inheritDoc}
     */
    public function getOrder()
    {
       return 2; 
    }
}