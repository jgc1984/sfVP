<?php

namespace AppBundle\Menu;

use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class BackendMenuBuilder extends ContainerAware {

    protected $user;

    /**
     *
     * Función que Arma la estructura principal de menu
     *
     * @param FactoryInterface $menu
     * @param array $options
     */
    public function mainMenu(FactoryInterface $factory, array $options) {


        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'sidebar-menu');

        $menu->addChild('Dashboard', array('route' => 'personas'))
                ->setAttribute('class', 'active')
                ->setAttribute('icon', 'fa fa-dashboard');
        
        if ($this->container->get('security.context')->isGranted(array('ROLE_USER'))) {
            $menu = $this->roleSuperAdmin($menu);
        }

        return $menu;
    }
    
    public function createBreadcrumbsMenu(Request $request) {
        //
        $bcmenu = $this->createMainMenu($request);
        return $this->getCurrentMenuItem($bcmenu);
    }
    
    public function getCurrentMenuItem($menu)
    {
        $voter = $this->container->get('centrodia.theme.menu.request');
        
        foreach ($menu as $item) {
            if ($voter->matchItem($item)) {
                return $item;
            }
            
            if ($item->getChildren() && $currentChild = $this->getCurrentMenuItem($item)) {
                return $currentChild;
            }
        }
        
        return null;
    }

    public function roleSuperAdmin($menu) {
        $menu = $this->empadronadorAdminMenu($menu);
        //$menu = $this->administracionMenu($menu);
        $menu = $this->academicoMenu($menu);
        //$menu = $this->institucionalMenu($menu);
        
                
        return $menu;
    }
     
    

    public function userMenu(FactoryInterface $factory, array $options) {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav pull-right');

        $menu->addChild('User', array('label' => 'Hi visitor'))
                ->setAttribute('dropdown', true)
                ->setAttribute('icon', 'icon-user');

        $menu['User']->addChild('Edit profile', array('route' => 'acme_hello_profile'))
                ->setAttribute('icon', 'icon-edit');

        return $menu;
    }

}
